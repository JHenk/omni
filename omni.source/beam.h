// beam.h
//
// enthaelt die Definition des Datentypes in dem die k-parallel Vektoren
// der verwendeten Strahlen gespeichert werden
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _BEAMSET_
#define _BEAMSET_

#include "vector2.h"

#ifndef NULL
#define NULL    0
#endif

struct beamset{
	int		nbeam;		// Gesamte Anzahl Strahlen
	vector2		*vec;		// Enthaelt die Vektoren
	int		*h;		// Enthaelt den h-Index des Vektors
	int		*k;		// enthaelt den k-Index des Vektors
	beamset();			// Der Konstruktor
	beamset(const beamset &);	// Der Kopierkonstruktor
	~beamset();			// Der Destruktor
        beamset &operator=(const beamset &); // Die Zuweisung (Kopie)
	};

// Konstruktor
inline	beamset::beamset()

{
vec = NULL;
h = NULL;
k = NULL;
}

// Kopierkonstruktor
inline beamset::beamset(const beamset &b){

  int i;

  nbeam = b.nbeam;

  //    delete[] vec;
  //    delete[] h  ;
  //    delete[] k  ;

  vec = new vector2[nbeam];
  h   = new int[nbeam];
  k   = new int[nbeam];
  
  for(i = 0; i < nbeam; i++){
    vec[i].x = b.vec[i].x;
    vec[i].y = b.vec[i].y;

    h[i]     = b.h[i];
    k[i]     = b.k[i];
  }

}

inline beamset &beamset::operator=(const beamset &b){

  int i;

  nbeam = b.nbeam;

  //    delete[] vec;
  //    delete[] h  ;
  //    delete[] k  ;

  vec = new vector2[nbeam];
  h   = new int[nbeam];
  k   = new int[nbeam];
  
  for(i = 0; i < nbeam; i++){
    vec[i].x = b.vec[i].x;
    vec[i].y = b.vec[i].y;

    h[i]     = b.h[i];
    k[i]     = b.k[i];
  }

  return *this;
}


// Destruktor
inline	beamset::~beamset()

{
delete[] vec;
delete[] h;
delete[] k;
}

#endif
