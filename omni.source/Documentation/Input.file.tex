\documentclass[12pt,a4paper]{article}
\usepackage{ae}
\usepackage[T1]{fontenc}

\input{defines}

\begin{document}
\begin{center}
        \textbf{\LARGE \omni}

        \textbf{\large Fully relativistic electron spectroscopy calculations}

        Format of input files

        \textit{\today}
\end{center}


\section{Introduction}
The calculation performed by \omni\ is specified by its input file.
The latter is usually specified on the command line (\command{\omni\ 
  $<$input file$>$ $<$output file$>$)}. If no files are specified the
default names are taken (\command{omni.in} and \command{omni.out}).

The supplied program \geninput\ can be used to generate input files
from scratch. Further it makes simple checks on the input specified by
the user. \textsl{This code needs to be updated\ldots}

\section{Running \omni}
\omni\ is run using the command line
\begin{verbatim}
 omni input output nthreads
\end{verbatim}
\command{input} and \command{output} specify the input and output file, respectively. \command{nthreads} (integer) specifies the number of threads. If \command{nthreads} equals $1$ or is not specified at all, a single thread is used --- \omni\ runs in its usual single-threaded way. If \command{nthreads} is equal or greater  $2$, \command{nthreads} are used --- \omni\ runs in parallel mode, using \command{openmp}.

Up to now not all modes are parallelized. Note that output data are not written to the output file instantaneously; output data are temporarliy stored in a buffer until it can be flushed. The buffer is flushed when output data in the correct order have been calculated; the order is given by the energy and wavevector meshes. This mechanism prevents mixing up of output data.


\section{Structure of the input file}
The input file can be cast into three main regions: the job control,
the crystal set-up, and the angular or $\vec{k}_{\parallel}$ mesh.
In the following we refer to the sample input file printed below.

\subsection{Job control}
\begin{description}
\item[Lines 1--3] The first three lines serve as comment lines and are
  printed on standard output.
  \begin{footnotesize}
  \begin{verbatim}
Omni (C++ version)
Complex band structure
Semi-infinite fcc Ni(001) with Cu lattice constant
  \end{verbatim}
  \end{footnotesize}
\item[Line 6]: This line specifies the global parameters for a job (aka
  a calculation). Some values are specified in \command{spec.h}.
  \begin{footnotesize}
  \begin{verbatim}
+ Job specifications ++++++++++++++++++++++++++++++++++++
+ Global parameters +++++++++++++++++++++++++++++++++++++
-2  3 20  10.00  1.00  -1  0  0  0.1  0 1                spec, lmax, nlay, eg, xrel, dbl, dis, xmin, optpotu, optpotl
  \end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[spec] Type of calculation. Here: complex-band structure. For
    details, see file \verb|spec.h|.
    \item[lmax] Maximum angular momentum.
    \item[nlay] Number of layers for which a calculation should be
      performed. If only the bulk is to be treated, \command{nlay ==
        0}.
    \item[eg] Energy radius. Specifies the number of beams (plane
      waves) in the calculation.
    \item[xrel] Strength of spin-orbit coupling. For \command{xrel ==
        1}, SOC is fully taken into account, for \command{xrel == 0}
      it is switched off (scalar-relativistic calculation). For
      intermediate values, a pseudo Dirac equation similar to the
      scalar-relativistic treatment is solved. \textbf{Caution:} Use
      \command{xrel = 1} for photoemission modes, otherwise the
      results will be wrong.
    \item[dbl] Number of layer doublings for the bulk. For
      \command{xrel == -1} the Bloch-wave method is used.
    \item[dis] Type of disorder.
      \begin{description}
        \item[0] No disorder.
        \item[1] Virtual crystal approximation (VCA).
        \item[2] Averaged t-matrix approximation (ATA).
        \item[3] Coherent potential approximation (CPA) for binary alloys.
         \item[4] Averaged t-matrix approximation (ATA) for ternary alloys.
         \item[5] Coherent potential approximation (CPA) for ternary alloys.
      \end{description}
    \item[spherical] specifies the output of the charge density
      calculations (Only used in charge density calculations).
    \item[xmin] Minimum of the modulus of eigenvalues in a band
      structure calculation (only used in band modes).
    \item[optpotu] Type of the optical potential, for upper states.
      \begin{description}
        \item[0] Power law.
        \item[1] Inoue-type optical potential. See I.\,H. Inoue
          \textit{et al.}, Physical Review Letters \textbf{74} (1995) 2539.
      \end{description}
    \item[optpotl] Type of the optical potential, for lower states.
      \begin{description}
        \item[0] Power law.
        \item[1] Inoue-type optical potential. See I.\,H. Inoue
          \textit{et al.}, Physical Review Letters \textbf{74} (1995) 2539.
      \end{description}
  \end{description}
\item[Lines 8 and 9] Energy parameters. On line 8, the energy range in
  eV is specified. The first parameter is the file flag. If $0$ is
  given, the next three numbers specify a (real) energy interval
  (emin, emax, estep). The imaginary part of this mesh is zero. If $1$
  is given, the next parameter specifies the file which contains the
  (complex) energy mesh, in standard list format (two columns: real
  and imaginary part of the energies). If $2$ is given, the next
  parameter specifies the file which contains the energy mesh that is
  produced by \textsf{emesh} (in \textsf{add-ons}).

  Line 9 contains the photon energy in photoemission mode, or energy
  of the lower (upper) state in CIS (CFS) mode..
  \begin{footnotesize}
  \begin{verbatim}
+ Energy parameters +++++++++++++++++++++++++++++++++++++
 0 0.05 30.05 5.00                                         energy range
21.2200                                                  omega
  \end{verbatim}
  \end{footnotesize}
\item[Line 11--13] Parameters for the incident light
  \begin{footnotesize}
  \begin{verbatim}
+ Electric-field parameters (only for photoemission) ++++
(0.59500, 0.84000)                                       epsilon
45.00000  0.00000 0                                      theta, phi, aflag
(1.00000, 0.00000) (0.00000, 1.00000)                    as, ap
  \end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[Line 11] Dielectric constant
  \item[Line 12] Incidence angles of the light
    ($\vartheta_{\mathrm{ph}}, \varphi_{\mathrm{ph}}$) and aflag. The
    latter determines whether the angles are given with respect to the
    sample (0) or with respect to the photoelectron detection
    direction (1).
  \item[Line 13] S- and p-polarized parts of the light.
  \end{description}
\item[Line 15] Spin polarization of the incident electron in SPLEED
  mode.
  \begin{footnotesize}
  \begin{verbatim}
+ Incident electron polarization (only for SPLEED) ++++++
 0.0000   0.0000   1.0000                                polarization
  \end{verbatim}
  \end{footnotesize}
\item[Line 17] Temperature in Kelvin (used for Debye-Waller factors).
  \begin{footnotesize}
  \begin{verbatim}
+ Temperature +++++++++++++++++++++++++++++++++++++++++++
 0.0000                                                  temperature
  \end{verbatim}
  \end{footnotesize}
\item[Lines 19--26] Description of the symmetry output.
  \begin{footnotesize}
  \begin{verbatim}
+ Symmetry parameters (only for bands and LDOS) +++++++++
 2  1                                                    angular representation, frame
 0.0000000000                                            initial z-rotation
 0.0000000000  0.0000000000  1.0000000000                local angular-momentum axis
 0.0000000000  0.0000000000  1.0000000000                local spin axis
                0 1
            0 1 0 1 0 1
        0 1 0 1 0 1 0 1 0 1 
    0 1 0 1 0 1 0 1 0 1 0 1 0 1
  \end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[Line 19] \command{symm} and \command{frame} specify the
    spin-angular basis and the frame of the output. For example,
    \command{symm} can be chosen as $(\kappa, \mu)$, $(l ,m \tau)$ or
    some combinations of the latter. \command{frame} specifies whether
    the output refers to the global frame ($z$-axis along the surface
    normal) or local (atomic) frame ($z$-axis along the magnetization
    direction).
  \item[Line 20] Initial $z$-rotation in degrees (usually 0 or 90).
  \item[Line 21] Local angular momentum axis ($x$, $y$, and $z$) in
    local flame. The local quantization axis can be chosen along the
    specified vector.
  \item[Line 22] Local spin momentum axis ($x$, $y$, and $z$) in
    local frame. The spin quantization axis can be chosen along the
    specified vector.
  \item[Line 23--26] These lines specify which spin-angular basis
    function belongs to what representation. In the example below, a
    simple spin-decomposition is given.
  \end{description}
  \item[Lines 29 and 30] Crystal set-up
  \begin{footnotesize}
  \begin{verbatim}
+ Crystal set-up ++++++++++++++++++++++++++++++++++++++++
+ Global parameters +++++++++++++++++++++++++++++++++++++
 6.81735                                                 lattice constant (Bohr)
 0.5000000000  0.5000000000  0.5000000000 -0.5000000000  base vectors
  \end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[Line 29] Lattice constant in Bohr
  \item[Line 30] Surface-parallel translation vectors. These specify the
    unit cell in each layer.
  \end{description}
\item[Lines 32--34] Description of the elements
  \begin{footnotesize}
\begin{verbatim}
+ Element definitions +++++++++++++++++++++++++++++++++++
 2                                                       number of elements
+ Element 1
 ni6 ni6.pot 0.5 1 1.0  ni6.pot 0.5 1 0.00    (name, file, scale, concentration)
+ Element 2
 ni1 ni1a.pot 0.5 1 0.45 ni1b.pot 0.5 1 0.55 (name, file, scale, concentration)
\end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[Line 32] Number of elements
  \item[Line 34] Specification of the first element.
    \begin{description}
    \item[name] The name of the element. The next four values show up as many times as crystals are being used (1 to 3).
    \item[file] Name of the file which contains the potential description
    \item[scale] Scaling factor of the exchange split potentials. For
      \command{scale == 0} the element is treated non-magnetic.
    \item[type] The type of the potential.
      \begin{description}
      \item[-2] magnetic spherical well
      \item[-1] spherical well
      \item[0] non-magnetic, spherically symmetric
      \item[1] magnetic, spherically symmetric
      \item[2] magnetic, not spherically symmetric
      \end{description}
    \item[conc] The concentration of this potential (used only in
      calculations with disorder).
    \end{description}
  \end{description}
\item[Line 38] Layer definitions
  \begin{footnotesize}
  \begin{verbatim}
+ Layer definitions +++++++++++++++++++++++++++++++++++++
 2  2  1  0                                              number of slabs and slices, nbulk
  \end{verbatim}
  \end{footnotesize}
  Number of slabs, slices and bulk layers. Each slab can
  consist of several identical layers, the so-called slices. The
  number of bulk layers specifies how many last slices are considered
  as bulk-like (a value of zero means no bulk, i.\,e.\ a film
  geometry). Of course are there two numbers for the bulk: one for the
  right- and one for the left-hand bulk in order to treat interface
  problems.
\item[Line 40] Surface barrier definition
  \begin{footnotesize}
  \begin{verbatim}
+ Surface barrier definition ++++++++++++++++++++++++++++
 0   0.7574   0.4054   0.1733                            barrier (upper)
 3   0.7574   0.4054   0.1733                            barrier (lower)
  \end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[Lines 40 and 41] Type and parameters of the surface barriers
    for the upper and the lower states. The first value specifies the
    barrier type.
    \begin{description}
    \item[-1] barrier read from a file
    \item[0] no barrier
    \item[1] step-like barrier
    \item[2] Jones-Jennings-Jepsen-type barrier
    \item[3] Henk-Schattke barrier
    \item[4] step-like barrier (magnetic)
    \item[5] Jones-Jennings-Jepsen-type barrier (magnetic)
    \item[6] Henk-Schattke barrier (magnetic)
    \item[7] Jones-Jennings-Jepsen-type barrier with corrugation
    \item[8] Henk-Schattke barrier with corrugation
    \item[9] Jones-Jennings-Jepsen-type barrier with corrugation (magnetic)
    \item[10] Henk-Schattke barrier with corrugation (magnetic)
    \item[11] totally reflecting barrier
    \item[12] Tamura barrier
    \item[13] Tamura barrier (magnetic)
    \item[14] Improved Henk-Schattke barrier for interfaces (magnetic)
    \end{description}
    In the non-magnetic case, the next three parameters specify the
    barrier shape. In the magnetic case, they specify the shape for
    spin-up, the next three are for spin-down (not shown here). The last
    three specify the direction of the barrier magnetization.
    
    If tunneling through a smooth interface barrier is chosen as a
    mode, the next parameters specify the interface position (slab),
    the width (in units of the lattice constant), the magnetic
    alignment (P or AP), and the bias voltage.
    
    In the cases with corrugation (types 7--10), the Fourier
    components of the barrier potential for non-zero reciprocal
    lattice vectors are specified in the function \command{void
      filllookup(beamset \&{}b, int spin)} in
    \command{source/basic/sfbarr.C}. The shape, given by the
    (0,0)-Fourier component, is---as for the other barrier
    types---specified in the input file.
  \end{description}
\item[Lines 43--50] Slab definition
  \begin{footnotesize}
  \begin{verbatim}
+ Slab 1
 1  12.844936                                            natom, v0
 ni6                                                     name
 0.0000000000  0.0000000000  0.0000000000                position
 0.0000000000  0.0000000000  1.0000000000                magnetization direction
 1   0.2000000000                                        repetition,  thickness
 0.5000000000  0.0000000000  0.5000000000                displacement
-0.1500   5.6100   1.0000  -0.0500   5.6100   1.2500     optical potential (upper)
 0.0000   0.0000   0.0000  -0.0200   5.4100   0.0000     optical potential (lower)
+ Slab 2
 1  13.75                                                natom, v0
 ni1                                                     name
 0.0000000000  0.0000000000  0.0000000000                position
 0.0000000000  0.0000000000  1.0000000000                magnetization direction
 1   0.5000000000                                        repetition,  thickness
 0.5000000000  0.0000000000  0.5000000000                displacement
-0.1500   5.6100   1.0000  -0.0500   5.6100   1.2500     optical potential (upper)
 0.0000   0.0000   0.0000  -0.0200   5.4100   0.0000     optical potential (lower)
  \end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[Line 43] Number of atoms in the unit cell and muffin-tin zero
    (in eV) relative to the vacuum level.
  \item[Line 44] Name of the element as defined above
  \item[Line 45] Position in the unit cell
  \item[Line 46] Direction of the magnetization (obviously in global frame)
  \item[Line 47] The number of repetition specifies by how many slices
    the slab is made of. The thickness (in units of the interlayer
    distance) specifies the place where the optical potentials match.
  \item[Line 48] The vector which connects this layer with the next.
    There should be as many displacement lines as number slices in the
    slab.
  \item[Lines 49 and 50] Parameters for the optical potential of upper and lower states.
    \begin{description}
    \item[optpot = 0] For the power law-type optical potential, the
      first three values are for the real part ($a$, $b$, $c$), the
      latter three for the imaginary part ($d$, $e$, $f$). The energy
      dependence is given by
      \begin{eqnarray*}
        \mathrm{Re} \Sigma(E) & = & a (E - b)^{c},
        \\
        \mathrm{Im} \Sigma(E) & = & d (E - e)^{f},
      \end{eqnarray*}
      with $a < 0$ and $d < 0$. $b$ and $e$ are usually set to the
      Fermi level, the energy zero is typically the vacuum level.
    \item[optpot = 1] For the Inoue-type optical potential only five
      parameters are used. The energy dependence is given by
      \begin{displaymath}
        \Sigma(E)
        =
        c \frac{a E'}{E' + \mathrm{i}a} \frac{d}{E' + \mathrm{i}d} + \mathrm{i}e,
      \end{displaymath}
      with $E' = E - b$. All parameters are positive.

      The parameter $e$ is not included in Inoue's origninal
      derivation. Here, it mimics a finite experimental energy
      resolution (Lorentzian).
    \end{description}
  \end{description}
\item[Lines 61--64] Mesh definition
  \begin{footnotesize}
  \begin{verbatim}
+ k-parallel and angular parameters +++++++++++++++++++++
 0                                                       k-parallel mode
 1 k.mesh                                                range or file name
 0                                                       k-averaging mode
 1 kmean.mesh                                            range or file name
  \end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[Line 61] Flag for $\vec{k}_{\parallel}$ mode. 
    \begin{description}
    \item[0] angular mode. Wavevectors are specified by polar angle
      and azimuth. Note that is meaningful mostly for spectroscopies
      where the electron-detection direction is specified. Further,
      the flag acts also on the mesh used for wavevector averaging.
    \item[1] $\vec{k}_{\parallel}$ mode. Each wavevector is specified
      by numbers $\alpha$ and $\beta$ such that $\vec{k}_{\parallel} =
      \alpha \vec{b}_{1} + \beta \vec{b}_{2}$.
    \item[2] Cartesian $\vec{k}_{\parallel}$ mode. Each wavevector is specified
      by numbers $\alpha$ and $\beta$ such that $\vec{k}_{\parallel} =
      \alpha {e}_{x} + \beta \vec{e}_{y}$.
    \item[3] $\vec{k}_{\parallel}$ mode with sample tilting. This mode
      is used to simulate photoemission experiments in which the
      sample is tilted and all other parameters are kept fixed.
    \item[4] Cartesian $\vec{k}_{\parallel}$ line mode.
    \end{description}
  \item[Line 62] The first number is the file flag. If nonzero, the
    next parameter is the name of the file which contains the list of
    values.  If zero, the next parameters specify the
    $\vec{k}_{\parallel}$ region.

    If `file flag' is $0$ (generate table) and if 0--3 is specified in
    line~62, the program reads $\alpha_{\mathrm{min}}$,
    $\alpha_{\mathrm{max}}$, $\alpha_{\mathrm{step}}$,
    $\beta_{\mathrm{min}}$, $\beta_{\mathrm{max}}$,
    $\beta_{\mathrm{step}}$.

    If 4 is specified in line~62 (file flag is dummy), the program
    reads $kmin.x$, $kmin.y$, $kmax.x$, $kmax.y$, and $nsteps$. The
    number of steps has to be at least $1$.
  \item[Line 63] Mode for of k-averaging.
    \begin{description}
    \item[0] Special points.
    \item[1] Cascading linear adaptive grids.
    \item[2] Triangular adaptive grids.
    \item[3] Triangular adaptive grids with less refinement.
    \item[4] Triangular Gauss-Legendre quadrature.
    \item[5] Parallelogram Gauss-Legendre quadrature.
    \end{description}
  \item[Line 64] If special points are used, one has to provide a line
    as line~62. Otherwise, that is for the adaptive meshes, one has to
    specify the number of initial steps, the error type---absolute
    error ($0$) or relative error ($1$)---and the maximum tolerated
    error. The next four numbers specify if a 2BZ quadrant is used
    ($1$) or not ($0$). The wavevector weights are automatically
    adjusted.
  \end{description}
\item[Lines 66--67] Defect specification for real-space and supercell modes
  \begin{footnotesize}
\begin{verbatim}
+ defect elements
defect.def         name of the defect definition file
cluster.def        name of the cluster definition file
\end{verbatim}
  \end{footnotesize}
  \begin{description}
  \item[Line 66] Name of the defect-definition file. A
    defect-definition file has the format
\begin{verbatim}
 2                 // number of defects 
 6 0  0  0  0  Fe 0.0  0.0  0.0  // layer, cell index, cell1, cell2, site index, defect element, displaxment
 7 0  0  0  0  Fe 0.01 0.02 0.03 // layer, cell index, cell1, cell2, site index, defect element, displacement
\end{verbatim}
    The first line specifies the number of defects in the real-space
    cluster.

    Each defect is defined in the following lines. The
    position of a defect is given by the layer index, the index of the
    cell in this layer, the translation offsets, and the site index.
    The element of the defect is given at last. The latter has to be
    defined in the input file; otherwise an error message is
    displayed. The translation offset is
    \begin{displaymath}
      \vec{R} = \mathrm{cell1}\, \vec{a}_{1} + \mathrm{cell2}\, \vec{a}_{2},
    \end{displaymath}
    where $\vec{a}_{1}$ and $\vec{a}_{2}$ are the basis vectors of the
    layer lattice (specified in the input file). $\mathrm{cell1}$ and
    $\mathrm{cell2}$ must be integer.

    The displacement $\vec{\tau}$, specified by the three coordinates
    [$\vec{\tau} = (x, y, z)$], is given in units of the lattice
    constant. It refers to a shift of the defect atom out off its bulk
    position $\vec{R}$ (which is specified by cell1 and cell2).

    A defect definition must match a cell in the real-space cluster.

    For Bloch-wave transmission mode using supercells, the defect
    definition looks as follows.
\begin{verbatim}
 2 5 5            // number of defects, N M
 6 0  0  0  0  Fe // layer, cell index, cell1, cell2, site index, defect element
 7 0  0  0  0  Fe // layer, cell index, cell1, cell2, site index, defect element
\end{verbatim}
    The first line contains in addition the size of the $N \times M$
    supercell, $N$ and $M$. The cell indices run from $n = 0, \ldots,
    N - 1$ and $m = 0, \ldots, M - 1$. There is no displacement off the ideal
    positions.

  \item[Line 67] Name of the cluster-definition file. A
    cluster-definition file has the format
\begin{verbatim}
14                // number of layers
 0 0                // layer 0, number of cells
 1 0                // layer 1, number of cells
 2 5                // layer 2, number of cells
 2 0  0  0            // layer 2, cell index, cell 1, cell2
 2 1 -1  1            // layer 2, cell index, cell 1, cell2
 2 2 -1 -1            // layer 2, cell index, cell 1, cell2
 2 3  1  1            // layer 2, cell index, cell 1, cell2
 2 4  1 -1            // layer 2, cell index, cell 1, cell2
 3 5                // layer 3, number of cells
 3 0  0  0            // layer 3, cell index, cell 1, cell2
 3 1 -1  1            // layer 3, cell index, cell 1, cell2
 3 2 -1 -1            // layer 3, cell index, cell 1, cell2
 3 3  1  1            // layer 3, cell index, cell 1, cell2
 3 4  1 -1            // layer 3, cell index, cell 1, cell2
 4 0                // layer 4, number of cells
 5 0                // layer 5, number of cells
 6 1                // layer 6, number of cells
 6 0  0  0            // layer 8, cell index, cell1, cell2 (defect here)
 7 1                // layer 7, number of cells
 7 0  0  0            // layer 8, cell index, cell1, cell2 (defect here)
 8 0                // layer 8, number of cells
 9 0                // layer 9, number of cells
10 5                // layer 10, number of cells
10 0  0  0            // layer 10, cell index, cell1, cell2
10 1 -1  1            // layer 5, cell index, cell1, cell2
10 2 -1 -1            // layer 5, cell index, cell1, cell2
10 3  1  1            // layer 5, cell index, cell1, cell2
10 4  1 -1            // layer 5, cell index, cell1, cell2
11 5                // layer 11, number of cells
11 0  0  0            // layer 11, cell index, cell1, cell2
11 1 -1  1            // layer 5, cell index, cell1, cell2
11 2 -1 -1            // layer 5, cell index, cell1, cell2
11 3  1  1            // layer 5, cell index, cell1, cell2
11 4  1 -1            // layer 5, cell index, cell1, cell2
12 0                // layer 12, number of cells
13 0                // layer 13, number of cells
\end{verbatim}
    The first line specifies the number of layers. It must match that
    given in the input file (j.general.nlayer).
    
    For each layer, the number of cells in that layer has to be given.
    A cell number might be zero. If it is nonzero, for each of the
    cells, the translation offset must be specified.

    For Bloch-wave transmission mode using supercells, the cluster
    definition file is not used.
\end{description}
\end{description}

\clearpage
\section{Sample input file}
\begin{footnotesize}
\begin{verbatim}
Omni2k (C++ version)
Complex band structure
Semi-infinite fcc Ni(001) with Cu lattice constant
+ Job specifications ++++++++++++++++++++++++++++++++++++
+ Global parameters +++++++++++++++++++++++++++++++++++++
-2  3 20  10.00  1.00  -1  0  0  0.1                     spec, lmax, nlay, eg, xrel, dbl, dis, xmin
+ Energy parameters +++++++++++++++++++++++++++++++++++++
 0.05 30.05 5.00                                         energy range
21.2200                                                  omega
+ Electric-field parameters (only for photoemission) ++++
(0.59500, 0.84000)                                       epsilon
45.00000  0.00000                                        theta, phi
(1.00000, 0.00000) (0.00000, 1.00000)                    as, ap
+ Incident electron polarization (only for SPLEED) ++++++
 0.0000   0.0000   1.0000                                polarization
+ Temperature +++++++++++++++++++++++++++++++++++++++++++
 0.0000                                                  temperature
+ Symmetry parameters (only for bands and LDOS) +++++++++
 2  1                                                    angular representation, frame
 0.0000000000                                            initial z-rotation
 0.0000000000  0.0000000000  1.0000000000                local angular-momentum axis
 0.0000000000  0.0000000000  1.0000000000                local spin axis
                0 1
            0 1 0 1 0 1
        0 1 0 1 0 1 0 1 0 1 
    0 1 0 1 0 1 0 1 0 1 0 1 0 1
+ Crystal set-up ++++++++++++++++++++++++++++++++++++++++
+ Global parameters +++++++++++++++++++++++++++++++++++++
 6.81735                                                 lattice constant (Bohr)
 0.5000000000  0.5000000000  0.5000000000 -0.5000000000  base vectors
+ Element definitions +++++++++++++++++++++++++++++++++++
 2                                                       number of elements
+ Element 1
 ni6   ni6.pot 0.5 1  ni6.pot 0.5 1 1.00                 2x (name, file, scale), concentration
+ Element 2
 ni1   ni1.pot 0.5 1  ni1.pot 0.5 1 1.00                 2x (name, file, scale), concentration
+ Layer definitions +++++++++++++++++++++++++++++++++++++
 2  2  1  0                                              number of slabs and slices, nbulk
+ Surface barrier definition ++++++++++++++++++++++++++++
 0   0.7574   0.4054   0.1733                            barrier (upper)
 3   0.7574   0.4054   0.1733                            barrier (lower)
+ Slab 1
 1  12.844936                                            natom, v0
 ni6                                                     name
 0.0000000000  0.0000000000  0.0000000000                position
 0.0000000000  0.0000000000  1.0000000000                magnetization direction
 1   0.2000000000                                        repetition,  thickness
 0.5000000000  0.0000000000  0.5000000000                displacement
-0.1500   5.6100   1.0000 -0.0500   5.6100   1.2500      optical potential (upper)
 0.0000   0.0000   0.0000 -0.0200   5.4100   0.0000      optical potential (lower)
+ Slab 2
 1  13.75                                                natom, v0
 ni1                                                     name
 0.0000000000  0.0000000000  0.0000000000                position
 0.0000000000  0.0000000000  1.0000000000                magnetization direction
 1   0.5000000000                                        repetition,  thickness
 0.5000000000  0.0000000000  0.5000000000                displacement
-0.1500   5.6100   1.0000 -0.0500   5.6100   1.2500      optical potential (upper)
 0.0000   0.0000   0.0000 -0.0200   5.4100   0.0000      optical potential (lower)
+ k-parallel and angular parameters +++++++++++++++++++++
 0                                                       k-parallel or angular mode
 1 k.mesh                                                range or file name
 0                                                       k-averaging mode 
 1 kmean.mesh                                            range of fime name
+ defect elements
defect.def
cluster.def
\end{verbatim}
\end{footnotesize}

\section{How to treat disorder}
In any cases, except for disorder mode $0$, for each element two
potentials are read.

The program treats mean-field disorder in two principally different
ways. First, one can average potentials, as it is done in disorder
mode $1$ (virtual crystal approximation). In this case, the potentials
are averaged in the early stages of the computation and the subsequent
calculations proceeds as in the case of disorder mode $0$ (no
disorder). Second, one can treat averages of configuration, as it is
done in disorder modes $2$ (averaged t-matrix approximation) and $3$
(inhomogeneous single-site coherent potential approximation). This
requires a complete different treatment of the calculation: impurity
problems, averaging over $\vec{k}_{\parallel}$, etc. In the program,
two crystals are setup, one with the first potential ($A$), the other
for the second potential ($B$). From these, the effective average
medium ($C$) is constructed.

Note for CPA: Each layer has to correspond $1$-to-$1$ to a slab.
Otherwise the CPA may not converge (self-consistency loop).

In the real-space and supercell modes, disorder can be introduced by
replacing atoms, via the defect-definition file.

\section{How integrate over the surface Brillouin zone}
There are three modes for Brillouin zone integration.
\begin{description}
\item[Special points] One provides \omni\ with a symmetry-adapted mesh
  of $\vec{k}_{\parallel}$, evaluates the desired quantity at the mesh
  points, and then sums up the weighted values. The mesh can be
  generated by \kmesh. The error and the quality of the mesh has to be
  estimated by performing calculations with different meshes.
\item[Cascading linear adaptive grid] The two-dimensional integral is
  performed as two successive one-dimensional integrations. Therefore,
  one uses basically linear adaptive grids.
  
  One first generates an equidistant mesh in the Brillouin zone. For
  each basic interval, one integrates the function via the trapezoidal
  and the Simpson rule. If these two integrals differ too much, reduce
  the interval size and integrate on the finer interval. Otherwise
  take the Simpson integral. This algorithm can be easily implemented
  in a recursive fashion.
  
  The main advantage of this algorithm is that it reduces the mesh
  size near the important points. In `flat' areas the mesh remains
  coarse.

\item[Triangular adaptive grid] Instead of cascading linear adaptive
  grids, one performs the integration on triangles. The basic idea is,
  however, the same as for linear meshes.
\end{description}

\section{How to calculate spin magnetic and orbital moments}
The Green function allows for calculating expectation values via
\begin{displaymath}
  \left< A \right>
  =
  - \frac{1}{\pi} \mathrm{Im}\, \mathrm{Tr}(A G).
\end{displaymath}
If $A = 1$, the density of states is calculated (or exactly: the Bloch
spectral function if the Green function is resolved with respect to
energy and wave-vector). For $A = \sigma_{z}$ and $A = l_{z}$ the spin
magnetic moment and the orbital moment are calculated. The type of
moment is specified in \command{source/basic/ldos.C} via the variable
\command{MOMENT}.

\section*{Contact}
In case of questions or problems, mail to henk@mpi-halle.mpg.de.

\end{document}