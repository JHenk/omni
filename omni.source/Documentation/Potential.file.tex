\documentclass[12pt,a4paper]{article}
\usepackage{ae}
\usepackage[T1]{fontenc}

\input{defines}

\begin{document}
\begin{center}
        \textbf{\LARGE \omni}

        \textbf{\large Fully relativistic electron spectroscopy calculations}

        Format of potential files

        \textit{\today}
\end{center}


\section{Introduction}
The potentials used by \omni\ are stored in files, one for each
element (see \verb|Input.file.pdf|). There are different potential
types which are specified by code numbers (see \verb|potential.h|).
\begin{verbatim}
POT_MAG_EMPTY_SPHERE_SIC  -4  // empty, magnetic, spherical, SIC
POT_EMPTY_SPHERE_SIC      -3  // empty, non-magnetic, spherical, SIC
POT_MAG_EMPTY_SPHERE      -2  // empty, magnetic, spherical
POT_EMPTY_SPHERE          -1  // empty, non-magnetic, spherical
POT_NONMAG_SPHERE          0  // non-magnetic, spherical
POT_MAG_SPHERE             1  // magnetic, spherical
POT_MAG_NONSPHERE          2  // magnetic, non-spherical
POT_NONMAG_SPHERE_SIC      3  // non-magnetic, spherical, SIC
POT_MAG_SPHERE_SIC         4  // magnetic, spherical, SIC
POT_NONMAG_SPHERE_U        5  // non-magnetic, spherical, LDA+U
POT_MAG_SPHERE_U           6  // magnetic, spherical, LSDA+U
\end{verbatim}
The potentials are cast into several classes.
\begin{description}
\item[Nonmagnetic --- magnetic] Nonmagnetic potentials comprise only
  data for the spin-averaged potential, whereas magnetic potentials
  comprise data for spin-up and -down potentials.
\item[Full --- empty] `Full' means that the potential contains the
  Coulombic part $-Z/r$, while `empty' means that this part is not
  present ($Z = 0$). `Full' potentials describe atoms (in the solid),
  `empty' potential empty space such as interstitial regions of the
  vacuum region at a surface. 

  For a `full' potential, only its potential function $v(r)$ is
  stored, so that
  \begin{displaymath}
    V(r) = -\frac{Z}{r} + v(r),
  \end{displaymath}
  for each spin orientation (up, down) if specified. The requirement
  of a zero potential at the sphere radius, $V(r_{\mathrm{sph}}) = 0$,
  leads to
  \begin{displaymath}
    v(r_{\mathrm{sph}}) = Z.
  \end{displaymath}

  The potential in an empty sphere is
  constant.
\item[L(SDA) -- SIC -- U] `L(S)DA' potentials comprise only data for
  the effective Kohn-Sham potential, obtained self-consistently from
  DFT\@.

  `SIC' potentials contain in addition to the L(S)DA data potentials
  which have to be used for self-interaction corrected levels. At the
  moment, SIC is treated scalar-relativistically; so the SIC levels are
  specified by the angular quantum number $l$ (nonmagnetic) or $(l,
  m)$ (magnetic case).

  `L(S)DA+U' potentials use a constant shift ($U$) applied to the
  potential for a level specified by $(\kappa, \mu)$. $U$ is given in
  Hartree.
\item[Spherical --- nonspherical] `Spherical' means a spherically
  symmetric potential in the sphere, that is
  \begin{displaymath}
    V(\vec{r}) = V(r),
  \end{displaymath}
  the potential depends only on the radius. `Nonspherical' potentials
  use an angular expansion within the sphere,
  \begin{displaymath}
    V(\vec{r}) = \sum_{lm} V_{lm}(r) Y_{l}^{m}(\theta, \phi),
  \end{displaymath}
  but are constant in the interstitial region.
\end{description}


\section{General structure of a potential file}
\subsection{Header line}
The header line specifies the basic parameters of a potential.
\begin{verbatim}
    64  3.7620810560  0.0128378980  1001    0    0    7
\end{verbatim}
\begin{description}
\item[Core charge] The core charge (here, $64$, for Gd) is used to
  compute the Coulomb part of the potential, $-Z / r$.
\item[Sphere radius] The potential is spherically symmetric within a
  sphere, except for the full-potential type. The radius
  $r_{\mathrm{sph}}$ (here, $3.7620810560$) is given in Bohr radii.
\item[Step size] \omni\ uses an exponential radial mesh, with mesh
  points $r_{i}$ given by
  \begin{displaymath}
     r_{i} = r_{\mathrm{sph}} \exp{(i - n + 1) h}.
  \end{displaymath}
  $i$ runs from $0$ to $n - 1$. The step width $h$ is also given in
  Bohr radii (here, $0.0128378980$).
\item[Number of mesh points] The number of mesh points $n$ (here,
  $1001$).
\item[Mass and Debye temperature] These parameters, $m$ and
  $t_{\mathrm{D}}$, are used for the temperature correction
  (Debye-Waller factor). Here, both are $0$.
\item[Number of corrected levels] For potentials of SIC or L(S)DA+U
  types (see below), \omni\ needs to know the number of corrected
  levels, $n_{\mathrm{SIC}}$ or $n_{U}$. Here, $n_{\mathrm{SIC}} = 7$.
\end{description}

\subsection{Potential data}
Depending on the potential type, the next block (blocks) of the
potential file contains (contain) the potential function (functions)
at the $n$ radial mesh points, see list below.
\begin{description}
\item[-4] Value of the constant spin-up potential, value of the
  constant spin-down potential, in Hartree.  For each SI-corrected
  level: $l$, $m$, $n$ spin-up data, $n$ spin-down data.
\item[-3] Value of the constant potential, in Hartree.  For each
  SI-corrected level: $l$, $m$, $n$ data.
\item[-2] Value of the constant spin-up potential, value of the
  constant spin-down potential, in Hartree.
\item[-1] Value of the constant potential, in Hartree.
\item[0] $n$ data.
\item[1] $n$ data spin-up, $n$ data spin-down.
\item[2] For each pair of $(l, m)$: $l$, $m$, $n$ data for spin-up,
  $n$ data for spin-down.
\item[3] $n$ data for the LDA potential. For each SI-corrected
  level: $l$, $m$, $n$ data.
\item[4] $n$ data for the spin-up LSDA potential, $n$ data for the
  spin-down LSDA potential. For each SI-corrected level: $l$, $m$,
  $n$ data for spin-up, $n$ data for spin-down.
\item[5] $n$ data for the LDA potential. For each U-corrected
  level: $\kappa$, $U$
\item[6] $n$ data for the spin-up LSDA potential, $n$ data for the
  spin-down LSDA potential. For each U-corrected level: $\kappa$, $2
  \mu$, $U$ for spin-up, $U$ for spin-down.
\end{description}

\section*{Contact}
In case of questions or problems, mail to henk@mpi-halle.mpg.de.

\end{document}