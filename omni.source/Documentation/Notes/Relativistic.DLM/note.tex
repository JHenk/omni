\documentclass[a4paper,10pt]{scrartcl}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath}


\newcommand{\mat}[1]{\mathsf{#1}}

%opening
\title{Relativistic disordered local moments}
\author{J. Henk}

\begin{document}

\maketitle

\begin{abstract}

\end{abstract}

\section{Paramagnetic state}
The following considerations are based on Ref.~\cite{Szunyogh10}.

The single-site $t$-matrices are calculated within the local frame, that is with magnetization along $\vec{z}$. Using rotation matrices $\mat{R}(\vec{\omega})$ these are rotated into the global frame,
\begin{displaymath}
 \mat{t}_{i}(\vec{\omega}_{i})
 =
\mat{R}(\vec{\omega}_{i})\,
\mat{t}_{i}(\vec{z})\,
\mat{R}(\vec{\omega}_{i})^{\mathrm{\dagger}}.
\end{displaymath}
Here, $i$ denotes the site.

For the effective (coherent) medium, the excess scattering matrices
\begin{displaymath}
 \mat{X}_{i}(\vec{\omega}_{i})
 = \left[
  \left( \mat{t}_{i, c}^{-1} - \mat{t}_{i}(\vec{\omega}_{i})^{-1} \right)^{-1}
  - \mat{\tau}_{ii, c}
 \right]^{-1}
\end{displaymath}
have to vanish when averaged over all orientations of the magnetization (paramagnetic state):
\begin{displaymath}
 \frac{1}{4\pi} \int \mat{X}_{i}(\vec{\omega}_{i})\,\mathrm{d}^{2}\omega_{i} = 0.
\end{displaymath}
Alternatively, the scattering path operator has to fulfill
\begin{displaymath}
 \frac{1}{4\pi}
 \int \mat{\tau}_{ii}(\vec{\omega}_{i})\,\mathrm{d}^{2}\omega_{i} = \mat{\tau}_{ii, c}.
\end{displaymath}
Note that the orientation $\vec{\omega}_{i}$ plays the role of the concentration in substitutional alloys.

In terms of `defect matrix'
\begin{displaymath}
 \mat{D}_{i}(\vec{\omega}_{i}) = \mat{1} + \mat{X}_{i}(\vec{\omega}_{i}) \mat{\tau}_{ii, c},
\end{displaymath}
it follows
\begin{eqnarray*}
 \mat{\tau}_{ii, c}
& = &
\frac{1}{4\pi}
 \int \mat{\tau}_{ii}(\vec{\omega}_{i})\,\mathrm{d}^{2}\omega_{i},
 \\
 & = & 
\frac{1}{4\pi}
\mat{\tau}_{ii, c}
 \int \mat{D}_{i}(\vec{\omega}_{i})\,\mathrm{d}^{2}\omega_{i},
\\
 & = & 
\frac{1}{4\pi}
\mat{\tau}_{ii, c}
 \int \left[ \mat{1} + \mat{X}_{i}(\vec{\omega}_{i}) \mat{\tau}_{ii, c} \right]\,\mathrm{d}^{2}\omega_{i},
\\
 & = & 
\mat{\tau}_{ii, c}
+
\mat{\tau}_{ii, c}
\frac{1}{4\pi}
 \int \mat{X}_{i}(\vec{\omega}_{i}) \,\mathrm{d}^{2}\omega_{i} \mat{\tau}_{ii, c},
\end{eqnarray*}
from which the DLM condition for $\mat{X}_{i}(\vec{\omega}_{i})$ follows. Further,
\begin{eqnarray*}
 0
 & = &
 \frac{1}{4\pi}
 \int \mat{X}_{i}(\vec{\omega}_{i}) \,\mathrm{d}^{2}\omega_{i},
 \\
 & = & 
\frac{1}{4\pi}
 \int \left[\mat{D}_{i}(\vec{\omega}_{i})  - \mat{1} \right] \,\mathrm{d}^{2}\omega_{i}\, \mat{\tau}_{ii, c}^{-1},
 \\
\frac{1}{4\pi}
 \int \mat{D}_{i}(\vec{\omega}_{i})\,\mathrm{d}^{2}\omega_{i} & = & \mat{1}.
\end{eqnarray*}
This kind of condition is used in \textsc{omni} in the CPA mode.

The DLM algorithm then reads
\begin{enumerate}
 \item Make an initial guess for the coherent single-site scattering matrix $\mat{t}_{i, c}$.
 \item \label{step2} Average the defect matrix $\mat{D}_{i}(\vec{\omega}_{i})$ over the unit sphere
 \begin{enumerate}
 \item Rotate $\mat{t}_{i, c}$ into the global frame: $\mat{t}_{i, c} \to \mat{t}_{i}(\vec{\omega}_{i})$.
 \item Compute $\mat{\tau}_{ii}(\vec{\omega}_{i})$.
 \item Compute $\mat{D}_{i}(\vec{\omega}_{i})$.
 \end{enumerate}
 This gives $\mat{Y}_{i} \equiv \int \mat{D}_{i}(\vec{\omega}_{i})\,\mathrm{d}^{2}\omega_{i} / (4 \pi)$.
 \item Check whether $\mat{Y}_{i} == \mat{1}$. If yes, stop. Otherwise calculate the new $\mat{t}_{i, c}$ and go to step~\ref{step2}.
\end{enumerate}


\section{Evolution from the ferromagnetic to the paramagnetic state}
In the paramagnetic state (temperature $T \geq T_{\mathrm{C}}$), the magnetization fluctuates so that each orientation is equal likely. The distribution of orientations $f(\vec{\omega})$ is thus constant,
\begin{displaymath}
 f(\vec{\omega}) = \frac{1}{4 \pi}.
\end{displaymath}
In the ferromagnetic state (temperature $T = 0$), however, the magnetization does not fluctuate at all, giving
\begin{displaymath}
 f(\vec{\omega}) = \delta(\vec{\omega} - \vec{\omega}_{0}).
\end{displaymath}
It is hence strictly aligned along $\vec{\omega_{0}}$.

With the distribution we can reformulate the DLM criterion,
\begin{displaymath}
\frac{1}{4\pi}
 \int \mat{D}_{i}(\vec{\omega}_{i})\, f(\vec{\omega}_{i}; \vec{\omega}_{0})\, \mathrm{d}^{2}\omega_{i} = \mat{1}.
\end{displaymath}
With the two limits --- ferromagnetic and paramagnetic state --- in mind, we can construct a state at an intermediate temperature $T$, $0 \leq T \leq T_{\mathrm{C}}$. For this, one needs to find an intermediate distribution function.

\subsection{Construction of the distribution function}
The distribution function has to fulfill the normalization condition
\begin{displaymath}
 1 = 
 \int f(\theta, \phi)\,\mathrm{d}^{2}\omega.
\end{displaymath}
Explicitly,
\begin{displaymath}
 1 
= \int_{0}^{2\pi} \int_{0}^{\pi} f(\theta, \phi) \sin\theta\,\mathrm{d}\phi\,\mathrm{d}\theta
= \int_{0}^{2\pi}  f_{\phi} \mathrm{d}\phi\, \int_{0}^{\pi} f_{\theta}(\theta) \sin\theta\,\mathrm{d}\theta.
\end{displaymath}
In the paramagnetic state, both  $f_{\theta}$ and $f_[\phi$ are constant. Hence, it follows
\begin{displaymath}
\int_{0}^{\pi} f_{\theta}(\theta) \sin\theta \,\mathrm{d}\theta
= c \int_{0}^{\pi} \sin\theta \,\mathrm{d}\theta = - c \cos(\theta)\mid_{0}^{\pi} = -c (-1 -1)=  2 c \equiv 1
\end{displaymath}
and
\begin{displaymath}
 \int_{0}^{2\pi}  f_{\phi} \mathrm{d}\phi = 2 \pi c \equiv 1.
\end{displaymath}
Thus
\begin{displaymath}
 f_{\theta} = \frac{1}{2}
\end{displaymath}
and 
\begin{displaymath}
 f_{\phi} = \frac{1}{2\pi}.
\end{displaymath}
Or
\begin{displaymath}
 f(\theta, \phi) = \frac{1}{4\pi}.
\end{displaymath}

In the ferromagnetic state, the magnetization is oriented along $\vec{\omega}_{0} = (\theta_{0}, \phi_{0})$. Hence, we make the \textit{ansatz}
\begin{displaymath}
 f(\theta, \phi) = c \delta(\theta - \theta_{0}) \delta(\phi - \phi_{0}).
\end{displaymath}
The normalization then gives
\begin{displaymath}
 \frac{1}{c} 
= \int_{0}^{2\pi} \delta(\phi - \phi_{0})\, \mathrm{d}\phi\, \int_{0}^{\pi} \delta(\theta - \theta_{0}) \sin\theta\,\mathrm{d}\theta
= \sin\theta_{0},
\end{displaymath}
or
\begin{displaymath}
 f(\theta, \phi) = \frac{1}{\sin\theta_{0}} \delta(\theta - \theta_{0}) \delta(\phi - \phi_{0}).
\end{displaymath}

For intermediate temperatures, we represent the $\delta$-distributions by the von Mises-Fisher distribution
$f_{3}(\vec{x}; \vec{\mu}, \kappa)$
with mean and variance $\vec{\mu}$ and $1 / \kappa$ (see appendix). This distribution is the equivalent of a Gaussian distribution on the unit sphere in $p$ dimensions (Here: $p = 3$). Given the angles $\theta_{0}$ and $\phi_{0}$ at which the distribution should be centered, we have using spherical coordinates
\begin{displaymath}
 \mu_{x} = \cos\phi_{0}\,\sin\theta_{0},
 \mu_{y} = \sin\phi_{0}\,\sin\theta_{0},
 \mu_{z} = \cos\theta_{0}.
\end{displaymath}
Then any (transversal) fluctuation\footnote{Longitudinal fluctuations of the magnetization are neglected in this theory. This is why the von Mises-Fisher distribution is used: It gives a distribution on the unit sphere.} is expressed as a finite offset from this direction with weight $f_{3}(\vec{x}; \vec{\mu}, \kappa)$, with $\vec{x}$ expressed as
\begin{displaymath}
 x = \cos\phi\,\sin\theta,
 y = \sin\phi\,\sin\theta,
 z = \cos\theta.
\end{displaymath}
More precisely,
\begin{displaymath}
   f_{3}(\vec{x}; \vec{\mu}, \kappa)=C_{3}(\kappa)\exp\left( \kappa \vec{\mu}^{T} \vec{x} \right),
\end{displaymath}
\begin{displaymath}
 \vec{\mu}^{T} \vec{x}
 = \cos\phi\,\sin\theta \cos\phi_{0}\,\sin\theta_{0}
 + \sin\phi\,\sin\theta \sin\phi_{0}\,\sin\theta_{0}
 + \cos\theta\cos\theta_{0},
\end{displaymath}
and
\begin{displaymath}
    C_{3}(\kappa)=\frac {\kappa} {4\pi\sinh \kappa}=\frac {\kappa} {2\pi(e^{\kappa}-e^{-\kappa})}.
\end{displaymath}
$C_{3}$ is mainly the modified Bessel function of order $1/2$, $(\sinh z) / z$; cf.\ Ref.~\cite[10.2.13]{Abramowitz70}).

The modified DLM algorithm for intermediate temperatures then reads
\begin{enumerate}
 \item Set the direction $\vec{\omega}_{0}$ and width $\kappa$.
 \item Make an initial guess for the coherent single-site scattering matrix $\mat{t}_{i, c}$.
 \item \label{step2a} Average the defect matrix $\mat{D}_{i}(\vec{\omega}_{i})$ over the unit sphere
 \begin{enumerate}
 \item Rotate $\mat{t}_{i, c}$ into the global frame: $\mat{t}_{i, c} \to \mat{t}_{i}(\vec{\omega}_{i})$.
 \item Compute $\mat{\tau}_{ii}(\vec{\omega}_{i})$.
 \item Compute $\mat{D}_{i}(\vec{\omega}_{i})$.
 \end{enumerate}
 This gives
 \begin{displaymath}
\mat{Y}_{i} \equiv
 \frac{1}{4\pi}
 \int \mat{D}_{i}(\vec{\omega}_{i})\,f_{3}(\vec{\omega}; \vec{\omega}_{0}, \kappa) \,\mathrm{d}^{2}\omega_{i}.
 \end{displaymath}
 \item Check whether $\mat{Y}_{i} == \mat{1}$. If yes, stop. Otherwise calculate the new $\mat{t}_{i, c}$ and go to step~\ref{step2a}.
\end{enumerate}

\section{Effective Heisenberg exchange constants}
For a binary alloy, with constituents $A$ and $B$, the exchange energy between sites $i$ and $j$ reads
\begin{eqnarray*}
 E_{ij} & = &
 - \sum_{\mu, \nu = A, B}
 c_{\mu} c_{\nu} \tilde{J}_{i\mu, j\nu} \vec{e}_{i\mu} \cdot \vec{e}_{j\nu}
 \\
 & = &
- \sum_{\mu, \nu = A, B} 
  J_{i\mu, j\nu} \vec{e}_{i\mu} \cdot \vec{e}_{j\nu}.
\end{eqnarray*}
The local moments of the $A$ and $B$ constituents at the same site are identical ($\vec{e}_{iA} = \vec{e}_{iB}$), which is expressed by the single scalar product $\vec{e}_{i} \cdot \vec{e}_{j}$:
\begin{eqnarray*}
 E_{ij} & = &
 - \vec{e}_{i} \cdot \vec{e}_{j}
  \sum_{\mu, \nu = A, B}
  J_{i\mu, j\nu}
\\
 & = &
- \vec{e}_{i} \cdot \vec{e}_{j}
J_{i j}^{\mathrm{eff}},
\end{eqnarray*}
with
\begin{displaymath}
 J_{ij}^{\mathrm{eff}}
 =
 J_{iA, jA} + J_{iA, jB} + J_{iB, jA} + J_{iB, jB}.
\end{displaymath}
Note that the exchange constants refer to the ferromagnetic ground state.


For nonrelativistic DLM, with Ising-type averaging, the Heisenberg exchange constants are given by
\begin{displaymath}
 J_{i\mu, j\nu}
 =
 \frac{c_{\mu} c_{\nu}}{4\pi}
 \mathrm{tr}
 \int \Delta \mat{t}_{i\mu} \mat{\tau}_{i\mu, j\nu}^{\uparrow} \Delta \mat{t}_{j\nu} \mat{\tau}_{j\nu, i\mu}^{\downarrow}
 \,\mathrm{d}\epsilon,
\end{displaymath}
with $\mu, \nu = \Uparrow, \Downarrow$ (see paper on temperature-dependent exchange constants). This allows to define effective exchange constants
\begin{displaymath}
 J_{i j}^{\mathrm{eff}}
 =
 J_{i\Uparrow, j\Uparrow} - J_{i\Uparrow, j\Downarrow} -J_{i\Downarrow, j\Uparrow} + J_{i\Downarrow, j\Downarrow}.
\end{displaymath}
Since the magnetic moment of an $\Uparrow$-atom is along, say, the $z$-direction ($\vec{e}_{\Uparrow}$) and that of an $\Downarrow$-atom along the $-z$-direction ($\vec{e}_{\Downarrow} = - \vec{e}_{\Uparrow}$), we rewrite
\begin{displaymath}
 J_{i j}^{\mathrm{eff}}
 =
 \sum_{\mu, \nu= \Uparrow, \Downarrow} J_{i\mu, j\nu} \vec{e}_{\mu} \cdot \vec{e}_{\nu}
\end{displaymath}
because $\vec{e}_{\mu} \cdot \vec{e}_{\nu} = +1$ for $\mu = \nu$ and $\vec{e}_{\mu} \cdot \vec{e}_{\nu} = -1$ for $\mu \not= \nu$. The $-$ signs in the definition of the effective exchange constants are due to the fact that the ground state is ferromagnetic. Thus, opposite orientation of local moments costs energy. This can be motivated as follows.

Consider a nonmagnetic host with two magnetic impurities at sites $i$ and $j$. Then the exchange energy is $-J$ for parallel alignment and $+J$ for antiparallel alignment of the magnetic moments ($J > 0$). Thus, parallel alignment is preferred. If $\vec{e}_{i} = \vec{e}_{j}$, the exchange energy is $-J_{i\Uparrow, j\Uparrow}$ because this $J$ computed for parallel alignment. The exchange energy would be $+J_{i\Uparrow, j\Downarrow}$ ($= -J_{i\Uparrow, j\Uparrow}$) because this $J$ is computed for antiparallel alignment. Hence, the change of sign. This argumentation can be extended to the paramagnetic case.

In the paramagnetic case, the host is as well nonmagnetic since $c_{iA} = c_{iB} = 1/2$. Nevertheless, the effective interaction is nonzero (it would be zero if the $-$ signs were replaced by $+$ signs in $J_{ij}^{\mathrm{eff}}$) because $J_{i\Uparrow, j\Uparrow} = - J_{i\Uparrow, j\Downarrow} = -J_{i\Downarrow, j\Uparrow} = J_{i\Downarrow, j\Downarrow}$). This yields $J_{ij}^{\mathrm{eff}} = 4 J_{i\Uparrow, j\Uparrow} = \tilde{J}_{i\Uparrow, j\Uparrow}$. Eventually in the ferromagnetic case, i.\,e.\ $c_{iA} = 1 - c_{iB} = 1$, the effective interaction is given by $J_{i\Uparrow, j\Uparrow} = \tilde{J}_{i\Uparrow, j\Uparrow}$ and we recover the standard non-alloy case.

The Ising-type averaging can be generalized to rotational averaging in DLM. In analogy, we have 
\begin{displaymath}
 J(\vec{e}_{i},\vec{e}_{ j})
 =
 \frac{f(\vec{e}_{i}) f(\vec{e}_{j})}{4\pi}
 \mathrm{tr}
 \int \Delta \mat{t}(\vec{e}_{i})  \mat{\tau}^{\uparrow}(\vec{e}_{i},\vec{e}_{ j}) \Delta \mat{t}(\vec{e}_{j}) \mat{\tau}^{\downarrow}(\vec{e}_{j},\vec{e}_{ i})
 \,\mathrm{d}\epsilon,
\end{displaymath}
where $f$ is the derived von-Mises-Fisher distribution function (with respect to the direction of the ferromagnetic ground state) and $\vec{e}_{i}$ is the magnetic-moment direction of the atom at site $i$. The effective constant then reads
\begin{displaymath}
 J_{i j}^{\mathrm{eff}}
  =  
 \int \int  J(\vec{e}_{i},\vec{e}_{ j}) \vec{e}_{i} \cdot \vec{e}_{j} \,\mathrm{d}\Omega_{i} \,\mathrm{d}\Omega_{j}.
\end{displaymath}
The integration is over all directions (unit sphere).

We recover the case of Ising-type averaging by restricting the integration to $\pm \vec{z}$:
\begin{eqnarray*}
 J_{i j}^{\mathrm{eff}}
 & = &
 \int \int  J(\vec{e}_{i},\vec{e}_{ j}) \vec{e}_{i} \cdot \vec{e}_{j} [\delta(\vec{e}_{i}, \vec{z}) + \delta(\vec{e}_{i}, -\vec{z}) ] \,\mathrm{d}\Omega_{i} [\delta(\vec{e}_{j}, \vec{z}) + \delta(\vec{e}_{j}, -\vec{z}) ] \,\mathrm{d}\Omega_{j}.
\\
 & = &
J(\vec{z}_{i}, \vec{z}_{ j}) 
- J(\vec{z}_{i}, -\vec{z}_{ j}) 
- J(-\vec{z}_{i}, \vec{z}_{ j})
+ J(-\vec{z}_{i}, -\vec{z}_{ j}),
\end{eqnarray*}
which coincides with the effective $J$ of the Ising-type averaging.

\section{Weiss fields}
The Weiss field at site $i$ is given by (Ref.~\cite{Buruzs08})
\begin{displaymath}
 h_{i} =
 -\frac{3}{4 \pi^{2}}
 \Im \int^{E_{\mathrm{F}}} \int \vec{e}_{i}\cdot\vec{n}\, \log\det \mat{D}(\vec{e}_{i}) \,\mathrm{d}\Omega \,\mathrm{d}E.
\end{displaymath}
Here, $\mat{D}$ creates a magnetic impurity at site $i$ with magnetization direction $\vec{e}_{i}$ in the effective medium (for a chosen von-Mises-Fisher width $\kappa$). $\vec{n}$ is the average magnetization direction.
\emph{Does the above equation depend on the KKR representation?}

\section{Computational strategy}
The rotational averaging allows to calculate physical quantities for all temperatures $T$. A computational strategy may look as follows.
\begin{enumerate}
 \item Choose $\kappa$, the ``width'' of the von-Mises-Fisher distribution.
 \item Compute for the chosen $\kappa$ the quantity of interest $Q(\kappa)$ (e.\,g.\ density of states, spectral density, exchange constants).
 \item Compute for the chosen $\kappa$ the Weiss field $h(\kappa)$.
 \item From
  \begin{displaymath}
   \kappa = \frac{h}{k_{\mathrm{B}} T}
  \end{displaymath}
   determine the temperature,
 \begin{displaymath}
   T(\kappa) = \frac{h(\kappa)}{k_{\mathrm{B}} \kappa}.
  \end{displaymath}
   This gives $Q(T)$.
\end{enumerate}
Note that for $\kappa = $ $h(\kappa)  = 0$, so use 

\appendix
\section{Von Mises distribution (from Wikipedia)}
In probability theory and directional statistics, the von Mises distribution (also known as the circular normal distribution) is a continuous probability distribution on the circle. It may be thought of as a close approximation to the wrapped normal distribution, which is the circular analogue of the normal distribution. The von Mises distribution is more mathematically tractable than the wrapped normal distribution and is the preferred distribution for many applications. It is used in applications of directional statistics, where a distribution of angles is found which is the result of the addition of many small independent angular deviations, such as target sensing, or grain orientation in a granular material.

If $x$ is the angular random variable, it is often useful to think of the von Mises distribution as a distribution of complex numbers $z = e^{ix}$ rather than the real numbers $x$. The von Mises distribution is a special case of the von Mises-Fisher distribution on the $N$-dimensional sphere.

The von Mises probability density function for the angle $x$ is given by (Fig.~\ref{fig:vMd})
\begin{displaymath}
    f(x|\mu,\kappa)=\frac{e^{\kappa\cos(x-\mu)}}{2\pi I_0(\kappa)}
\end{displaymath}
where $I_{0}(x)$ is the modified Bessel function of order $0$.

The parameters $\mu$ and $1/\kappa$ are analogous to $\mu$ and $\sigma^{2}$ (the mean and variance) in the normal distribution.
\begin{itemize}
 \item $\mu$ is a measure of location (the distribution is clustered around $\mu$), and
 \item $\kappa$ is a measure of concentration (a reciprocal measure of dispersion, so $1/\kappa$ is analogous to $\sigma^{2}$).
 \item If $\kappa$ is zero, the distribution is uniform, and for small $\kappa$, it is close to uniform.
 \item If $\kappa$ is large, the distribution becomes very concentrated about the angle $\mu$ with $\kappa$ being a measure of the concentration. In fact, as $\kappa$ increases, the distribution approaches a normal distribution in $x$  with mean $\mu$ and variance $1 / \kappa$.
\end{itemize}
\begin{figure}
 \centering
 \includegraphics[width = 0.75\textwidth]{800px-VonMises_distribution_PDF}
 \caption{Von Mises distribution.}
 \label{fig:vMd}
\end{figure}


\section{Von Mises-Fisher distribution (from Wikipedia)}
In directional statistics, the von Mises-Fisher distribution is a probability distribution on the $(p - 1)$-dimensional sphere in $R^{p}$. If $p = 2$ the distribution reduces to the von Mises distribution on the circle.

The probability density function of the von Mises-Fisher distribution for the random $p$-dimensional unit vector $\vec{x}$ is given by
\begin{displaymath}
   f_{p}(\vec{x}; \vec{\mu}, \kappa)=C_{p}(\kappa)\exp \left( {\kappa \vec{\mu}^T \vec{x} } \right)
\end{displaymath}
where $\kappa \ge 0$, $\left \Vert \vec{\mu} \right \Vert =1$, and the normalization constant $C_{p}(\kappa)$ is equal to
\begin{displaymath}
  C_{p}(\kappa)=\frac {\kappa^{p/2-1}} {(2\pi)^{p/2}I_{p/2-1}(\kappa)}. \,
\end{displaymath}
where $I_v$ denotes the modified Bessel function of the first kind and order $v$. If $p = 3$ (Fig.~\ref{fig:vMFd}), the normalization constant reduces to
\begin{displaymath}
    C_{3}(\kappa)=\frac {\kappa} {4\pi\sinh \kappa}=\frac {\kappa} {2\pi(e^{\kappa}-e^{-\kappa})}.
\end{displaymath}
The parameters $\vec{\mu}$ and $\kappa$ are called the mean direction and concentration parameter, respectively. The greater the value of $\kappa$, the higher the concentration of the distribution around the mean direction $\vec{\mu}$. The distribution is unimodal for $\kappa > 0$, and is uniform on the sphere for $\kappa = 0$.
\begin{figure}
 \centering
 \includegraphics[width = 0.75\textwidth]{712px-Von_mises_fisher}
\caption{Von Mises-Fisher distribution. Points sampled from three von Mises-Fisher distributions on the sphere (blue: $\kappa = 1$, green: $\kappa  = 10$, red: $\kappa = 100$). The mean directions $\vec{\mu}$ are shown with arrows.}
\label{fig:vMFd}
\end{figure}

The von Mises-Fisher distribution for $p = 3$, also called the Fisher distribution, was first used to model the interaction of dipoles in an electric field. Other applications are found in geology, bioinformatics, and text mining.

\paragraph{Normalization of the vMF distribution.} With angular coordinates and the reference axis along $+z$, we have
\begin{align*}
   f_{3}(\theta, \phi; \kappa) 
  & = 
 \frac{\kappa} {2\pi(e^{\kappa}-e^{-\kappa})} \exp\left( \kappa \cos\theta\right).
\end{align*}
Integration over the unit sphere thus yields
\begin{align*}
   \int_{0}^{2 \pi} \int_{0}^{\pi} f_{3}(\theta, \phi; \kappa) \sin\theta \,\mathrm{d}\theta \,\mathrm{d}\phi
  & = 
  \frac{\kappa} {2\pi(e^{\kappa}-e^{-\kappa})}
  \int_{0}^{2 \pi} \int_{0}^{\pi} \exp\left( \kappa \cos\theta\right) \sin\theta \,\mathrm{d}\theta \,\mathrm{d}\phi
 \\
 & = 
  \frac{\kappa}{(e^{\kappa}-e^{-\kappa})}
  \int_{0}^{\pi} \exp\left( \kappa \cos\theta\right) \sin\theta \,\mathrm{d}\theta
 \\
 & = 
  \frac{\kappa}{(e^{\kappa}-e^{-\kappa})}
  \int_{-1}^{+1} \exp\left( \kappa u\right) \,\mathrm{d}u
 \\
 & = 
  \frac{\kappa}{(e^{\kappa}-e^{-\kappa})}
  \frac{1}{\kappa} (e^{\kappa}-e^{-\kappa})
 \\
 & = 1.
\end{align*}

\paragraph{Paramagnetic limit.} A paramagnetic smaple is decribed for $\kappa = 0$. Therefore, using l'Hospital's rule:
\begin{align*}
  \lim_{\kappa \to 0} f_{3}(\theta, \phi; \kappa) 
  & = 
 \lim_{\kappa \to 0}
  \frac{\kappa  \exp\left( \kappa \cos\theta\right)}{2\pi(e^{\kappa}-e^{-\kappa})}
  \\
  & = 
 \frac{1}{2\pi}
 \lim_{\kappa \to 0}
  \frac{\exp\left( \kappa \cos\theta\right) + \kappa  \cos\theta \exp\left( \kappa \cos\theta\right)}{e^{\kappa}+e^{-\kappa}}
  \\
  & = 
 \frac{1}{2\pi}
  \frac{1}{2}
  \\
  & = 
 \frac{1}{4\pi}.
\end{align*}


\section{Integration on the unit sphere}
An integral
\begin{displaymath}
 I = \int f(\theta, \phi) \,\mathrm{d}\Omega
   = \int_{0}^{2 \pi} \int_{0}^{\pi} f(\theta, \phi) \,\sin\theta \,\mathrm{d}\theta \,\mathrm{d}\phi
\end{displaymath}
over the unit sphere can be evaluated approximately by Gaussian integration (see Ref.~\cite{Atkinson82}),
\begin{displaymath}
 I_{m} =
 \frac{\pi}{m}
 \sum_{j = 1}^{2 m} \sum_{i = 1}^{m} w_{i} f(\theta_{i}, \phi_{j}).
\end{displaymath}
The set $\{ \theta_{i} \}$ is chosen that $\{ \cos\theta_{i} \}$ and $w_{i}$ are Gauss-Legendre nodes (Gaussian mesh points) and weights on the interval $[-1, +1]$.\footnote{These are computed by \textsf{gauleg()} in \textsf{gauss.C}} Explicitly, $\theta_{i} = \arccos x_{i}$ for each mesh point $x_{i}$. The set $\{ \phi_{j} \}$ is evenly spaced,
\begin{displaymath}
 \phi_{j} = \frac{\pi}{m} j.
\end{displaymath}
$I_{m}$ integrates exactly any polynomial $f(x, y ,z)$ of degree less than $2 m$.


\bibliography{short,refs}
\bibliographystyle{unsrt}

\end{document}
