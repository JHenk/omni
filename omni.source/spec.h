// Flags job specification (mode, j.spec)
#define J_PHASE          -5     // Phase shifts (no SOC)
#define J_LEED_COEF      -4     // LEED coefficients
#define J_SCAT_MAT       -3     // Single-site scattering matrices
#define J_C_BAND         -2     // Complex band structure
#define J_RE_BAND        -1     // Real band structure
#define J_VLEED           0     // Very low energy electron diffraction
#define J_SEE             1     // Secondary electron emission
#define J_PE_PENDRY       2     // Photoemission (Pendry type, PEOVER)
#define J_LDOS_SD         3     // Spectral density
#define J_PE_GREEN        4     // Photoemission (Green function)
#define J_PE_CIS          5     // Photoemission, constant initial state
#define J_PE_CFS          6     // Photoemission, constant final state
#define J_PE_CORE         7     // Photoemission from core levels
#define J_LDOS_TOT        8     // Density of states
#define J_LDOS_CORE       9     // Density of states, core levels
#define J_LDOS_DEF       10     // Spectral density, system with defects
#define J_LDOS_RS        12     // Spectral density (real-space mode)
#define J_ELDENS         13     // Electron density, k-resolved
#define J_ELDENS_TOT     14     // Electron density, k-averaged
#define J_BWT            15     // Bloch-wave transmission
#define J_BWT_SMO        16     // Bloch-wave transmission (smooth barrier)
#define J_PE_CORE_CIS    18     // Photoemission from core levels, constant initial state
#define J_BWT_GF         19     // Bloch-wave transmission (Green function)
#define J_BWT_GF_NOME    20     // Bloch-wave transmission (Green function, no matrix elements)
#define J_BWT_GF_NOME_RS 21     // Bloch-wave transmission (Green function, no matrix elements, real space)
#define J_DOS            22     // Density of states (summed over all atoms of a layer)
#define J_BWT_SC         23     // Bloch-wave transmission (supercell mode)
#define J_EXC_RS         24     // Exchange parameters (Liechtenstein formula, real-space, no SOC)
#define J_DM_RS          25     // Exchange matrix (Dzyaloshinski-Moriya, real-space, with SOC)
#define J_PE_TIME        26     // Time-resolved 1PPE, based on J_PE_GREEN
#define J_WEISS          27     // Weiss fields
#define J_DAMPING_RS     28     // Gilbert damping tensor (real space)
#define J_SIGMA_ELPH     29     // Electron self-energy due to phonons

// Flags for disorder type (j.dis)
#define DIS_NO            0     // No disorder
#define DIS_VCA           1     // Virtual crystal approximation
#define DIS_ATA           2     // Averaged t-matrix approximation
#define DIS_CPA           3     // Coherent potential approximation for binary alloys
#define DIS_ATA3          4     // Averaged t-matrix approximation for ternary alloys
#define DIS_CPA3          5     // Coherent potential approximation for ternary alloys
#define DIS_ATADLM        6     // Averaged t-matrix approximation, relativistic disordered local moments
#define DIS_CPADLM        7     // Coherent potential approximation, relativistic disordered local moments

// Flags for surface and interface barrier types 
#define BARR_FILE        -1     // Barrier profile read from file
#define BARR_NO	          0     // No barrier
#define BARR_STEP         1     // Step-shaped barrier
#define BARR_JJJ          2     // Jones-Jennings-Jepsen shape
#define BARR_HS           3     // Henk-Schattke shape
#define BARR_STEP_MAG     4     // Step-shaped barrier, magnetic
#define BARR_JJJ_MAG      5     // Jones-Jennings-Jepsen shape, magnetic
#define BARR_HS_MAG       6     // Henk-Schattke shape, magnetic
#define BARR_JJJ_C        7     // Jones-Jennings-Jepsen shape, corrugated
#define BARR_HS_C         8     // Henk-Schattke shape, corrugated
#define BARR_JJJ_MAG_C    9     // Jones-Jennings-Jepsen shape, corrugated and magnetic
#define BARR_HS_MAG_C    10     // Henk-Schattke shape, corrugated and magnetic
#define BARR_TOTREF      11     // Totally reflecting barrier
#define BARR_TAM         12     // Tamura shape (= Rundgren-Malmstreom shape)
#define BARR_TAM_MAG     13     // Tamura shape, magnetic
#define BARR_HS_MAG_IF   14     // Henk-Schattke shape, for interface
#define BARR_REFRACTING  15     // Step-shaped barrier, non-reflecting but refracting
#define BARR_CORRUGATED  16     // General corrugated smooth barrier

// Flags for k_parallel or angular mode
#define K_ANG             0     // Angular mode
#define K_PAR             1     // Wavevector mode using reciprocal lattice vectors
#define K_PARCART         2     // Wavevector mode using Cartesian reciprocal basis
#define K_ANGTILT         3     // Wavevector mode using sample tilting
#define K_LINCART         4     // Wavevector mode using a line in Cartesian reciprocal basis
#define K_ANG_SD          5     // Angular mode, for angular scans of band structure and spectral density; uses photon energy
#define K_ANGTILT2        6     // Wavevector mode using sample tilting, alternative version

// Flags for fixing the photon angles (only photoemission modes)
#define PHOTON_SAMPLE     0     // Angles w.r.t. to the sample
#define PHOTON_ELECTRON   1     // Angles w.r.t. to the electron detection direction
#define PHOTON_TILT       2     // Sample tilting (set only for K_ANGTILT mode)
#define PHOTON_ELECTRON2  3     // Angles w.r.t. to the electron detection direction, alternative version

// Flags for symmetry expansion type
#define SYMM_KM           0     // Kappa-mu
#define SYMM_CLMS         1     // Complex spherical harmonics
#define SYMM_RLMS         2     // Real spherical harmonics
#define SYMM_SLMS         3     // Special spherical harmonics (for point groups m and 3m)

// Flags for reference frame
#define FRAME_GLOBAL      0     // Global frame (z-axis normal to the layers)
#define FRAME_LOCAL       1     // Local frame (z-axis specified by magnetization direction)

// Flags for the charge density calculations (j.spherical)
#define CD_SPHERICAL      0     // Spherical charge density
#define CD_NONSPHERICAL   1     // Nonspherical charge density
#define CD_MULTIPOL       2     // Multipole expansion of the charge density

// Flags for k-averaging of the Green function
#define V_DIAGONAL        0     // Only layer- or site-diagonal components computed
#define V_ALL             1     // All components computed

// Flags for k-averaging modes
#define KAVER_SP          0     // Special points
#define KAVER_LIN         1     // Linear cascading meshes, adaptive
#define KAVER_TRI         2     // Triangular integration, adaptive
#define KAVER_TRITWO      3     // Triangular integration, alternative, adaptive
#define KAVER_GL          4     // Gauss-Legendre quadrature on triangles
#define KAVER_GLPAR       5     // Gauss-Legendre quadrature on a parallelogram

// Flags for type of optical potential
#define OP_POWLAW         0     // Power law for energy dependence
#define OP_INOUE          1     // Energy dependence proposed by Inoue
#define OP_DUISBURG       2     // Energy dependence from Duisburg

// Flags for defects
#define ISDEFECT          1     // Site is a defect
#define ISNODEFECT        0     // Site is not a defect
