// waves.h
//
// Definitikon for wavefunctions
//

#ifndef _WAVES_
#define _WAVES_

#include "complex.h"
#include "function.h"

struct waveelement{
  complex f1;
  complex g1;
  complex f2;
  complex g2;
  // Die Konstruktoren
  waveelement();
  // Der Destruktor
  ~waveelement();
  // Die Zuweisung
  waveelement &operator=(const waveelement &);
};

inline	waveelement::waveelement(){
}

inline	waveelement::~waveelement(){
}

//
// Assignment
//
inline waveelement &waveelement::operator=(const waveelement &a){
  f1 = a.f1;
  g1 = a.g1;
  f2 = a.f2;
  g2 = a.g2;
  
  return *this;
}

struct wavefunction{
  complexfunction	f1;
  complexfunction	g1;
  complexfunction	f2;
  complexfunction	g2;
  // Die Konstruktoren
  wavefunction();
  wavefunction(int);
  // Der Destruktor
  ~wavefunction();
  // Setze die Zahl der Stuetzstellen
  void setsize(int);
};

//
// Erzeuge einen Platzhalter fuer eine Wellenfunktion
//
inline	wavefunction::wavefunction(){
}

inline	wavefunction::wavefunction(int n) : f1(n), g1(n), f2(n), g2(n)
{
}

inline	wavefunction::~wavefunction()  
{
}

inline	void wavefunction::setsize(int n){
  f1.setsize(n);
  g1.setsize(n);
  f2.setsize(n);
  g2.setsize(n);
}

#endif
