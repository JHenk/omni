#include <string>
#include <sstream>
#include <vector>

#include "state.h"
#include "iostuff.h"

using namespace std;

template<class T>
std::ostream& operator<< (std::ostream& stream, const std::vector<T>& vec)
{
  typename std::vector<T>::const_iterator it;
  for(it=vec.begin(); it<vec.end(); it++)
    stream << *it << std::endl;
  return stream;
}


ostream& operator<< (ostream& out, const wavefunction &wave)
{
  int n;
  
  n = wave.f1.getsize();
  out << "f1 " << n << endl;
  out << wave.f1 << endl;

  n = wave.g1.getsize();
  out << "g1 " << n << endl;
  out << wave.g1 << endl;

  n = wave.f2.getsize();
  out << "f2 " << n << endl;
  out << wave.f2 << endl;

  n = wave.g2.getsize();
  out << "g2 " << n << endl;
  out << wave.g2 << endl;

  return out;
}



ostream& operator<< (ostream& out, const selem &elem)
{
  int n, r, c;

  n = elem.wr.size();
  out << "wr " << n << endl;
  out << elem.wr << endl;

  n = elem.wir.size();
  out << "wir " << n << endl;
  out << elem.wir << endl;
  
  n = elem.wii.size();
  out << "wii " << n << endl;
  out << elem.wii << endl;
  
  n = elem.wfrr.getsize();
  out << "wfrr " << n << endl;
  out << elem.wfrr << endl;

  n = elem.wgrr.getsize();
  out << "wgrr " << n << endl;
  out << elem.wgrr << endl;

  n = elem.wfrir.getsize();
  out << "wfrir " << n << endl;
  out << elem.wfrir << endl;

  n = elem.wgrir.getsize();
  out << "wgrir " << n << endl;
  out << elem.wgrir << endl;

  n = elem.wfrii.getsize();
  out << "wfrii " << n << endl;
  out << elem.wfrii << endl;

  n = elem.wgrii.getsize();
  out << "wgrii " << n << endl;
  out << elem.wgrii << endl;

  n = elem.wflr.getsize();
  out << "wflr " << n << endl;
  out << elem.wflr << endl;

  n = elem.wglr.getsize();
  out << "wglr " << n << endl;
  out << elem.wglr << endl;

  n = elem.wflir.getsize();
  out << "wflir " << n << endl;
  out << elem.wflir << endl;

  n = elem.wglir.getsize();
  out << "wglir " << n << endl;
  out << elem.wglir << endl;

  n = elem.wflii.getsize();
  out << "wflii " << n << endl;
  out << elem.wflii << endl;

  n = elem.wglii.getsize();
  out << "wglii " << n << endl;
  out << elem.wglii << endl;
  
  out << "e " << endl;
  out << elem.e << endl << endl;

  out << "k " << endl;
  out << elem.k << endl << endl;

  elem.T.getsize(r, c);
  out << "T " << n << endl;
  out << elem.T << endl;

  elem.Rr11.getsize(r, c);
  out << "Rr11 " << r << " " << c << endl;
  out << elem.Rr11 << endl;

  elem.Rr12.getsize(r, c);
  out << "Rr12 " << r << " " << c << endl;
  out << elem.Rr12 << endl;

  elem.Rr21.getsize(r, c);
  out << "Rr21 " << r << " " << c << endl;
  out << elem.Rr21 << endl;

  elem.Rr22.getsize(r, c);
  out << "Rr22 " << r << " " << c << endl;
  out << elem.Rr22 << endl;

  elem.Ir11.getsize(r, c);
  out << "Ir11 " << r << " " << c << endl;
  out << elem.Ir11 << endl;

  elem.Ir12.getsize(r, c);
  out << "Ir12 " << r << " " << c << endl;
  out << elem.Ir12 << endl;

  elem.Ir21.getsize(r, c);
  out << "Ir21 " << r << " " << c << endl;
  out << elem.Ir21 << endl;

  elem.Ir22.getsize(r, c);
  out << "Ir22 " << r << " " << c << endl;
  out << elem.Ir22 << endl;

  elem.Ii.getsize(r, c);
  out << "Ii " << r << " " << c << endl;
  out << elem.Ii << endl;

  elem.Drp.getsize(r, c);
  out << "Drp " << r << " " << c << endl;
  out << elem.Drp << endl;

  elem.Drz.getsize(r, c);
  out << "Drz " << r << " " << c << endl;
  out << elem.Drz << endl;

  elem.Drm.getsize(r, c);
  out << "Drm " << r << " " << c << endl;
  out << elem.Drm << endl;

  elem.Dlp.getsize(r, c);
  out << "Dlp " << r << " " << c << endl;
  out << elem.Dlp << endl;

  elem.Dlz.getsize(r, c);
  out << "Dlz " << r << " " << c << endl;
  out << elem.Dlz << endl;

  elem.Dlm.getsize(r, c);
  out << "Dlm " << r << " " << c << endl;
  out << elem.Dlm << endl;

  elem.Dpp.getsize(r, c);
  out << "Dpp " << r << " " << c << endl;
  out << elem.Dpp << endl;

  elem.Dpz.getsize(r, c);
  out << "Dpz " << r << " " << c << endl;
  out << elem.Dpz << endl;

  elem.Dpm.getsize(r, c);
  out << "Dpm " << r << " " << c << endl;
  out << elem.Dpm << endl;

  elem.Dzp.getsize(r, c);
  out << "Dzp " << r << " " << c << endl;
  out << elem.Dzp << endl;

  elem.Dzz.getsize(r, c);
  out << "Dzz " << r << " " << c << endl;
  out << elem.Dzz << endl;

  elem.Dzm.getsize(r, c);
  out << "Dzm " << r << " " << c << endl;
  out << elem.Dzm << endl;

  elem.Dmp.getsize(r, c);
  out << "Dmp " << r << " " << c << endl;
  out << elem.Dmp << endl;

  elem.Dmz.getsize(r, c);
  out << "Dmz " << r << " " << c << endl;
  out << elem.Dmz << endl;

  elem.Dmm.getsize(r, c);
  out << "Dmm " << r << " " << c << endl;
  out << elem.Dmm << endl;

  elem.TMr.getsize(r, c);
  out << "TMr " << r << " " << c << endl;
  out << elem.TMr << endl;

  elem.TMi.getsize(r, c);
  out << "TMi " << r << " " << c << endl;
  out << elem.TMi << endl;

  return out;
}

ostream& operator<< (ostream& out, const satom &atom)
{
  int r, c;

  atom.Dr.getsize(r, c);
  out << "Dr " << r << " " << c << endl;
  out << atom.Dr << endl;

  atom.Dl.getsize(r, c);
  out << "Dl " << r << " " << c << endl;
  out << atom.Dl << endl;

  atom.D.getsize(r, c);
  out << "D " << r << " " << c << endl;
  out << atom.D << endl;

  return out;
}

ostream& operator<< (ostream& out, const sslab &slab)
{
  int n, r, c;
  
  out << "opt" << endl;
  out << slab.opt << endl << endl;

  out << "e" << endl;
  out << slab.e << endl << endl;

  out << "k" << endl;
  out << slab.k << endl << endl;

  n = slab.kz.size();
  out << "kz " << n << endl;
  out << slab.kz << endl;
  
  slab.Wp.getsize(r, c);
  out << "Wp " << r << " " << c << endl;
  out << slab.Wp << endl;

  slab.Wm.getsize(r, c);
  out << "Wm " << r << " " << c << endl;
  out << slab.Wm << endl;

  slab.Vp.getsize(r, c);
  out << "Vp " << r << " " << c << endl;
  out << slab.Vp << endl;

  slab.Vm.getsize(r, c);
  out << "Vm " << r << " " << c << endl;
  out << slab.Vm << endl;

  slab.Wap.getsize(r, c);
  out << "Wap " << r << " " << c << endl;
  out << slab.Wap << endl;

  slab.Wam.getsize(r, c);
  out << "Wam " << r << " " << c << endl;
  out << slab.Wam << endl;

  slab.Wbp.getsize(r, c);
  out << "Wbp " << r << " " << c << endl;
  out << slab.Wbp << endl;

  slab.Wbm.getsize(r, c);
  out << "Wbm " << r << " " << c << endl;
  out << slab.Wbm << endl;

  slab.Wcp.getsize(r, c);
  out << "Wcp " << r << " " << c << endl;
  out << slab.Wcp << endl;

  slab.Wcm.getsize(r, c);
  out << "Wcm " << r << " " << c << endl;
  out << slab.Wcm << endl;

  slab.Wdp.getsize(r, c);
  out << "Wdp " << r << " " << c << endl;
  out << slab.Wdp << endl;

  slab.Wdm.getsize(r, c);
  out << "Wdm " << r << " " << c << endl;
  out << slab.Wdm << endl;
  
  n = slab.atom.size();
  out << "atom " << n << endl;
  out << slab.atom << endl;

  slab.T.getsize(r, c);
  out << "T " << r << " " << c << endl;
  out << slab.T << endl;

  slab.S.getsize(r, c);
  out << "S " << r << " " << c << endl;
  out << slab.S << endl;

  slab.O.getsize(r, c);
  out << "O " << r << " " << c << endl;
  out << slab.O << endl;

  slab.Mpp.getsize(r, c);
  out << "Mpp " << r << " " << c << endl;
  out << slab.Mpp << endl;

  slab.Mpm.getsize(r, c);
  out << "Mpm " << r << " " << c << endl;
  out << slab.Mpm << endl;

  slab.Mmp.getsize(r, c);
  out << "Mmp " << r << " " << c << endl;
  out << slab.Mmp << endl;

  slab.Mmm.getsize(r, c);
  out << "Mmm " << r << " " << c << endl;
  out << slab.Mmm << endl;

  slab.Dr.getsize(r, c);
  out << "Dr " << r << " " << c << endl;
  out << slab.Dr << endl;

  slab.Dl.getsize(r, c);
  out << "Dl " << r << " " << c << endl;
  out << slab.Dl << endl;

  slab.D.getsize(r, c);
  out << "D " << r << " " << c << endl;
  out << slab.D << endl;

  slab.Z.getsize(r, c);
  out << "Z " << r << " " << c << endl;
  out << slab.Z << endl;

  slab.TMr.getsize(r, c);
  out << "TMr " << r << " " << c << endl;
  out << slab.TMr << endl;

  slab.TMi.getsize(r, c);
  out << "TMi " << r << " " << c << endl;
  out << slab.TMi << endl;

  return out;
}

ostream& operator<< (ostream& out, const sslice &slice)
{
  int r, c;

  slice.Kp.getsize(r, c);
  out << "Kp " << r << " " << c << endl;
  out << slice.Kp << endl;

  slice.Km.getsize(r, c);
  out << "Km " << r << " " << c << endl;
  out << slice.Km << endl;

  slice.Rmp.getsize(r, c);
  out << "Rmp " << r << " " << c << endl;
  out << slice.Rmp << endl;

  slice.Rpm.getsize(r, c);
  out << "Rpm " << r << " " << c << endl;
  out << slice.Rpm << endl;

  slice.Z.getsize(r, c);
  out << "Z " << r << " " << c << endl;
  out << slice.Z << endl;

  slice.Up.getsize(r, c);
  out << "Up " << r << " " << c << endl;
  out << slice.Up << endl;

  slice.Um.getsize(r, c);
  out << "Um " << r << " " << c << endl;
  out << slice.Um << endl;

  slice.Vp.getsize(r, c);
  out << "Vp " << r << " " << c << endl;
  out << slice.Vp << endl;

  slice.Vm.getsize(r, c);
  out << "Vm " << r << " " << c << endl;
  out << slice.Vm << endl;

  slice.E.getsize(r, c);
  out << "E " << r << " " << c << endl;
  out << slice.E << endl;


  return out;
}

ostream& operator<< (ostream& out, const scell &cell)
{
  int n, r, c;

  out << "cind " << endl;
  out << cell.cind << endl << endl;;

  out << "n1 " << endl;
  out << cell.n1 << endl << endl;

  out << "n2 " << endl;
  out << cell.n2 << endl << endl;

  n = cell.V.size();
  out << "V " << n << endl;
  out << cell.V << endl;
  
  cell.T.getsize(r, c);
  out << "T " << r << " " << c << endl;
  out << cell.T << endl;
  
  return out;
}

ostream& operator<< (ostream& out, const slayer &layer)
{
  int n, r, c;

  layer.Rpm.getsize(r, c);
  out << "Rpm " << r << " " << c << endl;
  out << layer.Rpm << endl;

  n = layer.V.size();
  out << "V " << n << endl;
  out << layer.V << endl;

  layer.Z.getsize(r, c);
  out << "Z " << r << " " << c << endl;
  out << layer.Z << endl;

  layer.A0.getsize(r, c);
  out << "A0 " << r << " " << c << endl;
  out << layer.A0 << endl;

  layer.A.getsize(r, c);
  out << "A " << r << " " << c << endl;
  out << layer.A << endl;

  layer.B0.getsize(r, c);
  out << "B0 " << r << " " << c << endl;
  out << layer.B0 << endl;

  layer.B.getsize(r, c);
  out << "B " << r << " " << c << endl;
  out << layer.B << endl;

  layer.Ap.getsize(r, c);
  out << "Ap " << r << " " << c << endl;
  out << layer.Ap << endl;

  layer.Am.getsize(r, c);
  out << "Am " << r << " " << c << endl;
  out << layer.Am << endl;

  layer.Bm.getsize(r, c);
  out << "Bm " << r << " " << c << endl;
  out << layer.Bm << endl;

  layer.Dp.getsize(r, c);
  out << "Dp " << r << " " << c << endl;
  out << layer.Dp << endl;

  layer.Dm.getsize(r, c);
  out << "Dm " << r << " " << c << endl;
  out << layer.Dm << endl;

  n = layer.cell.size();
  out << "cell " << n << endl;
  out << layer.cell << endl;

  return out;
}


ostream& operator<< (ostream &out, const state &s)
{
  int r, c,  n;

  //first the easy things
  out << "flagw" << endl;
  out << s.flagw << endl << endl;

  out << "ev" << endl;
  out << s.ev << endl << endl;

  n = s.elem.size();
  out << "elem " << n << endl;
  out << s.elem << endl;

  n =  s.slab.size();
  out << "slab " << n << endl; 
  out << s.slab << endl;

  n = s.slice.size();
  out << "slice " << n << endl;
  out << s.slice << endl;

  n = s.layer.size();
  out << "layer " << n << endl;
  out << s.layer << endl;

  s.Mpp.getsize(r, c);
  out << "Mpp " << r << " " << c << endl;
  out << s.Mpp << endl;
  
  s.Mpm.getsize(r, c);
  out << "Mpm " << r << " " << c << endl;
  out << s.Mpm << endl;

  s.Mmp.getsize(r, c);
  out << "Mmp " << r << " " << c << endl;
  out << s.Mmp << endl;

  s.Mmm.getsize(r, c);
  out << "Mmm " << r << " " << c << endl;
  out << s.Mmm << endl;

  s.Rmp.getsize(r, c);
  out << "Rmp " << r << " " << c << endl;
  out << s.Rmp << endl;

  s.Rpm.getsize(r, c);
  out << "Rpm " << r << " " << c << endl;
  out << s.Rpm << endl;

  s.Rtot.getsize(r, c);
  out << "Rtot " << r << " " << c << endl;
  out << s.Rtot << endl;

  s.R2tot.getsize(r, c);
  out << "R2tot " << r << " " << c << endl;
  out << s.R2tot << endl;

  s.Rbmp.getsize(r, c);
  out << "Rbmp " << r << " " << c << endl;
  out << s.Rbmp << endl;

  s.Rbpm.getsize(r, c);
  out << "Rbpm " << r << " " << c << endl;
  out << s.Rbpm << endl;

  s.Rb2mp.getsize(r, c);
  out << "Rb2mp " << r << " " << c << endl;
  out << s.Rb2mp << endl;

  s.Rb2pm.getsize(r, c);
  out << "Rb2pm " << r << " " << c << endl;
  out << s.Rb2pm << endl;

  s.E.getsize(r, c);  //diagonalmatrix
  out << "E " << r <<  endl;
  out << s.E << endl;

  s.V.getsize(r, c);
  out << "V "     << r << " " << c << endl;
  out << s.V << endl;

  s.M2mp.getsize(r, c);
  out << "M2mp "  << r << " " << c << endl;
  out << s.M2mp << endl;

  s.Esc.getsize(r, c);
  out << "Esc "  << r << " " << c << endl;
  out << s.Esc << endl;

  s.Vsc.getsize(r, c);
  out << "Vsc "  << r << " " << c << endl;
  out << s.Vsc << endl;

  return out;
}

// void state::read(std::istream &in)
// {

// }

