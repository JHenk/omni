// state.h
//
// Definition of an electronic state
//

#ifndef	_STATE_
#define	_STATE_

#include "complex.h"
#include "cmatrix.h"
#include "csmatrix.h"
#include "waves.h"
#include "cmatfunc.h"
#include "jparallel.h"
#include <vector>

// Definition der Flagwerte
#define	NEEDSTATE	0x0001		// Zustand soll berechnet werden
#define	RWAVES		0x0002		// reg. Wellenf. berechnen
#define	IWAVES		0x0004		// ireg. Wellenf. berechnen
#define	RTOTMATRIX	0x0008		// Rtot Matrix berechnen
#define	RMPMATRIX	0x0010		// R-+ Matrix berechnen
#define	RPMMATRIX	0x0020		// R+- Matrix berechnen
#define	EMATRIX		0x0040		// Eigenvektoren speichern
#define	RMATRIX		0x0080		// R Matrizen speichern
#define	REVERSEB	0x0100		// Zeitumgekehrter Zustand
#define	REVERSEK	0x0200		// k-parallel umdrehen
#define	ISUPPER		0x0400		// ist oberer Zustand

#ifndef	NULL
#define	NULL	0
#endif

struct selem{
  std::vector<wavefunction> wr;  // regulaere Loesung
  std::vector<wavefunction> wir; // irregulaere Loesung regulaerer Teil
  std::vector<wavefunction> wii; // irregulaere Loesung irregulaerer Teil
  complexmatrixfunction wfrr, wgrr;     // regulaere Loesung
  complexmatrixfunction wfrir, wgrir;   // irregulaere Loesung regulaerer Teil
  complexmatrixfunction wfrii, wgrii;   // irregulaere Loesung irregulaerer Teil
  complexmatrixfunction wflr, wglr;     // regulaere Loesung
  complexmatrixfunction wflir, wglir;   // irregulaere Loesung regulaerer Teil
  complexmatrixfunction wflii, wglii;   // irregulaere Loesung irregulaerer Teil
  complex		e;		// Die komplexe	Energie [in Hartree]
  complex		k;		// Betrag des k-Vektors
  complexmatrix	T;		// atomare Streumatrix
  complexmatrix	Rr11;		// regulaere Integrale k1,k2
  complexmatrix	Rr12;		// regulaere Integrale k1,-k2-1
  complexmatrix	Rr21;		// regulaere Integrale -k1-1,k2
  complexmatrix	Rr22;		// regulaere Integrale -k1-1,-k2-1
  complexmatrix	Ir11;		// regulaere Integrale k1,k2
  complexmatrix	Ir12;		// regulaere Integrale k1,-k2-1
  complexmatrix	Ir21;		// regulaere Integrale -k1-1,k2
  complexmatrix	Ir22;		// regulaere Integrale -k1-1,-k2-1
  complexmatrix	Ii;		// irregulaere Integrale
  complexmatrix	Drp, Drz, Drm;	// rechtsseitige Matrixelemente
  complexmatrix	Dlp, Dlz, Dlm;	// linksseitige	Matrixelemente
  complexmatrix	Dpp, Dpz, Dpm;	// atomare Matrixelemente
  complexmatrix	Dzp, Dzz, Dzm;	// atomare Matrixelemente
  complexmatrix	Dmp, Dmz, Dmm;	// atomare Matrixelemente
  complexmatrix	TMr;		// regular tunneling matrix
  complexmatrix	TMi;		// irregular tunneling matrix
};

struct satom{
  selem		*elem;		// Zeiger auf das zugehoerige Element
  complexmatrix	Dr;		// rechtsseitige Matrixelemente
  complexmatrix	Dl;		// linksseitige	Matrixelemente
  complexmatrix	D;		// atomare Matrixelemente
};

struct sslab{
  complex		opt;		// Das tatsaechliche opt Potential
  complex		e;		// Die komplexe	Energie [in Hartree]
  complex		k;		// Betrag des k-Vektors
  std::vector<complex>  kz;		// Speicher fuer die z-Komponente
					// des k-Vektors
  complexmatrix	Wp, Wm;		// Matrizen zur Transformation von
  // ebenen Wellen in Kugelwellen
  complexmatrix	Vp, Vm;		// Matrizen zur Transformation von
  // Kugelwellen in ebene Wellen
  complexsupermatrix   Wap, Wam;	// Matrizen zur Transformation von
  // ebenen Wellen in Kugelwellen
  complexsupermatrix   Wbp, Wbm;	// Matrizen zur Transformation von
  // Kugelwellen in ebene Wellen
  complexsupermatrix   Wcp, Wcm;	// Matrizen zur Transformation in
  // Kugelwellen mit Vielfachstreuung
  complexsupermatrix   Wdp, Wdm;	// Matrizen zur Transformation in
  // ebene Wellen mit Streumatrix
  std::vector<satom>   atom;		// Daten der Atome
  complexsupermatrix   T;		// atomare Streumatrizen
  complexsupermatrix   S;		// Strukturkonstante
  complexsupermatrix   O;		// Enthaelt (1-TS)^-1
  complexmatrix	Mpp;		// M++ Matrix
  complexmatrix	Mpm;		// M+- Matrix
  complexmatrix	Mmp;		// M-+ Matrix
  complexmatrix	Mmm;		// M-- Matrix
  complexsupermatrix   Dr;		// rechtsseitige Matrixelemente
  complexsupermatrix   Dl;		// linksseitige	Matrixelemente
  complexsupermatrix   D;		// atomare Matrixelemente
  complexsupermatrix   Z;		// Zur besonderen Verwendung
  complexsupermatrix   TMr;		// regular tunneling matrix
  complexsupermatrix   TMi;		// irregular tunneling matrix
};

struct sslice{
  sslab		     *slab;		// Zeiger auf zugehoerige Scheibe
  complexdiagmatrix   Kp;		// K+ Phasor
  complexdiagmatrix   Km;		// K- Phasor
  complexmatrix	      Rmp;		// R-+ Matrix
  complexmatrix	      Rpm;		// R-+ Matrix
  complexmatrix	      Z;		// Zur besonderen Verwendung
  complexmatrix	      Up, Um;		// Entwicklung der Baender
  complexmatrix	      Vp, Vm;		// Entwicklung der Baender
  complexsupermatrix  E;		// Entwicklung der Baender
};

struct scell{
  int                 cind;             // Cell index
  int                 n1;               // Displacement lattice vector 1
  int                 n2;               // Displacement lattice vector 2
  std::vector<complexsupermatrix>  V;   // V matrix of the Green function
  complexsupermatrix  T;                // Single-site scattering matrices
  int                 from;             // Spin dynamics flag
  int                 to;               // Spin dynamics flag
};

struct slayer{
  sslice	      *slice;		// Zeiger auf zugehoerige Schicht
  complexmatrix	       Rpm;		// R+- Matrix
  std::vector<complexsupermatrix>  V;	// V Matrizen
  complexmatrix	       Z;		// Zur besonderen Verwendung
  complexsupermatrix   A0;		// Entwicklungskoeffizienten
  complexsupermatrix   A;		// Entwicklungskoeffizienten
  complexsupermatrix   B0;		// Entwicklungskoeffizienten
  complexsupermatrix   B;		// Entwicklungskoeffizienten
  complexmatrix	       Ap;		// Entwicklungskoeffizienten
  complexmatrix	       Am;		// Entwicklungskoeffizienten
  complexmatrix	       Bm;		// Entwicklungskoeffizienten
  complexmatrix	       Dp;		// Entwicklungskoeffizienten
  complexmatrix	       Dm;		// Entwicklungskoeffizienten
  std::vector<scell>   cell;            // Cells in this layer
};

struct state{
  int		flagw;		// Flags
  complex	ev;		// Energy in Vacuum [in Hartree]
  std::vector<selem> elem;	// Elements
  std::vector<sslab> slab;	// Slabs
  std::vector<sslice> slice;	// Slices
  std::vector<slayer> layer;	// Layers
  complexmatrix Mpp;		// M++ matrix of the surface barrier
  complexmatrix Mpm;		// M+- matrix of the surface barrier
  complexmatrix Mmp;		// M-+ matrix of the surface barrier
  complexmatrix Mmm;		// M-- matrix of the surface barrier
  complexmatrix	Rmp;		// Reflection matrix surface barrier
  complexmatrix	Rpm;		// Reflection matrix 2nd surface barrier
  complexmatrix	Rtot;		// Total reflection matrix (surface side)
  complexmatrix	R2tot;		// Total reflection matrix (bulk side)
  complexmatrix	Rbmp;		// Bulk reflection matrix
  complexmatrix	Rbpm;		// Bulk reflection matrix
  complexmatrix	Rb2mp;		// Bulk reflection matrix (surface side)
  complexmatrix	Rb2pm;		// Bulk reflection matrix (surface side)
  complexdiagmatrix E;		// Eigenvalues
  complexmatrix	V;		// Eigenvectors
  complexmatrix M2mp;		// M-+ matrix of the 2. surface barrier (bulk side)
  complexsupermatrix Esc;         // Container matrix for eigenvalues, supercell modes
  complexsupermatrix Vsc;         // Container matrix for eigenvectors, supercell modes
};

std::ostream& operator<< (std::ostream &, const state &);


#endif
