// exchangers.h
//
// Exchange paramters
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function
void exchangers(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);

