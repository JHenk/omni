// spectral.h
//
// Bloch spectral function
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void spectral(job &j, crystal &c, crystal &c2, crystal &c3, int lflagw, std::ofstream &out);

