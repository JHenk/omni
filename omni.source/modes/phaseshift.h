// phaseshift.h
#include <iostream>
#include <fstream>

#include "crystal.h"
#include "job.h"
#include "state.h"

// The mode function
void phaseshift(job &j, crystal &c, int lflagw, std::ofstream &out);
