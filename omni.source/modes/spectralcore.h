// spectralcore.h
//
// Bloch spectral function in core-level mode
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void spectralcore(job &j, crystal &c, int lflagw, std::ofstream &out);

