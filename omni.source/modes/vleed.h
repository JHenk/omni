// vleed.h
//
// Very-low energy electron diffraction
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void vleed(job &j, crystal &c, crystal &c2, int uflagw, std::ofstream &out);

