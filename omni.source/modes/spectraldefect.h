// spectraldefect.h
//
// Bloch spectral function in defect mode
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void spectraldefect(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);

