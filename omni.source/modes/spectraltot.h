// spectraltot.h
//
// Bloch spectral function
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void spectraltot(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);

