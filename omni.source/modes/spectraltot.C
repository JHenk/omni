// spectraltot.C
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include "beam.h"
#include "constants.h"
#include "crystal.h"
#include "job.h"
#include "spec.h"
#include "state.h"

#include "actual.h"
#include "connect.h"
#include "ldos.h"
#include "mmat.h"

using namespace std;


// The mode function

void spectraltot(job &j, crystal &c, crystal &c2, int lflagw, ofstream &out){

  actual a;

  unsigned int ieval;

  Logger logger(std::cout);
  if(j.parallel.mpi.rank != j.parallel.mpi.NO_MPI)
    logger.setHeader(string("$LEVEL (proc=$RANK) from spectraltot(): "));
  else
    logger.setHeader(string("$LEVEL from spectraltot(): "));

  connect_actual(a, c);

  // Energy - loop
  for(ieval = 0; ieval < j.general.el.size(); ieval++){

    beamset b;		// Beams
    
    state l;		        // States
    
    // Set actual energy
    a.e = cmplx(j.general.el[ieval].item1, j.general.el[ieval].item2);

    logger << "Calculation for energy ";
    logger << setw (12) << setprecision (6) << a.e * HART;
    logger << " eV" << endl;
    
    // Lower state
    connect (j, l, c, lflagw);
    
    // Energy in vacuum
    l.ev = a.e;
    
    // Calculate single-site t-matrices
    tmat (j, l, c, j.general.x, j.general.t, j.general.optpotl, j.general.spec);
    
    // Integral
    imat (l, c);
    
    // k-averaged spectral density
    tldos(a, j, l, c, out);

    logger << "Done" << endl;
  }
}
