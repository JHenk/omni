// bwt.h
//
// Bloch-wave transmission
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void bwt(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);
