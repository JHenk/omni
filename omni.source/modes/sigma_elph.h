// sigma_elph.h
//
// Bloch spectral function
//

#include <fstream>

#include "job.h"
#include "crystal.h"

// The mode function

void sigma_elph(job &j, std::ofstream &out, crystal &c);

