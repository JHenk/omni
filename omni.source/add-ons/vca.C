//
// Virtual crystal approximation
//
// JH 2014-12-10

#include <iostream>
using namespace std;

#include <fstream>
#include <iomanip>
#include <cmath>

#include "constants.h"
#include "potential.h"

#include "complex.h"
#include "platform.h"

// Main program
int main(int argc, char **argv){
  
  ofstream out;
  ifstream inp;
  
  potential p1,        p2,        p3;
  char      name1[80], name2[80], name3[80];

  double c;
  int    i;
  
  // Print program information
  cout << "Virtual crystal approximation" << endl;
  cout << "- Generates a potential for use with omni" << endl << endl;
  
  
  // Read 1st potential
  cout << "Enter file name of 1st potential." << endl;
  cin >> name1;
  cout << "Enter potential type (ptype == " << POT_NONMAG_SPHERE << " or " << POT_MAG_SPHERE << ")" << endl;
  cin >> p1.ptype;
  switch(p1.ptype){
    case POT_NONMAG_SPHERE:
    case POT_MAG_SPHERE:
      break;
    default:
      cerr << "ERROR from main(): wrong potential type specified." << endl;
      myexit(1);
      break;
  }
  inp.open(name1);
  // Reading header
  inp >> p1.z >> p1.rm >> p1.h >> p1.n >> p1.m >> p1.td >> p1.ef >> p1.lambda;
  // Reading potential data
  p1.v.setsize(p1.n);
  p1.b.setsize(p1.n);
  inp >> p1.v;
  if(p1.ptype == POT_MAG_SPHERE){
    inp >> p1.b;
  }
  inp.close();
  
  
  // Read 2nd potential
  cout << "Enter file name of 2nd potential." << endl;
  cin >> name2;
  cout << "Enter potential type (ptype == " << POT_NONMAG_SPHERE << " or " << POT_MAG_SPHERE << ")" << endl;
  cin >> p2.ptype;
  switch(p2.ptype){
    case POT_NONMAG_SPHERE:
    case POT_MAG_SPHERE:
      break;
    default:
      cerr << "ERROR from main(): wrong potential type specified." << endl;
      myexit(1);
      break;
  }
  inp.open(name2);
  // Reading header
  inp >> p2.z >> p2.rm >> p2.h >> p2.n >> p2.m >> p2.td >> p2.ef >> p2.lambda;
  // Reading potential data
  p2.v.setsize(p2.n);
  p2.b.setsize(p2.n);
  inp >> p2.v;
  if(p2.ptype == POT_MAG_SPHERE){
    inp >> p2.b;
  }
  inp.close();
  

  // Checking consistency
  if(fabs(p1.rm - p2.rm) > EMACH){
    cerr << "ERROR from main(): muffin-tin radii do not match." << endl;
    myexit(1);
  }
  if(fabs(p1.h - p2.h) > EMACH){
    cerr << "ERROR from main(): radial mesh step sizes do not match." << endl;
    myexit(1);
  }
  if(p1.n != p2.n){
    cerr << "ERROR from main(): radial mesh sizes do not match." << endl;
    myexit(1);
  }
  if(p1.ptype != p2.ptype){
    cerr << "ERROR from main(): potential types do not match." << endl;
    myexit(1);
  }
  

  // Mix the potentials
  cout << "Enter concentration [0, ..., 1]" << endl;
  cin >> c;
  if((c < 0.0) || (c > 1.0)){
    cerr << "ERROR from main(): concentration not in [0 ... 1]" << endl;
    myexit(1);
  }
  
  // Handle the header line of the potential file
  p3.z      = c * p1.z      + (1 - c) * p2.z;
  p3.n      = p1.n;
  p3.h      = c * p1.h      + (1 - c) * p2.h;
  p3.rm     = c * p1.rm     + (1 - c) * p2.rm;
  p3.m      = c * p1.m      + (1 - c) * p2.m;
  p3.td     = c * p1.td     + (1 - c) * p2.td;
  p3.ef     = c * p1.ef     + (1 - c) * p2.ef;
  p3.lambda = c * p1.lambda + (1 - c) * p2.lambda;
  // Handle potential type
  p3.ptype  = p1.ptype;

  switch(p1.ptype){
  case POT_NONMAG_SPHERE:
    p3.v  = c * p1.v + (1 - c) * p2.v;
    break;
  case POT_MAG_SPHERE:
    p3.v  = c * p1.v + (1 - c) * p2.v;
    p3.b  = c * p1.b + (1 - c) * p2.b;
    break;
 default:
   cerr << "ERROR from main(): wrong potential type." << endl;
   myexit(1);
   break;
  }    

  // Write the potential
  cout << "Enter file name of the resulting potential." << endl;
  cin >> name3;
  
  out.open(name3);
  out << setiosflags(ios::fixed);
  out << setw(4) << setprecision(2)  << p3.z;
  out << setw(20) << setprecision(10) << p3.rm;
  out << setw(20) << setprecision(10) << p3.h;
  out << setw(20) << setprecision(6) << p3.n;
  out << setw(20) << setprecision(10) << p3.m;
  out << setw(20) << setprecision(10) << p3.td;
  out << setw(20) << setprecision(10) << p3.ef;
  out << setw(20) << setprecision(10) << p3.lambda << endl;
  
  // Write the spin-dependent potentials instead of sum and difference
  for(i = 0; i < p3.n; i++){
    out << setiosflags(ios::scientific) << setw(14) << setprecision(8) << real(p3.v(i)) << endl;
  }
  if(p3.ptype == POT_MAG_SPHERE){
    for(i = 0; i < p3.n; i++){
      out << setiosflags(ios::scientific)<< setw(14) << setprecision(8) << real(p3.b(i)) << endl; 
    }
  }
  out.close();
}
