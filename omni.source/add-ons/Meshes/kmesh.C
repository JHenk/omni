//
// Generation of a symmetry-adapted k-mesh
//
// JH -- 18/5/1999
//

#include <iostream>
using namespace std;

#include <fstream>
#include <iomanip>
#include <math.h>

#include "platform.h"

#include "vector2.h"
#include "operate.h"
#include "complex.h"

#define GAUDI 2.0

struct  tupel{
  int          r;  
  int          s; 
  }; 

struct  kpoint{
  tupel        t;          // Coefficients
  vector2      v;          // the vector itself
  double       n;          // its norm
  double       weight;     // the name says it all
  int          used;       // used flag
  }; 


// Calculate a vector from its coefficients and the base vectors
vector2 calcvec(tupel t, vector2 g1, vector2 g2, int q){
    return (double(2 * t.r - q + 1) * g1 + double(2 * t.s - q + 1) * g2) / double(2 * q); 
}


// Sort a list of kpoints with increasing norm by straight insertion
void sortk(kpoint *k, int N){

  int     n, np, marker;
  double  value, valuep, valmin;

  // Over all k-points
  for(n = 0; n < N; n++){
    value  = k[n].n;
    valmin = value;
    marker = n;
    
    // Determine the vector with least norm
    for(np = n+1; np < N; np++){
      valuep = k[np].n;
      if(valuep < valmin){
	valmin = valuep;
	marker = np;
      }
    }
    // Swap if necessary
    if(n != marker){
      kpoint  kp;
      kp        = k[n];
      k[n]      = k[marker];
      k[marker] = kp;
    }
  }
}
// end sortk


// Shift vector vec into the first zone
// Return the coefficients of the base vectors g1 and g2
tupel shift(vector2 vec, vector2 g1, vector2 g2, int q){

  int r, s;

  double rp, sp;

  double det;

  tupel t;

  // Compute coefficients
  det = g1.x * g2.y - g2.x * g1.y;
  if(det == 0){
    cout << "ERROR from shift(): Error in base vectors: det == 0" << endl;
  }

  //r = (int) rint((double(2.0 * q) * ( vec.x * g2.y - vec.y * g2.x) / det + q - 1.0) / 2.0);
  //s = (int) rint((double(2.0 * q) * (-vec.x * g1.y + vec.y * g1.x) / det + q - 1.0) / 2.0);

  
  rp = (2 * q * vec.x * g2.y           - 2 * q * vec.y * g2.x)          / det;
  sp = (g1.x          * 2 * q * vec.y  - g1.y          * 2 * q * vec.x) / det;

  r = (int) rint((rp + q - 1) / 2);
  s = (int) rint((sp + q - 1) / 2);

  // Shift into 1st zone
  while(r < 0) r += q;
  while(r >= q) r -= q;
  while(s < 0) s += q;
  while(s >= q) s -= q;

  t.r = r;
  t.s = s;

  // Error checking
  if(t.r < 0){
    cerr << "ERROR from shift(): t.r < 0" << endl;
  }
  if(t.r >= q){
    cerr << "ERROR from shift(): t.r >= q" << endl;
  }
  if(t.s < 0){
    cerr << "ERROR from shift(): t.s < 0" << endl;
  }
  if(t.s >= q){
    cerr << "ERROR from shift(): t.s >= q" << endl;
  }

  return t;
}
// end shift



// Main program
int     main(int argc, char **argv){

  const char *name;		          // IO file names
  
  ofstream        out;                    // output stream
  
  int             q, N;                   // number of k-points
  int             r, s, n, np, npp;
  int             elem, nelem;            // number of point group elements
  int             pgroup;                 // index of point group
  int             nused;                  // number of used k-points
  
  kpoint          *k;                     // k-mesh
  
  vector2         t1, t2;                 // Primitive translation vectors in direct space
                                          // Must be symmetry-adapted!
  
  vector2         g1, g2;                 // Base vectors of reciprocal lattice
  
  vector2         (*op)(vector2, int);    // point group operations

  vector2         tp;
  double          alpha, beta;
  
  // Print program information
  cout << "Generating k-mesh (C++-Version)" << endl;
  cout << "Generates output for use with omni2k" << endl << endl;
  
  // Open output stream
  if(argc > 1){
    // Take name from the command line
    name = argv[1];
    cout << "Writing output file '" << name << "'" << endl << endl;
    argv++;
    argc--;
  }
  else{
    // Take standard name
    name = "kaver.mesh";
  }
  
  // Read point group
  cout << "Enter point group" << endl;
  cout << "(1)   1 {id}" << endl;
  cout << "(2)   2 {id, c2}" << endl;
  cout << "(3)   3 {id, c3, ic3}" << endl;
  cout << "(4)   4 {id, c2, c4, ic4}" << endl;
  cout << "(5)   m {id, my}" << endl;
  cout << "(6) 2mm {id, c2, mx, my}" << endl;
  cout << "(7)  3m {id, c3, ic3, my, c3*my, ic3*my}" << endl;
  cout << "(8) 4mm {id, c2, c4, ic4, mx, my, c4*my, ic4*my}" << endl;
  cout << "(9)   1 {id}, with Gaussian distribution" << endl;
  cout << "pgroup = ";
  cin >> pgroup;
  
  switch(pgroup){
  case 1:
    nelem = NELEM1;
    t1 = vector2(1.0, 0.0);
    t2 = vector2(0.0, 1.0);
    op = operate1;
    break;
  case 2:
    nelem = NELEM2;
    t1 = 0.5 * vector2(1.0,  sqrt(2.0));
    t2 = 0.5 * vector2(1.0, -sqrt(2.0));
    op = operate2;
    break;
  case 3:
    nelem = NELEM3;
    t1 = vector2(0.5,  0.5 * sqrt(3.0));
    t2 = vector2(0.5, -0.5 * sqrt(3.0));
    op = operate3;
    break;
  case 4:
    nelem = NELEM4;
    t1 = vector2( 1.0, 0.0);
    t2 = vector2( 0.0, 1.0);
    op = operate4;
    break;
  case 5:
    nelem = NELEMM;
    t1 = vector2( 1.0, 1.0);
    t2 = vector2(-1.0, 1.0);
    op = operatem;
    break;
  case 6:
    nelem = NELEM2MM;
    t1 = vector2(0.5 * sqrt(2.0),  0.5);
    t2 = vector2(0.5 * sqrt(2.0), -0.5);
    op = operate2mm;
    break;
  case 7:
    nelem = NELEM3M;
    t1 = vector2(0.5,  0.5 * sqrt(3.0));
    t2 = vector2(0.5, -0.5 * sqrt(3.0));
    op = operate3m;
    break;
  case 8:
    nelem = NELEM4MM;
    t1 = vector2(1.0, 0.0);
    t2 = vector2(0.0, 1.0);
    op = operate4mm;
    break;
  case 9:
    nelem = NELEM1;
    t1 = vector2(1.0, 0.0);
    t2 = vector2(0.0, 1.0);
    op = operate1;
    break;
  default:
    nelem = 0;
    t1 = vector2(0.0, 0.0);
    t2 = vector2(0.0, 0.0);
    op = operate1;
    cerr << "ERROR from main(): Wrong point group specified." << endl;
    myexit(1);
    break;
  }

  // Calculate the basic vectors of the reciprocal lattice
  g1 = -2 * M_PI * perp(t2) / area(t1, t2);
  g2 =  2 * M_PI * perp(t1) / area(t1, t2);
  
  cout << "base vector t1: ";
  cout << setw(12) << setprecision(5) << t1.x;
  cout << setw(12) << setprecision(5) << t1.y << endl;
  cout << "base vector t2: ";
  cout << setw(12) << setprecision(5) << t2.x;
  cout << setw(12) << setprecision(5) << t2.y << endl;
  
  cout << "base vector g1: ";
  cout << setw(12) << setprecision(5) << g1.x;
  cout << setw(12) << setprecision(5) << g1.y << endl;
  cout << "base vector g2: ";
  cout << setw(12) << setprecision(5) << g2.x;
  cout << setw(12) << setprecision(5) << g2.y << endl;
  

  // Check the basis vectors t1 and t2
  cout << "Checking basis vector t1 ..." << endl;
  for(elem = 0; elem < nelem; elem++){
    tp = op(t1, elem);
    alpha = (tp.x * t2.y - tp.y * t2.x) / (t1.x * t2.y - t1.y * t2.x);
    beta  = (t1.x * tp.y - t1.y * tp.x) / (t1.x * t2.y - t1.y * t2.x);
    if(fabs(alpha) < 1.e-10){
      alpha = 0;
    }
    if(fabs(beta) < 1.e-10){
      beta = 0;
    }
    cout << setw(10) << setprecision(6) << tp.x;
    cout << setw(10) << setprecision(6) << tp.y;
    cout << setw(10) << setprecision(6) << alpha;
    cout << setw(10) << setprecision(6) << beta << endl;
  }
  cout << "Done." << endl;
  cout << "Checking basis vector t2 ..." << endl;
  for(elem = 0; elem < nelem; elem++){
    tp = op(t2, elem);
    alpha = (tp.x * t2.y - tp.y * t2.x) / (t1.x * t2.y - t1.y * t2.x);
    beta  = (t1.x * tp.y - t1.y * tp.x) / (t1.x * t2.y - t1.y * t2.x);
    if(fabs(alpha) < 1.e-10){
      alpha = 0;
    }
    if(fabs(beta) < 1.e-10){
      beta = 0;
    }
    cout << setw(10) << setprecision(6) << tp.x;
    cout << setw(10) << setprecision(6) << tp.y;
    cout << setw(10) << setprecision(6) << alpha;
    cout << setw(10) << setprecision(6) << beta << endl;
  }
  cout << "Done." << endl;


  // Check the reciprocal basis vectors g1 and g2
  cout << "Checking reciprocal basis vector g1 ..." << endl;
  for(elem = 0; elem < nelem; elem++){
    tp = op(g1, elem);
    alpha = (tp.x * g2.y - tp.y * g2.x) / (g1.x * g2.y - g1.y * g2.x);
    beta  = (g1.x * tp.y - g1.y * tp.x) / (g1.x * g2.y - g1.y * g2.x);
    if(fabs(alpha) < 1.e-10){
      alpha = 0;
    }
    if(fabs(beta) < 1.e-10){
      beta = 0;
    }
    cout << setw(10) << setprecision(6) << tp.x;
    cout << setw(10) << setprecision(6) << tp.y;
    cout << setw(10) << setprecision(6) << alpha;
    cout << setw(10) << setprecision(6) << beta << endl;
  }
  cout << "Done." << endl;
  cout << "Checking reciprocal basis vector g2 ..." << endl;
  for(elem = 0; elem < nelem; elem++){
    tp = op(g2, elem);
    alpha = (tp.x * g2.y - tp.y * g2.x) / (g1.x * g2.y - g1.y * g2.x);
    beta  = (g1.x * tp.y - g1.y * tp.x) / (g1.x * g2.y - g1.y * g2.x);
    if(fabs(alpha) < 1.e-10){
      alpha = 0;
    }
    if(fabs(beta) < 1.e-10){
      beta = 0;
    }
    cout << setw(10) << setprecision(6) << tp.x;
    cout << setw(10) << setprecision(6) << tp.y;
    cout << setw(10) << setprecision(6) << alpha;
    cout << setw(10) << setprecision(6) << beta << endl;
  }
  cout << "Done." << endl << endl;

  
  // Read input
  cout << "Enter initial mesh size q =  ";
  cin >> q;
  
  // Initialize
  N = q * q;
  
  k = new kpoint[N];
  
  n = 0;
  for(r = 0; r < q; r++){
    for(s = 0; s < q; s++){
      k[n].t.r      = r;
      k[n].t.s      = s;
      k[n].v        = calcvec(k[n].t, g1, g2, q);
      k[n].n        = norm(k[n].v);
      k[n].weight   = 0;
      k[n].used     = 1;
      n++;
    }
  }
  sortk(k, N);


  // Generate symmetry-adapted mesh
  for(n = 0; n < N; n++){
    // Only over used k-points
    if(k[n].used == 1){
      tupel   top;
      
      // Over all elements of the point group
      for(elem = 0; elem < nelem; elem++){
	top = shift(op(k[n].v, elem), g1, g2, q);
	
	// If we end on the same point
	if(top.r == k[n].t.r && top.s == k[n].t.s){
	  k[n].weight += 1;
        }
	else{
	  // If we end on any other point, not considered up to now
	  for(np = n  + 1; np < N; np++){
	    if(k[np].used == 1){
	      if(top.r == k[np].t.r && top.s == k[np].t.s){
		k[np].used = 0;
		break;
              }
	    }
	  }
        }
      }
    }  
  }
  
  // Transform the weights and determine the number of used k-points
  nused = 0;
  for(n = 0; n < N; n++){
    if(k[n].used == 1){
      k[n].weight = nelem / k[n].weight / q / q;
      nused++;
    }
  }

  // JH - for pgroup 9, re-weight the mesh by a Gaussian distribution.
  if(pgroup == 9){
    cout << endl;
    cout << "Re-weighting the mesh (Gaussian distribution), lambda = " << GAUDI << endl;
    for(n = 0; n < N; n++){
      if(k[n].used == 1){
	k[n].weight = exp(-(k[n].v.x * k[n].v.x + k[n].v.y * k[n].v.y) / GAUDI);
      }
    }
    // Weights must sum up to 1
    double sum = 0;
    for(n = 0; n < N; n++){
      if(k[n].used == 1){
	sum += k[n].weight;
      }
    }
    for(n = 0; n < N; n++){
      if(k[n].used == 1){
	k[n].weight /= sum;
      }
    }
    cout << "Re-weighting done." << endl;
  }
  
  
  // Print mesh
  out.open(name);
  // error control
  if(!out){
    cerr << "ERROR from main(): Can't open output file '" << name << "'" << endl;
    myexit(1);
  }
  out << setw(10) << nused << endl;
  for(n = 0; n < N; n++){
    if(k[n].used == 1){
      out << setw(30) << setprecision(20) << double(2 * k[n].t.r - q + 1) / double(2 * q);
      out << setw(30) << setprecision(20) << double(2 * k[n].t.s - q + 1) / double(2 * q);
      out << setw(30) << setprecision(20) << k[n].weight << endl;
    }
  }
  out.close();
  
  cout << endl;
  cout << "Generated k-mesh:" << endl;
  cout << "Number of points: " << setw(5) << nused << endl << endl;
  cout << "               r      s          weight                    k_x                    k_y" << endl;
  for(n = 0; n < N; n++){
    if(k[n].used == 1){
      cout << setw(7) << n << ": ";
      cout << setw(7) << k[n].t.r;
      cout << setw(7) << k[n].t.s;
      cout << setw(30) << setprecision(20) << k[n].weight;
      cout << setw(30) << setprecision(20) << k[n].v.x;
      cout << setw(30) << setprecision(20) << k[n].v.y << endl;
    }
  }
  
  // Check the mesh
  cout << endl;
  cout << "Mesh check: (the following data should be equal to 1.0)" << endl;
  
  // Over all shells
  for(n = 0; n < q; n++){
    for(np = 0; np < q; np++){
      
      vector2 r;
      complex am;
      
      r = n * t1 + np * t2;
      
      if((2.0*fabs(r.x) < q) && (2.0*fabs(r.y) < q)){
	am = 0;
	// Over all used k
	for(npp = 0; npp < N; npp++){
	  if(k[npp].used == 1){
	    
	    // Calculate A_{m}(k_{j})
	    for(elem = 0; elem < nelem; elem++){
	      am += k[npp].weight * exp(cmplx(0,1) * (k[npp].v * op(r, elem)));
	    }
	  }
	}
	if(abs(am) >= 1.0e-10){
	  cout << n << " " << np << " " << am / nelem << endl;
	}
      }
    }
  }
  
}
// end kmesh

