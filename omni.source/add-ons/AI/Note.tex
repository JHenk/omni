\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}

\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\mat}[1]{\mathsf{#1}}

\begin{document}
\section*{Perceptron}
The output $y$ of a perceptron is the weighted sum of the input signals $\{ x_{n}; n = 1, \ldots, N \}$,
\begin{align*}
	y & = \sum_{n = 1}^{N} w_{n} \, x_{n},
\end{align*}
neglecting for the moment an activation function.
	
The error with respect to the desired output $\bar{y}$ is defined as
\begin{align*}
	\epsilon & = \frac{1}{2} (y - \bar{y})^{2}.
\end{align*}
	
The weights $\{ w_{n}; n = 1, \ldots, N \}$ are updated (trained) according to
\begin{align*}
	w_{n} & \leftarrow w_{n} + \frac{\partial \epsilon}{\partial w_{n}} \tau,
\end{align*}
with the training rate $\tau$: the more the error depends on the weight $w_{i}$, the larger is the change of that weight. Or in other words: the more important is that weight. This training strategy is hence called \textit{reinforcement learning}.

With
\begin{align*}
	\frac{\partial \epsilon}{\partial w_{n}}
	 & = (y - \bar{y}) \frac{\partial y}{\partial w_{n}}
	 = (y - \bar{y}) \frac{\partial}{\partial w_{n}} \sum_{n' = 1}^{N} w_{n'} \, x_{n'}
	 = (y - \bar{y}) x_{n}
\end{align*}
we arrive at
\begin{align*}
	w_{n} & \leftarrow w_{n} + (y - \bar{y}) \, x_{n} \, \tau.
\end{align*}

If more that one reference output is given, say $\{ \bar{y}_{k}, k = 1, \ldots, K \}$ ($K$ is the number of training sets $\{ x_{n}^{(k)}, \bar{y}_{k} \}$), we have
\begin{align*}
\epsilon_{k} & = \frac{1}{2} (y - \bar{y}_{k})^{2},
\\
	w_{n} & \leftarrow w_{n} + (y_{k} - \bar{y}_{k}) \, x_{n}^{(k)} \, \tau
\end{align*}
for $k = 1, \ldots, K$.

\paragraph{Formulation in matrix notation.} 
\begin{description}
	\item[$\vec{x}$] input vector of length $N$.
	\item[$\vec{w}$] weight vector of length $N$.
\end{description}
\begin{align*}
y & = \vec{w} \cdot \vec{x},
 \\
\vec{w} & \leftarrow \vec{w} + (y - \bar{y}) \, \vec{x} \, \tau.
\end{align*}

\paragraph{Activation functions.} The output of a real perceptron is limited, which is reflected by the activation functions which often have a sigmoid shape. In other words, the preceptrons output $y$ is replaced by $f(y)$, with the activation function $f$. With this we have
\begin{align*}
\epsilon & = \frac{1}{2} \left( f(y) - f(\bar{y}) \right)^{2},
\\
\frac{\partial \epsilon}{\partial w_{n}} & =  \left( f(y) - f(\bar{y}) \right) \frac{\partial f(y)}{\partial y} x_{n},
\\
	w_{n} & \leftarrow w_{n} + \left( f(y) - f(\bar{y}) \right) \frac{\partial f(y)}{\partial y} x_{n} \, \tau.
\end{align*}


\paragraph{Output normalization.} Sometimes it is desirable to normalize the reference outputs $\{ \bar{y}_{k}\}$, in particular if the activation function produces a finite range (typically $[0, 1]$). With $\bar{y}_{\mathrm{min}}$ and $\bar{y}_{\mathrm{max}}$ the minimum and maximum of the reference outputs, respectively, the normalized reference outputs read
\begin{align*}
 \tilde{y}_{k} & = \frac{\bar{y}_{k} - \bar{y}_{\mathrm{min}}}{\bar{y}_{\mathrm{max}} - \bar{y}_{\mathrm{min}}} \in [0, 1].
\end{align*}
The backtransformation is given by
\begin{align*}
\bar{y}_{k} & = (\bar{y}_{\mathrm{max}} - \bar{y}_{\mathrm{min}}) \tilde{y}_{k} + \bar{y}_{\mathrm{min}}.
\end{align*}

	
\section*{Neural network}
The neural network consists of a hidden layer and an output layer of perceptrons. The input signals $\{ x_{i}; i = 1, \ldots, L \}$ are transmitted through the hidden layer, with $M$ perceptrons, to the output layer with $N$ perceptrons.

The input signals $\{ x_{l}; l = 1, \ldots, L \}$ give rise to the output signals $\{ y_{m}; m = 1, \ldots, M \}$ of the hidden layer,
\begin{align*}
y_{m} & = \sum_{l = 1}^{L} v_{l}^{(m)} \, x_{l}, \quad m = 1, \ldots, M,
\end{align*}
neglecting for the moment an activation function. These output signals of the hidden layer are the input signals for the perceptrons of the output layer. The output is given by
\begin{align*}
z_{n} & = \sum_{m = 1}^{M} w_{m}^{(n)} \, y_{m}, \quad n = 1, \ldots, N.
\end{align*}
Or
\begin{align*}
z_{n} & = \sum_{m = 1}^{M} w_{m}^{(n)} \sum_{l = 1}^{L} v_{l}^{(m)} \, x_{l}
= \sum_{m = 1}^{M} \sum_{l = 1}^{L} w_{m}^{(n)}  v_{l}^{(m)} \, x_{l}, \quad n = 1, \ldots, N.
\end{align*}

The error of the $n$-th output-layer preceptron with respect to the desired output $\bar{z}_{n}$ is defined as
\begin{align*}
\epsilon_{n} & = \frac{1}{2} (z_{n} - \bar{z}_{n})^{2}.
\end{align*}
The weights $\{ w_{m}^{(n)}; m = 1, \ldots, M; n = 1, \ldots, N \}$ of the output layer are updated according to
\begin{align*}
w_{m}^{(n)} & \leftarrow w_{m}^{(n)} + \frac{\partial \epsilon_{n}}{\partial w_{m}^{(n)}} \tau.
\end{align*}
With
\begin{align*}
\frac{\partial \epsilon_{n}}{\partial w_{m}^{(n)}}
& = (z_{n} - \bar{z}_{n}) \frac{\partial z_{n}}{\partial w_{m}^{(n)}}
 = (z_{n} - \bar{z}_{n}) \frac{\partial}{\partial w_{m}^{(n)}} \sum_{m' = 1}^{M} w_{m'}^{(n)} \, y_{m'}
 = (z_{n} - \bar{z}_{n}) \, y_{m}
\end{align*}
we have 
\begin{align*}
w_{m}^{(n)} & \leftarrow w_{m}^{(n)} + (z_{n} - \bar{z}_{n}) \, y_{m}\, \tau.
\end{align*}

The weights $\{ v_{l}^{(m)}; l = 1, \ldots, L;  m = 1, \ldots, M \}$ of the hidden layer are updated according to
\begin{align*}
v_{l}^{(m)} & \leftarrow v_{l}^{(m)} + \frac{\partial \epsilon_{n}}{\partial v_{l}^{(m)}} \tau.
\end{align*}
With
\begin{align*}
\frac{\partial \epsilon_{n}}{\partial v_{l}^{(m)}}
& = (z_{n} - \bar{z}_{n}) \frac{\partial z_{n}}{\partial v_{l}^{(m)}}
 = (z_{n} - \bar{z}_{n}) \frac{\partial}{\partial v_{l}^{(m)}} \sum_{m' = 1}^{M} \sum_{l' = 1}^{L} w_{m'}^{(n)}  v_{l'}^{(m')} \, x_{l'}
\\
&  = (z_{n} - \bar{z}_{n})  w_{m}^{(n)}  \, x_{l}
\end{align*}
we have 
\begin{align*}
v_{l}^{(m)} & \leftarrow v_{l}^{(m)} + (z_{n} - \bar{z}_{n})  w_{m}^{(n)}  \, x_{l} \, \tau.
\end{align*}
The update depends on the output error ($z_{n} - \bar{z}_{n}$) that is propagated back to the hidden layer [$(z_{n} - \bar{z}_{n})  w_{m}^{(n)}$]; hence the name \textit{back propagation network}.

\paragraph{Formulation in matrix notation.} 
\begin{description}
	\item[$\vec{x}$] input vector of length $L$.
	\item[$\mat{V}$] weight matrix of dimension $M \times L$.
	\item[$\vec{y}$] output vector of length $M$.
	\item[$\mat{W}$] weight matrix of dimension $N \times M$.
	\item[$\vec{z}$] output vector of length $N$.
\end{description}
\begin{align*}
\vec{y} & = \mat{V} \vec{x},
\\
\vec{z} & = \mat{W} \vec{y} = \mat{W} \mat{V} \vec{x}.
\end{align*}

\paragraph{Activation functions.} As for the perceptron, the output limitation of \emph{each} perceptron is modeled by an activation function, Hence,
\begin{align*}
\epsilon_{n} & = \frac{1}{2} \left( f(z_{n}) - f(\bar{z}_{n}) \right)^{2},
\\
\frac{\partial \epsilon_{n}}{\partial w_{m}^{(n)}}
& = \left( f(z_{n}) - f(\bar{z}_{n}) \right) \frac{\partial f(z_{n})}{\partial z_{n}}
\, y_{m}
\\
w_{m}^{(n)} & \leftarrow w_{m}^{(n)} + \left( f(z_{n}) - f(\bar{z}_{n}) \right) \frac{\partial f(z_{n})}{\partial z_{n}}
\, y_{m} \, \tau,
\\
\frac{\partial \epsilon_{n}}{\partial v_{l}^{(m)}}
& = \left( f(z_{n}) - f(\bar{z}_{n}) \right) \frac{\partial f(z_{n})}{\partial z_{n}}  w_{m}^{(n)}  \frac{\partial f\left( v_{l}^{(m)} \, x_{l} \right)}{\partial \left( v_{l}^{(m)} x_{l} \right)}  \, x_{l}
\\
v_{l}^{(m)} & \leftarrow v_{l}^{(m)} +  \left( f(z_{n}) - f(\bar{z}_{n}) \right) \frac{\partial f(z_{n})}{\partial z_{n}}  w_{m}^{(n)}  \frac{\partial f\left( v_{l}^{(m)} \, x_{l} \right)}{\partial \left( v_{l}^{(m)} x_{l} \right)}  \, x_{l} \, \tau.
\end{align*}


\paragraph{Output normalization.} As for the perceptrons, the output of the neural network is normalized which is important if an activation function is applied.

\end{document}