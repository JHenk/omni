
#ifndef _CURRENTGF_
#define _CURRENTGF_

void selectz(crystal &c);
void selectp(crystal &c);
void selectm(crystal &c);
void dmatz(state &l, crystal &c);
double conductancegf(job &j, actual &a, beamset &b, state &s, crystal &c, std::ostream &os, int ikval);
double conductancegfdis(job &j, actual &a, beamset &b, state &s, state &s2, state &s3, crystal &c, crystal &c2, std::ostream &os, int ikval);

#endif
