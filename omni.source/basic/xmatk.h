// xmatk.h
//
// enthaelt Prototypen der in xmatk.C definierten Unterprogramme
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _XMATK_
#define _XMATK_

#include "complex.h"
#include "cmatrix.h"
#include "crystal.h"
#include "vector2.h"

void elmg();
void xmatk(complexmatrix &S, complex e, crystal &c, vector2 q);


#endif
