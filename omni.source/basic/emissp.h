// emissp.h
//
// enthaelt Prototypen der in emissp.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _EMISSP_
#define _EMISSP_

#include <iostream>

#include "job.h"
#include "state.h"
#include "beam.h"
#include "crystal.h"

void emissp(actual &a, job &j, state &l, state &u, beamset &b, crystal &c, std::ostream &os, std::ostream &out=std::cout);

#endif
