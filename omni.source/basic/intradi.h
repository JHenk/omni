// intradi.h
//
// enthaelt Prototypen der in intradi.C definierten Funktionen
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _INTRADI_
#define _INTRADI_


#include "state.h"
#include "crystal.h"

void intradi(selem &se, celem &ce);
void intradim(selem &se, celem &ce);
void intradifp(selem &se, celem &ce);

#endif
