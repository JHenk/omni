// xmatkc.h
//
// enthaelt Prototypen der in xmatkc.C definierten Unterprogramme
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _XMATKC_
#define _XMATKC_

#include "complex.h"
#include "cmatrix.h"
#include "crystal.h"
#include "vector2.h"
#include "vector3.h"

void elmgc();
void xmatkc(complexmatrix &S, complex e, crystal &c, vector2 q, vector3 s);


#endif
