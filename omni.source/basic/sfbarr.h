// sfbarr.h
//
// enthaelt die Prototypen der in sfbarr.C definierten Funktionen
//

#ifndef _SFBARR_
#define _SFBARR_

#include "state.h"
#include "beam.h"
#include "crystal.h"

void sfbarr(state &s, beamset &b, crystal &c);

#endif
