// disorder.h
// 
// Functions for disordered system
//

#ifndef _DISORDER_
#define _DISORDER_

#include <cmath>

#include "complex.h"
#include "cmatrix.h"
#include "clebgn.h"

#include "crystal.h"
#include "state.h"


// Relativistic DLM: Number of Gaussian mesh points for integration
// over the unit sphere (integer)
#define DIS_DLM_MGAUSS 32


// Check potentials (for CPA)
void checkpot(potential &pot1, potential &pot2);
// (for CPA3)
void checkpot3(potential &pot1, potential &pot2, potential &pot3);

// Virtual Crystal Model (VCM)
void vcm(potential &pot1, potential &pot2, double conc1);

// Averaged t-matrix approximation (ATA)
void ata(state &s, crystal &c, state &s2, crystal &c2, state &s3);
// (for CPA3)
void ata3(state &s1, crystal &c1, state &s2, crystal &c2, state &s3, crystal &c3, state &s4);

// Average t-matrices over the unit sphere; for relativistic DLM
// Similar to ata(); serves as initial guess of the DLM t-matrices
void ataDLM(state &s, crystal &c, state &s2);
// Check the DLM condition
void check_dlm_condition(actual &a, job &j, state &s, crystal &c, state &s2);
// CPA self-consistency loop, for relativistic DLM
void cpaDLM(job &j, actual &a, state &s, crystal &c, state &s2, state &sp, int lflagw);

// Coherent potential approximation (CPA)
void cpa(job &j, actual &a, state &s, crystal &c, state &s2, crystal &c2, state &s3, state &sp, int lflagw);
// (for CPA3)
void cpa3(job &j, actual &a, state &s, crystal &c, state &s2, crystal &c2, state &s3, crystal &c3, state &s4, state &sp, int lflagw);

// Copy single-site t-matrices
void copyT(state &s, crystal &c, state &s3);


#endif
