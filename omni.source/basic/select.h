// select.h
//
// enthaelt Prototypen der in select.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _SELECT_
#define _SELECT_

#include "crystal.h"

void select(crystal &c);

#endif
