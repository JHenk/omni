// photo.h
//
// enthaelt Prototypen der in photo.C definierten Funktionen
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _PHOTO_
#define _PHOTO_

#include "state.h"
#include "crystal.h"

void photo(selem &ua, selem &la, celem &ca, crystal &c);
void photo_time(selem &le, selem &ule, selem &ure, celem &ce, crystal &c);
void photom(selem &ua, selem &la, celem &ca, crystal &c);
void photom_time(selem &le, selem &ule, selem &ure, celem &ce, crystal &c);
void photofp(selem &ua, selem &la, celem &ca, crystal &c);
void photofpx(selem &ua, selem &la, celem &ca, crystal &c);

#endif
