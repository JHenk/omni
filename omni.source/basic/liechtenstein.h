// liechtenstein.h
//
// enthaelt Prototypen der in lichtenstein.C definierten Funktionen
//

#ifndef _LIECHTENSTEIN_
#define _LIECHTENSTEIN_

#include <iostream>

#include "job.h"
#include "state.h"
#include "crystal.h"

void liechtensteinrs(job &j, state &s, state &s2, state &s3, crystal &c, crystal &c2, std::ostream &os, int &ref_mag);


#endif
