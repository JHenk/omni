// phasor.C
//
// Programm zur Berechnung der Phasoren
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#include <cmath>

#include "complex.h"
#include "cmatrix.h"
#include "vector2.h"

#include "constants.h"

#include "beam.h"
#include "crystal.h"
#include "state.h"

// Programm zum Berechnen der Phasoren
// sc enthaelt kz-Komponenten und bei Rueckkehr die Phasoren
// cc enthaelt den Abstandsvektor
// scp enthaelt kz-Komponenten der naechsten Schicht
// b enthaelt den k-parallel Anteil von k

void phasor(sslice &sc, cslice &cc, sslice &scp, beamset &b){
  int		i, i1, i2;
  complex	zm;
  
  sc.Kp.setsize(2 * b.nbeam);
  sc.Km.setsize(2 * b.nbeam);

  for(i = 0; i < b.nbeam; i++){
    i1 = 2 * i;
    i2 = i1 + 1;

    // Zeiger auf die entsprechenden Scheiben
    cslab &cs = *cc.slab;
    sslab &ss = *sc.slab;
    sslab &ssp = *scp.slab;

    // Berechne z-Beitrag
    zm = cs.t * ss.kz[i] + (cc.d.z - cs.t) * ssp.kz[i];
    // Berechne Phasoren
    sc.Kp(i1) = sc.Kp(i2) = exp(cmplx(0, 1) * (zm + b.vec[i] * cc.d))
      * sqrt(ss.kz[i] / ssp.kz[i]);
    sc.Km(i1) = sc.Km(i2) = exp(cmplx(0, 1) * (zm - b.vec[i] * cc.d))
      * sqrt(ssp.kz[i] / ss.kz[i]);
  }
}
