// Inclusion guard (make sure that we read this file only once)
#ifndef INC_SFBARR_COMMON_H
#define INC_SFBARR_COMMON_H

#include "complex.h"
#include "table.h"
#include "imatrix.h"
#include "cmatrix.h"

#include "beam.h"

// Definition of the used ODE solver (for correct values, see code.C)
#define SFBARR_ODE_TYPE 0

#define SFBARR_NVAR  2
#define SFBARR_KMAX  100
#define SFBARR_DZSAV 10
#define SFBARR_EPS   1.e-8
#define SFBARR_H     2

#define SFBARR_NSTEP 2000

#define SFBARR_SPIN_UP 0
#define SFBARR_SPIN_DO 1

#define SFBARR_UNUSED -1

// Parameter-Struktur fuer kontinuierliches Oberflaechenpotential
struct sfparm{
  complex	vopt;		// Optisches Potential
  double	lattice;	// Gitterparameter
  double	*bparam;	// Barrierenparameter
  complex 	(*func)(sfparm &, double);  // Zeiger auf Potential-Funktion
  complex	ev;		// Energie im Kristall
  complex	evo;		// Energie im Aussenraum
  complex	kz;		// kz im Kristall
  complex	kzo;		// kz im Aussenraum
  double	parm[6];	// Andere Parameter
};


// Structure for offdiagonal functions (for corrugation)
struct offd{
  int     used;
  int     h;
  int     k;
  double  z;      // Used for BARR_*_C
  double  lambda; // Used for BARR_*_C
  complex v;      // Used for BARR_*_C
  doubletable vg; // Used for BARR_CORRUGATED
};


// Global variables, defined in sfbarr_common.C
extern sfparm            *pglobal;

extern int               nvarglobal;
extern int               nvarglobalhalf;
extern intmatrix         indexglobal;

extern int               barrierglobal;

extern complexmatrix     Hglobal;
extern complexdiagmatrix *evoglobal;

extern beamset           *bglobal;

extern offd              *lookupglobal;


// Common functions
void presetjjj(sfparm &p);
void preseths(sfparm &p);
void presett(sfparm &p);
void presetcorr(sfparm &p);
void getz(sfparm &p, double &z1, double &z2);
void fillRS(complexmatrix &S, complexmatrix &R, complexdiagmatrix &Sphase, complexdiagmatrix &Rphase, complexdiagmatrix &kzo, complexdiagmatrix &kz, double z1, double z2, int nbeam);


#endif /* INC_SFBARR_COMMON_H */
