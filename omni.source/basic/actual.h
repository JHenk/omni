//
// Basic functions for structure actual


#ifndef _ACTUAL_
#define _ACTUAL_

#include <ostream>
#include "job.h"
#include "crystal.h"

void getkpar(job &j, actual &a, int ikval, std::ostream &out=std::cout);
void getkparmean(job &j, actual &a, int ikval, std::ostream &out=std::cout);
void getkparmeandis(job &j, actual &a, int ikval, std::ostream &out=std::cout);

// Connect a crystal object with an actual object
void connect_actual(actual &a, crystal &c);

#endif

