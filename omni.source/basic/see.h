// see.h
//
// enthaelt Prototypen der in see.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _SEE_
#define _SEE_

#include "job.h"
#include "state.h"
#include "beam.h"

void see(actual &a, job &j, state &u, beamset &b, std::ostream &os, std::ostream &out=std::cout);

#endif
