// emissp.C
//
// Spin-resolved photoemission - Pendry formalism
//

#include <iostream>
#include <iomanip>
#include <cmath>

#include "complex.h"
#include "cmatrix.h"
#include "csmatrix.h"
#include "do_lu.h"
#include "vector2.h"
#include "density.h"
#include "function.h"

#include "constants.h"

#include "clebgn.h"

#include "job.h"
#include "state.h"
#include "beam.h"
#include "crystal.h"

using namespace std;

// Programm zur Ausgabe der Photoemissions-Intensitaeten
// a enthaelt E, theta und phi
// j enthaelt die Anfangspolarisation
// l enthaelt den unteren Zustand
// u enthaelt den oberen Zustand
// b enthaelt die verwendeten Strahlen
// c enthaelt die Beschreibung des Kristalles
// os enthaelt den Ausgabestrom

void emissp(actual &a, job &j, state &l, state &u, beamset &b, crystal &c, ostream &os, ostream &out) {
  int		slab, slice, layer; 
  complexmatrix	X; 
  complexsupermatrix 
    Y;
  complexmatrix	S;
  complex		f;
  density		rho;
  double		qz;
  double		xi, px, py, pz, pn, normp;
  
  // Die Summe der Spin-Dichte Matrizen
  S.setsize(2, 2) = 0;
  // Ueber alle Schichten
  for(slab = 0; slab < c.nslab; slab++){
    // Zeiger auf zugehoerige Scheibe
    sslab &ls = l.slab[slab];
    // Berechne einen Faktor
    f = ls.k * (2 + ls.e / CLIGHT2) / M_PI;
    // Berechne eine Matrix
#ifdef USELU
    ls.Z = f * invlu(ls.T) * ls.Dr;
#else
    ls.Z = f * inv(ls.T) * ls.Dr;
#endif
  }
  // Berechne Monolayer-Emission
  for(layer = 0; layer < j.general.nlayer; layer++){
    // Zeiger auf die Lage
    slayer &ll = l.layer[layer];
    slayer &ul = u.layer[layer];
    // Zeiger auf zugehoerige Schicht
    sslice &lc = *ll.slice;
    // Zeiger auf zugehoerige Scheibe
    sslab &ls = *lc.slab;
    // Bestimme A0
    ll.A0 = ls.Z * ul.A;
    // Bestimme A
    ll.A = ls.O * ll.A0;
    // Entwicklung nach ebenen Wellen
    ll.Ap = ls.Wdp * ll.A;
    ll.Am = ls.Wdm * ll.A;
  }
  // Pendry-Verfahren
  X.setsize(2 * b.nbeam, 2) = 0;
  // Ueber alle Schichten
  for(slice = 0; slice < c.nslice; slice++){
    // Zeiger auf zugehoerige Schicht
    sslice &lc = l.slice[slice];
    // Zeiger auf zugehoerige Scheibe
    sslab &ls = *lc.slab;
    // Berechne eine Matrix
#ifdef USELU
    lc.Z = ls.Mmm * invlu(1 - lc.Rmp * ls.Mpm);
#else
    lc.Z = ls.Mmm * inv(1 - lc.Rmp * ls.Mpm);
#endif
  }
  // Zunaechst von hinten nach vorne
  for(layer = j.general.nlayer - 1; layer >= 0; layer--){
    // Zeiger auf die Lage
    slayer &ll = l.layer[layer];
    // Zeiger auf zugehoerige Schicht
    sslice &lc = *ll.slice;
    // Von hinten einlaufendes Wellenfeld
    ll.Bm = lc.Km * X;
    // nach vorne auslaufendes Wellenfeld
    X = ll.Am + lc.Z * (ll.Bm + lc.Rmp * ll.Ap);
  }
  // An der Oberflaechenbarriere reflektiertes Wellenfeld
#ifdef USELU
  X = invlu(1 - l.Mpm * l.Rmp) * l.Mpm * X;
#else
  X = inv(1 - l.Mpm * l.Rmp) * l.Mpm * X;
#endif
  // Ueber alle Schichten
  for(slice = 0; slice < c.nslice; slice++){
    // Zeiger auf zugehoerige Schicht
    sslice &lc = l.slice[slice];
    // Zeiger auf zugehoerige Scheibe
    sslab &ls = *lc.slab;
    // Berechne eine Matrix
#ifdef USELU
    lc.Z = invlu(1 - ls.Mpm * lc.Rmp);
#else
    lc.Z = inv(1 - ls.Mpm * lc.Rmp);
#endif
  }
  // Dann von vorne nach hinten
  for(layer = 0; layer < j.general.nlayer; layer++){
    // Zeiger auf die Lage
    slayer &ll = l.layer[layer];
    // Zeiger auf zugehoerige Schicht
    sslice &lc = *ll.slice;
    // Zeiger auf zugehoerige Scheibe
    sslab &ls = *lc.slab;
    // Gesamtes von vorne einlaufendes Wellenfeld
    ll.Dp = X;
    // Nach hinten auslaufendes Wellenfeld
    X = lc.Z * (ll.Ap + ls.Mpm * ll.Bm + ls.Mpp * X);
    // Gesamtes von hinten einlaufendes Wellenfeld
    ll.Dm = ll.Bm + lc.Rmp * X;
    // Transport zur naechsten Schicht
    X = lc.Kp * X;
  }
  // Finales Matrix Element
  for(layer = 0; layer < j.general.nlayer; layer++){
    // Zeiger auf die Lage
    slayer &ll = l.layer[layer];
    slayer &ul = u.layer[layer];
    // Zeiger auf zugehoerige Schicht
    sslice &lc = *ll.slice;
    // Zeiger auf zugehoerige Scheibe
    sslab &ls = *lc.slab;
    // Entwicklung nach Kugelwellen
    Y = ls.Wcp * ll.Dp + ls.Wcm * ll.Dm;
    // Gesamtes rueckgestreutes Wellenfeld in dieser Schicht
    Y = Y + ll.A - ll.A0;
    // Finales Matrix-Element
    S = S + herm(ul.A) * ls.Dl * Y;
  }
  // Atomarer Beitrag
  for(layer = 0; layer < j.general.nlayer; layer++){
    // Zeiger auf die Lage
    slayer &ll = l.layer[layer];
    slayer &ul = u.layer[layer];
    // Zeiger auf zugehoerige Schicht
    sslice &lc = *ll.slice;
    // Zeiger auf zugehoerige Scheibe
    sslab &ls = *lc.slab;
    // Compute a factor
    f = ls.k * (2 + ls.e / CLIGHT2) / M_PI;
    // Atomic matrix element
    S = S + f * herm(ul.A) * ls.D * ul.A;
  }
  S = cmplx(0, 1) * S;
  
  // Spin density matrix
  rho = density(S(0, 0), S(0,1), S(1, 0), S(1, 1));
  rho = (rho - herm(rho)) / cmplx(0, 2);

  // Prefector
  if(u.ev.re >= 0){
    qz = sqrt(u.ev.re * (2 + u.ev.re / CLIGHT2));
  }
  else{
    qz = 0;
  }
  xi = qz * real(trace(rho));
  px = qz * real(trace(SX * rho));
  py = qz * real(trace(SY * rho));
  pz = qz * real(trace(SZ * rho));
  if(xi <= 0){
    px = 0;
    py = 0;
    pz = 0;
  }
  else{
    px /= xi;
    py /= xi;
    pz /= xi;
  }
  normp = sqrt(px * px + py * py + pz * pz);
  
  // Spin polarization along n (P dot n)
  pn = j.symm.sz.x * px + j.symm.sz.y * py + j.symm.sz.z * pz;
  
  out << fixed;
  out << "MESSAGE from emissp(): SPARPES intensity" << endl;
  out << setw(8) << setprecision(3) << a.e.re * HART;
  out << setw(8) << setprecision(3) << a.e.im * HART;
  out << setw(12) << setprecision(5) << a.theta * 180 / M_PI;
  out << setw(12) << setprecision(5) << a.phi * 180 / M_PI;
  out << setw(12) << setprecision(5) << a.q.x;
  out << setw(12) << setprecision(5) << a.q.y;
  out << setw(14) << setprecision(8) << 100 * xi;
  out << setw(14) << setprecision(8) << 100 * px;
  out << setw(14) << setprecision(8) << 100 * py;
  out << setw(14) << setprecision(8) << 100 * pz;
  out << setw(14) << setprecision(8) << 100 * normp;
  out << setw(14) << setprecision(8) << 100 * pn;
  out << endl;
  
  os << fixed;
  os << setw(8) << setprecision(3) << a.e.re * HART;
  os << setw(8) << setprecision(3) << a.e.im * HART;
  os << setw(12) << setprecision(5) << a.theta * 180 / M_PI;
  os << setw(12) << setprecision(5) << a.phi * 180 / M_PI;
  os << setw(15) << setprecision(8) << a.q.x;
  os << setw(15) << setprecision(8) << a.q.y;
  os << " ";
  os << setw(14) << setprecision(8) << 100 * xi;
  os << " ";
  os << setw(14) << setprecision(8) << 100 * px;
  os << " ";
  os << setw(14) << setprecision(8) << 100 * py;
  os << " ";
  os << setw(14) << setprecision(8) << 100 * pz;
  os << " ";
  os << setw(14) << setprecision(8) << 100 * normp;
  os << " ";
  os << setw(14) << setprecision(8) << 100 * pn;
  os << endl;
}

