//
// Dyson equation solver
//

#ifndef _DYSON_
#define _DYSON_

int checkdef(state &s, state &s2, crystal &c, int layer);
void InitGF(job &j, const state &s, state &sp, double weight);
void dyson(job &j, state &s, state &s2, crystal &c);

void Dysonrs(job &j, state &s, state &s2, crystal &c);

#endif
