// ldos.h
//
// enthaelt Prototypen der in ldos.C definierten Funktionen
//

#ifndef _LDOS_
#define _LDOS_

#include <iostream>

#include "job.h"
#include "state.h"
#include "crystal.h"

void vmat(complexsupermatrix &V, complexmatrix &Rmp, complexmatrix &Rpm, sslab &ss);
void ldos(actual &a, job &j, state &u, crystal &c, std::ostream &os, std::ostream &out=std::cout);
void ldoscore(actual &a, job &j, state &s, crystal &c, std::ostream &os, std::ostream &out=std::cout);
void tldos(actual &a, job &j, state &s, crystal &c, std::ostream &os, std::ostream &out=std::cout);
void compdos(actual &a, job &j, state &s, crystal &c, std::ostream &os, std::ostream &out=std::cout);
void AddGF(job &j, const state &s, state &sp, double weight);
void GFdefect(job &j, state &s, state &s2, int deflayer);
void dyson(job &j, state &s, state &s2, crystal &c);
void ldosdef(actual &a, job &j, state &s, state &s2, crystal &c, crystal &c2, std::ostream &os, std::ostream &out=std::cout);
void ldosrs(actual &a, job &j, state &s, crystal &c, std::ostream &os, std::ostream &out=std::cout);

#endif
