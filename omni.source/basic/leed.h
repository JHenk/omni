// leed.h
//
// enthaelt Prototypen der in leed.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _LEED_
#define _LEED_

#include "job.h"
#include "state.h"
#include "beam.h"

void leed(actual &a, job &j, state &u, beamset &b, std::ostream &os, std::ostream &out=std::cout);

#endif
