// green.h
//
// enthaelt Prototypen der in green.C definierten Funktionen
//

#ifndef _GREEN_
#define _GREEN_

#include "job.h"
#include "state.h"
#include "crystal.h"

void green(job &j, state &u, crystal &c, int mode);

#endif
