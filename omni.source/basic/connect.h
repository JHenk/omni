// connect.h
//
// enthaelt die Prototypen der in connect.C definierten Funktionen
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _CONNECT_
#define _CONNECT_

#include "job.h"
#include "crystal.h"
#include "state.h"

void connect(job &j, state &s, crystal &c, int flagw);
void dvector(crystal &c, int layer, int atom, int layerp, int atomp, vector3 &d);
void print_positions(job &j, crystal &c);
void copy_state(const job &jj, state &s2, const state &s, const crystal &c, const bool = true);

#endif
