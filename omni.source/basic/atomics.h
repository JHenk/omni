// atomics.h
//
// Enthaelt Prototypen der in atomics.C definierten Funktionen
//

#ifndef _ATOMICS_
#define _ATOMICS_

#include <iostream>

#include "potential.h"

void atomics(const job &j, potential &p, char *name, int ptype);

#endif
