//
// Tunneling-matrix elements (for STM)
// 
// 29/10/01 JH
//

#ifndef _TUNMAT_
#define _TUNMAT_

#include "crystal.h"
#include "job.h"
#include "state.h"

// Tunneling-matrix element < regular | V | regular>
void TM(job &j, state &s, state &s2, state &s3, crystal &c, crystal &c2, crystal &c3, int laymin, int laymax);

// Tunneling-matrix elements, nonmagnetic; from layer to layerp
void TMlaylay(state &s, state &s2, state &s3, int layer, int layerp, complexsupermatrix &T, int tmode);

#endif
