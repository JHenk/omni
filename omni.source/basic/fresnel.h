// fresnel.h
//
// Transformation des A-Vektors gemaess der Fresnel-Formeln
// vgl. Jackson, Classical Electrodynamics, 2nd Edition
//      und Dissertation JH
//
// JH -- 21. 05. 96
//
// Parameter:
// theta, phi    Einfallswinkel des Lichts (Bogenmass)
// as            Anteil des A-Vektors senkrecht zur Einfallsebene
//               (komplex, s-polarisiert)
// ap            Anteil des A-Vektors in der Einfallsebene
//               (komplex, p-polarisiert)
// dk            Dielektrizitaetskonstante des Festkoerpers (komplex)
//
// Berechnet wird der A-Vektor in kartesischen Koordinaten im
// Festkoerper (ax, ay, az)

#ifndef _FRESNEL_
#define _FRESNEL_

#include "job.h"
#include "crystal.h"
#include "state.h"

void photon(job &j, int iangle, actual &a, crystal &c, crystal &c2, int ikval, std::ostream &out=std::cout);

#endif
