//
// Adaptive grid method (2dim. case) for Bloch-wave tunneling
//
// 11/09/00 Juergen Henk
//

#ifndef _BINTEGRATE_
#define _BINTEGRATE_

void bwtmean(job &j, actual &a, state &l, state &r, crystal &c, double &TotalSumTpp, double &TotalSumTpm, double &TotalSumTmp, double &TotalSumTmm, double &TotalErrp, double &TotalErrm, double &TotalFanoFp, double &TotalFanoFm, int lflagw, std::ofstream &out);

#endif
