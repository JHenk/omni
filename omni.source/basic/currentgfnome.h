//
// Compute the density matrix of the GF in plane-wave represenation
// Compute the STM current in Tersoff-Hamann theory
//

#ifndef _CURRENTGFNOME_
#define _CURRENTGFNOME_

#include "job.h"
#include "state.h"
#include "crystal.h"

double conductancegfnome(job &j, actual &a, crystal &c, crystal &c2, state &s, state &s2, std::ofstream &out);
double conductancegfnomers(job &j, actual &a, crystal &c, crystal &c2, state &s, state &s2, std::ofstream &out);

#endif
