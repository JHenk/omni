// wig.h
//
// enthaelt Prototypen der in wig.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _WIG_
#define _WIG_

#include "crystal.h"
#include "state.h"
#include "beam.h"

void wig(sslab &ss, cslab &cs, beamset &b, crystal &c);


#endif
