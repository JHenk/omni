// order.h
//
// enthaelt Prototypen der in order.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _ORDER_
#define _ORDER_

#include "vector2.h"
#include "crystal.h"
#include "beam.h"
#include "job.h"

void revbeam(beamset &b);
void order(const job &jj, double er, vector2 q, crystal &c, beamset &b,  std::ostream &out=std::cout);
void get_beamset(const job &j, double e, double er, vector2 q, crystal &c, beamset &b, std::ostream &out=std::cout);

#endif
