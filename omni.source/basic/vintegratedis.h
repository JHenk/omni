//
// Adaptive grid method (2dim. case) for V-matrix integration, disorder
//

#ifndef _VINTEGRATEDIS_
#define _VINTEGRATEDIS_

void vmatmeandis(job &j, actual &a, state &s, state &s2, crystal &c, int mode);

#endif
