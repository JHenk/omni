// ldos.h
//
// enthaelt Prototypen der in ldos.C definierten Funktionen
//

#ifndef _ELDOS_
#define _ELDOS_

#include <iostream>

#include "job.h"
#include "state.h"
#include "crystal.h"

void eldos(actual &a, job &j, state &u, crystal &c, std::ostream &os);
void teldos(actual &a, job &j, state &s, state &s2, crystal &c, std::ostream &os);

#endif
