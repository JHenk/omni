// disorder.h
// 
// enthaelt Routinen zur Berechnung von ungeordneten Systemen
//

#include <math.h>

#include "complex.h"
#include "cmatrix.h"
#include "clebgn.h"

#include "crystal.h"
#include "state.h"

// Check potentials
void checkpot(potential &pot1, potential &pot2);
// for CPA (3potentials)
void checkpot3(potential &pot1, potential &pot2, potential &pot3);


// Programm zur Berechnung des Virtual Crystal Model (VCM)
void vcm(potential &pot1, potential &pot2, double conc1);

// Programm zur Berechnung gemittelter t-Matrizen (ATA)
void ata(state &s, crystal &c, state &s2, crystal &c2, state &s3);

void ata3(state &s1, crystal &c1, state &s2, crystal &c2, state &s3, crystal &c3, state &s4);

void cpa(job &j, actual &a, state &s, crystal &c, state &s2, crystal &c2, state &s3, state &sp, int lflagw);

// Programm zur Berechnung gemittelter t-Matrizen (CPA)
void cpa3(job &j, actual &a, state &s, crystal &c, state &s2, crystal &c2, state &s3, crystal &c3, state &s4, state &sp, int lflagw);

// Copy single-site t-matrices
void copyT(state &s, crystal &c, state &s3);


