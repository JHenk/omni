// disorder.C
// 
// enthaelt Routinen zur Berechnung von ungeordneten Systemen
//
// Virtual crystal approximation (VCA)     j.general.dis = 1
// Averaged t-matrix approximation (ATA)   j.general.dis = 2
// Coherent potential approximation (CPA)  j.general.dis = 3

#include <math.h>
#include <iomanip>
#include <iostream>
using namespace std;

#include "platform.h"

#include "constants.h"
#include "complex.h"
#include "cmatrix.h"
#include "clebgn.h"
#include "ldos.h"

#include "crystal.h"
#include "connect.h"
#include "job.h"
#include "state.h"
#include "spec.h"

#include "vintegrate.h"

// For CPA
#define CPA_MIX      1.0    // Mixing factor for simple mixing
#define CPA_MAXITER  50     // Maximum allowed number of iterations
#define CPA_ABSERROR 1.e-6  // Maximum allowed absolute error
#define CPA_RELERROR 0.0000 // Maximum allowed relative error

#define TLIMIT 1.e-6

#define VERBOSITY 2

// Some useful inline functions
#define DMAX(a, b) ((a > b) ? a : b)
#define DMIN(a, b) ((a > b) ? b : a)
#define SIGN(a, b) ((b >= 0) ? fabs(a) : -fabs(a))

// Print the fixed parameters
void print_fixed_params_cpa(){

  static int flag = 0;

  if(flag == 0){
    cout << "MESSAGES from print_fixed_params_cpa():" << endl;
    cout << "CPA_MIX      = " << CPA_MIX << endl;
    cout << "CPA_MAXITER  = " << CPA_MAXITER << endl;
    cout << "CPA_ABSERROR = " << CPA_ABSERROR << endl;
    cout << "CPA_RELERROR = " << CPA_RELERROR << endl;
    cout << "TLIMIT       = " << TLIMIT << endl;
    cout << "HINT from print_fixed_params_cpa(): Each layer has to correspond 1-to-1 to a slab." << endl << endl;

    flag = 1;
  }

}



// Check the potentials for disorder
void checkpot(potential &pot1, potential &pot2){

  // Check on muffin-tin radii
  if(fabs(pot1.rm - pot2.rm) >= 1.0e-6){
    cerr << "ERROR from checkpot(): muffin-tin radii do not match." << endl;
    myexit(1);
  }
  // Check on mesh
  if(pot1.n != pot2.n){
    cerr << "ERROR from checkpot(): numbers of radial mesh points do not match." << endl;
    myexit(1);
  }
  if(pot1.h != pot2.h){
    cerr << "ERROR from checkpot(): mesh step widths do not match." << endl;
    myexit(1);
  }

}


// Check the potentials for CPA (3 potentials)
void checkpot3(potential &pot1, potential &pot2, potential &pot3){

  // Check on muffin-tin radii
  if(fabs(pot1.rm - pot2.rm) >= 1.0e-6 || fabs(pot1.rm - pot3.rm) >= 1.0e-6 || fabs(pot2.rm - pot3.rm) >= 1.0e-6){
    cerr << "ERROR from checkpot(): muffin-tin radii do not match." << endl;
    myexit(1);
  }
  // Check on mesh
  if(pot1.n != pot2.n || pot1.n != pot3.n  || pot2.n != pot3.n) {
    cerr << "ERROR from checkpot(): numbers of radial mesh points do not match." << endl;
    myexit(1);
  }
  if(pot1.h != pot2.h || pot1.h != pot3.h || pot2.h != pot3.h){
    cerr << "ERROR from checkpot(): mesh step widths do not match." << endl;
    myexit(1);
  }
}
// Programm zur Berechnung des Virtual Crystal Model (VCM)
// pot1   enthaelt das erste Potential
// pot2   enthaelt das zweite Potential
// conc1  enthaelt die Konzentration fuer pot1
// zurueckgegeben wird pot1, das gemischte Potential

void vcm(potential &pot1, potential &pot2, double conc1){

  double conc2;
  
  // Concentration of pot2
  conc2 = 1.0 - conc1;

  // Some preliminary checks
  checkpot(pot1, pot2);
  
  // Some security checks
  if(conc1 < 0.0){
    cerr << "ERROR from vcm(): concentration has to be positive." << endl;
    myexit(1);
  }
  if(conc1 > 1.0){
    cerr << "ERROR from vcm(): concentration has to be less than 1." << endl;
    myexit(1);
  }
  
  // Check on type
  if(pot1.ptype != pot2.ptype){
    cerr << "ERROR from vcm(): potential types do not match." << endl;
    myexit(1);
  }

  // Now we are ready to mix!
  // Handle the 1st line of the potential-data file
  pot1.z  = conc1 * pot1.z  + conc2 * pot2.z;
  pot1.n  = (int) (conc1 * pot1.n  + conc2 * pot2.n);
  pot1.h  = conc1 * pot1.h  + conc2 * pot2.h;
  pot1.rm = conc1 * pot1.rm + conc2 * pot2.rm;
  pot1.m  = conc1 * pot1.m  + conc2 * pot2.m;
  pot1.td = conc1 * pot1.td + conc2 * pot2.td;
  
  
  // Note: In a future version we should also treat the case of different
  // meshes
  pot1.r  = conc1 * pot1.r + conc2 * pot2.r;

  // Weight the potentials by conc1
  switch(pot1.ptype){
  case POT_EMPTY_SPHERE:
  case POT_NONMAG_SPHERE:
    pot1.v  = conc1 * pot1.v + conc2 * pot2.v;
    break;
  case POT_MAG_EMPTY_SPHERE:
  case POT_MAG_SPHERE:
    pot1.v  = conc1 * pot1.v + conc2 * pot2.v;
    pot1.b  = conc1 * pot1.b + conc2 * pot2.b;
    break;
  case POT_MAG_NONSPHERE:
    pot1.vp  = conc1 * pot1.vp + conc2 * pot2.vp;
    pot1.vm  = conc1 * pot1.vm + conc2 * pot2.vm;
    break;
  default:
    cerr << "ERROR from vcm(): wrong potential type." << endl;
    myexit(1);
    break;
  }    

}
// end vcm


// Programm zur Berechnung gemittelter t-Matrizen (ATA)
// s und s2 enthalten die Zustaende der beiden geordneten Phasen
// c und c2 enthalten die Kristallbeschreibungen der geordneten Phasen
// s3 enthaelt die gemittelten T-Matrizen

void ata(state &s, crystal &c, state &s2, crystal &c2, state &s3){

  int     slab;
  int     atom;

  complexmatrix localT;

  // Kopiere ein paar Eintraege
  s3.flagw = s.flagw;
  s3.ev    = s.ev;
  
  // Ueber alle Scheiben
  for(slab = 0; slab < c.nslab; slab++){
    // Zeiger auf diese Scheibe
    cslab &cs  = c.slab[slab];
    cslab &cs2 = c2.slab[slab];
    
    sslab &ss  = s.slab[slab];
    sslab &ss2 = s2.slab[slab];
    sslab &ss3 = s3.slab[slab];
    
    // Setze Groesse der T-Matrix
    ss3.T.setsize(cs.natom, cs.natom, kmmax, kmmax);
    
    // Kopiere ein paar Eintraege
    ss3.opt = ss.opt;
    ss3.e   = ss.e;
    ss3.k   = ss.k;
    
    // Fuer alle Atome die T-Matrizen mitteln
    for(atom = 0; atom < cs.natom; atom++){
      // Zeiger auf die Atome
      catom &ca  = cs.atom[atom];
      catom &ca2 = cs2.atom[atom];
      
      satom &sa  = ss.atom[atom];
      satom &sa2 = ss2.atom[atom];
      
      // Zeiger auf entsprechendes Element
      selem & se  = *sa.elem;
      selem & se2 = *sa2.elem;

      // Averaging the single-site t-matrices
      // Note: se.T and se2.T are in the local frame.
      localT = ca.elem->conc * se.T + ca2.elem->conc * se2.T;
      // Rotate the averaged T into the global frame.
      ss3.T(atom, atom) = herm(ca.R) * localT * ca.R;
    }
  }
}
// end ata


// Program to calculate average of 3 t-matrix
// s1, s2, and s3 are state of the ordered phase 
// c1, c2, and c3 are crystal of the ordered phase
// s4 contains average t-matrix

void ata3(state &s, crystal &c, state &s2, crystal &c2, state &s3, crystal &c3, state &s4){

  int     slab;
  int     atom;

  complexmatrix localT;

  // Kopiere ein paar Eintraege
  s4.flagw = s.flagw;
  s4.ev    = s.ev;
  
  // Ueber alle Scheiben
  for(slab = 0; slab < c.nslab; slab++){
    // Zeiger auf diese Scheibe
    cslab &cs = c.slab[slab];
    cslab &cs2 = c2.slab[slab];
    cslab &cs3 = c3.slab[slab];

    
    sslab &ss = s.slab[slab];
    sslab &ss2 = s2.slab[slab];
    sslab &ss3 = s3.slab[slab];
    sslab &ss4 = s4.slab[slab];

    // Setze Groesse der T-Matrix
    ss4.T.setsize(cs.natom, cs.natom, kmmax, kmmax);
    
    // Kopiere ein paar Eintraege
    ss4.opt = ss.opt;
    ss4.e   = ss.e;
    ss4.k   = ss.k;
    
    // Fuer alle Atome die T-Matrizen mitteln
    for(atom = 0; atom < cs.natom; atom++){
      // Zeiger auf die Atome
      catom &ca = cs.atom[atom];
      catom &ca2 = cs2.atom[atom];
      catom &ca3 = cs3.atom[atom];

      
      satom &sa = ss.atom[atom];
      satom &sa2 = ss2.atom[atom];
      satom &sa3 = ss3.atom[atom];

      // Zeiger auf entsprechendes Element
      selem & se = *sa.elem;
      selem & se2 = *sa2.elem;
      selem & se3 = *sa3.elem;

      // Averaging the single-site t-matrices
      // Note: se.T and se2.T are in the local frame.
      localT = ca.elem->conc * se.T + ca2.elem->conc * se2.T + ca3.elem->conc * se3.T;
      // Rotate the averaged T into the global frame.
      ss4.T(atom, atom) = herm(ca.R) * localT * ca.R;
    }
  }
}
// end ata

// Copy single-site t-matrices from state to state
void copyT(state &s, crystal &c, state &sp){

  int     slab;
  
  // Kopiere ein paar Eintraege
  sp.flagw = s.flagw;
  sp.ev    = s.ev;
  
  // Ueber alle Scheiben
  for(slab = 0; slab < c.nslab; slab++){
    // Zeiger auf diese Scheibe
    cslab &cs  = c.slab[slab];
    
    sslab &ss  = s.slab[slab];
    sslab &ssp = sp.slab[slab];
    
    // Setze Groesse der T-Matrix
    ssp.T.setsize(cs.natom, cs.natom, kmmax, kmmax) = 0;
    
    // Kopiere ein paar Eintraege
    ssp.opt = ss.opt;
    ssp.e   = ss.e;
    ssp.k   = ss.k;
    ssp.T   = ss.T;
  }
}
// end copyT


// CPA self-consistency loop
void cpa(job &j, actual &a, state &s, crystal &c, state &s2, crystal &c2, state &s3, state &sp, int lflagw){

  int                count;
  int                layer, slab, atom;
  int                km, kmp;

  double             conc, conc2;
  double             abserr, relerr, olderr, labserr, aabserr;

  //complexsupermatrix D, D2, D3, V, V2, V3, X;
  complexmatrix D, D2, D3, V, V2, V3, X;

  print_fixed_params_cpa();

  // Now make an initial guess for the single-site t-matrix of state s3 ...
  ata(s, c, s2, c2, s3);

  // ... and keep only components which are large enough
  for(slab = 0; slab < c.nslab; slab++){
    cslab &cs  = c.slab[slab];
    sslab &ss3 = s3.slab[slab];

    for(atom = 0; atom < cs.natom; atom++){
      for(km = 0; km < kmmax; km++){
	for(kmp = 0; kmp < kmmax; kmp++){
	  if(abs(ss3.T(atom, atom)(km, kmp)) < EMACH){
	    ss3.T(atom, atom)(km, kmp) = 0;
	  }
	}
      }
    }
  }

  // Start of the CPA self-consistency loop
  olderr = 0;
  count  = 0;
  do{
    // Compute the k-averaged V matrices for state s3
    copyT(s3, c2, sp);
    vmatmean(j, a, s3, sp, c2, V_DIAGONAL);
    
    // Initialize the absolute error
    abserr = 0;
    
    // And now for all layers
    // NOTE: Each layer has to correspond 1-to-1 to a slab!
    for(layer = 0; layer < j.general.nlayer; layer++){
      slayer &sl  = s.layer[layer];
      slayer &sl2 = s2.layer[layer];
      slayer &sl3 = s3.layer[layer];

      sslice &sc  = *sl.slice;
      sslice &sc2 = *sl2.slice;
      sslice &sc3 = *sl3.slice;

      sslab  &ss  = *sc.slab;
      sslab  &ss2 = *sc2.slab;
      sslab  &ss3 = *sc3.slab;

      clayer &cl  = c.layer[layer];
      clayer &cl2 = c2.layer[layer];

      cslice &cc  = *cl.slice;
      cslice &cc2 = *cl2.slice;

      cslab  &cs  = *cc.slab;
      cslab  &cs2 = *cc2.slab;

      // Absolute error of this layer
      labserr = 0;

      // And for all atoms
      for(atom = 0; atom < cs.natom; atom++){
	catom  &ca  = cs.atom[atom];
	catom  &ca2 = cs2.atom[atom];

	// If this is a defect atom...
        // - if the two single-site t-matrices are too similar, then do nothing
	if(norm(ss2.T(atom, atom) - ss.T(atom, atom)) > TLIMIT){
      
	  // Concentrations
	  conc  = ca.elem->conc;
	  conc2 = ca2.elem->conc;
	  
	  // Absolute error of this atom
	  aabserr = 0;
	  
	  // Compute the new impurity matrix D3 of s3 via the CPA condition
	  // If ok, then D3 == 1.
	  V3 = sl3.V[layer](atom, atom);
	  D  = inv(1 - cmplx(0, 1) * (1 / ss.k) * V3 * (ss.T(atom, atom)  - ss3.T(atom, atom)));
	  D2 = inv(1 - cmplx(0, 1) * (1 / ss.k) * V3 * (ss2.T(atom, atom) - ss3.T(atom, atom)));
	  D3 = conc * D + conc2 * D2;
	  
	  // Invert the functional form of D3 in order to get ss3.T
	  // If ok, then X == 0
	  X = cmplx(0, 1) * ss3.k * inv(D3 * V3) * (1 - D3);
	  
	  // Determine the error conditions
	  // Consider again only non-zero elements
	  for(km = 0; km < kmmax; km++){
	    for(kmp = 0; kmp < kmmax; kmp++){
	      if(abs(ss3.T(atom, atom)(km, kmp)) >= EMACH){
		//aabserr = DMAX(aabserr, abs(X(atom, atom)(km, kmp)));
		aabserr = DMAX(aabserr, abs(X(km, kmp)));
	      }
	    }
	  }
	  
#if VERBOSITY >= 2
	  cout << "MESSAGE from cpa():";
	  cout << " step "   << setw(3) << count;
	  cout << "  layer " << setw(3) << layer;
	  cout << "  atom "  << setw(3) << atom;
	  cout << "  absolute error: " << setw(13) << setprecision(10) << aabserr << endl;
#endif    

	  // Add the atomic error to the layer error
	  labserr += aabserr;
	  
	  // Compute the new ss3.T
	  // Consider again only non-zero elements
	  for(km = 0; km < kmmax; km++){
	    for(kmp = 0; kmp < kmmax; kmp++){
	      if(abs(ss3.T(atom, atom)(km, kmp)) >= EMACH){
		// ss3.T(atom, atom)(km, kmp) += CPA_MIX * X(atom, atom)(km, kmp);
		ss3.T(atom, atom)(km, kmp) += CPA_MIX * X(km, kmp);
	      }
	    }
	  }
	}
      }
      
#if VERBOSITY >= 2
      cout << "MESSAGE from cpa():";
      cout << " step " << setw(3) << count;
      cout << "  layer " << setw(3) << layer;
      cout << "  absolute error: " << setw(13) << setprecision(10) << labserr << endl;
#endif    

      // Add the layer error to the total error
      abserr += labserr;
    }

    if(count == 0){
      olderr = abserr;
    }
    if(olderr < EMACH){
      relerr = 1;
    }
    else{
      relerr =  abserr / olderr;
    }

#if VERBOSITY >= 1
    cout << "MESSAGE from cpa():";
    cout << " step " << setw(3) << count;
    cout << "  absolute error: " << setw(13) << setprecision(10) << abserr;
    cout << "  relative error: " << setw(13) << setprecision(10) << relerr << endl;
#endif    

    count++;

  } while(count < CPA_MAXITER && abserr > CPA_ABSERROR && relerr > CPA_RELERROR);

#if VERBOSITY >= 1
    cout << "MESSAGE from cpa(): After self-consistency loop at ";
    cout << " step " << setw(3) << count - 1;
    cout << "  absolute error: " << setw(13) << setprecision(10) << abserr;
    cout << "  relative error: " << setw(13) << setprecision(10) << relerr << endl;
#endif    

  // Error handling
  if(count >= CPA_MAXITER){
    cout << "WARNING from cpa(): t-matrices not converged.";
    cout << "  absolute error: " << setw(13) << abserr;
    cout << "  relative error: " << setw(13) << relerr << endl;
  }
}

// CPA3 self-consistency loop
void cpa3(job &j, actual &a, state &s, crystal &c, state &s2, crystal &c2, state &s3, crystal &c3, state &s4, state &sp, int lflagw){

  int                count;
  int                layer, slab, atom;
  int                km, kmp;

  double             conc, conc2, conc3;
  double             abserr, relerr, olderr, labserr, aabserr;
cout << "start CPA3 function"<<endl;

  //complexsupermatrix D, D2, D3, V, V2, V3, X;
  complexmatrix D, D2, D3, D4, V, V2, V3, V4, X;

  print_fixed_params_cpa();

  // Now make an initial guess for the single-site t-matrix of state s3 ...
  ata3(s, c, s2, c2, s3, c3, s4);

  // ... and keep only components which are large enough
  for(slab = 0; slab < c.nslab; slab++){
    cslab &cs  = c.slab[slab];
    sslab &ss4 = s4.slab[slab];

    for(atom = 0; atom < cs.natom; atom++){
      for(km = 0; km < kmmax; km++){
	for(kmp = 0; kmp < kmmax; kmp++){
	  if(abs(ss4.T(atom, atom)(km, kmp)) < EMACH){
	    ss4.T(atom, atom)(km, kmp) = 0;
	  }
	}
      }
    }
  }

  // Start of the CPA self-consistency loop
  olderr = 0;
  count  = 0;
  do{

cout << "start CPA3 loop"<<endl;
    // Compute the k-averaged V matrices for state s4

    copyT(s4, c2, sp); 
    vmatmean(j, a, s4, sp, c2, V_DIAGONAL);
    
    // Initialize the absolute error
    abserr = 0;
    
    // And now for all layers
    // NOTE: Each layer has to correspond 1-to-1 to a slab!
    for(layer = 0; layer < j.general.nlayer; layer++){
      slayer &sl  = s.layer[layer];
      slayer &sl2 = s2.layer[layer];
      slayer &sl3 = s3.layer[layer];
      slayer &sl4 = s4.layer[layer];


      sslice &sc  = *sl.slice;
      sslice &sc2 = *sl2.slice;
      sslice &sc3 = *sl3.slice;
      sslice &sc4 = *sl4.slice;

      sslab  &ss  = *sc.slab;
      sslab  &ss2 = *sc2.slab;
      sslab  &ss3 = *sc3.slab;
      sslab  &ss4 = *sc4.slab;

      clayer &cl  = c.layer[layer];
      clayer &cl2 = c2.layer[layer];
      clayer &cl3 = c3.layer[layer];

      cslice &cc  = *cl.slice;
      cslice &cc2 = *cl2.slice;
      cslice &cc3 = *cl3.slice;

      cslab  &cs  = *cc.slab;
      cslab  &cs2 = *cc2.slab;
      cslab  &cs3 = *cc3.slab;


      // Absolute error of this layer
      labserr = 0;

      // And for all atoms
      for(atom = 0; atom < cs.natom; atom++){
	catom  &ca  = cs.atom[atom];
	catom  &ca2 = cs2.atom[atom];
	catom  &ca3 = cs3.atom[atom];

	// If this is a defect atom...
        // - if the two single-site t-matrices are too similar, then do nothing
	if(norm(ss2.T(atom, atom) - ss.T(atom, atom)) > TLIMIT || norm(ss3.T(atom, atom) - ss.T(atom, atom)) > TLIMIT || norm(ss3.T(atom, atom) - ss2.T(atom, atom)) > TLIMIT ){
      
	  // Concentrations
	  conc  = ca.elem->conc;
	  conc2 = ca2.elem->conc;
	  conc3 = ca3.elem->conc;

	  
	  // Absolute error of this atom
	  aabserr = 0;
	  
	  // Compute the new impurity matrix D3 of s3 via the CPA condition
	  // If ok, then D3 == 1.
	  V4 = sl4.V[layer](atom, atom);
	  D  = inv(1 - cmplx(0, 1) * (1 / ss.k) * V4 * (ss.T(atom, atom)  - ss4.T(atom, atom)));
	  D2 = inv(1 - cmplx(0, 1) * (1 / ss.k) * V4 * (ss2.T(atom, atom) - ss4.T(atom, atom)));
	  D3 = inv(1 - cmplx(0, 1) * (1 / ss.k) * V4 * (ss3.T(atom, atom) - ss4.T(atom, atom)));

	  D4 = conc * D + conc2 * D2 + conc3 * D3;
	  
	  // Invert the functional form of D3 in order to get ss4.T
	  // If ok, then X == 0
	  X = cmplx(0, 1) * ss4.k * inv(D4 * V4) * (1 - D4);
	  
	  // Determine the error conditions
	  // Consider again only non-zero elements
	  for(km = 0; km < kmmax; km++){
	    for(kmp = 0; kmp < kmmax; kmp++){
	      if(abs(ss4.T(atom, atom)(km, kmp)) >= EMACH){
		//aabserr = DMAX(aabserr, abs(X(atom, atom)(km, kmp)));
		aabserr = DMAX(aabserr, abs(X(km, kmp)));
	      }
	    }
	  }
	  
#if VERBOSITY >= 2
	  cout << "MESSAGE from cpa3():";
	  cout << " step "   << setw(3) << count;
	  cout << "  layer " << setw(3) << layer;
	  cout << "  atom "  << setw(3) << atom;
	  cout << "  absolute error: " << setw(13) << setprecision(10) << aabserr << endl;
#endif    

	  // Add the atomic error to the layer error
	  labserr += aabserr;
	  
	  // Compute the new ss4.T
	  // Consider again only non-zero elements
	  for(km = 0; km < kmmax; km++){
	    for(kmp = 0; kmp < kmmax; kmp++){
	      if(abs(ss4.T(atom, atom)(km, kmp)) >= EMACH){
		// ss3.T(atom, atom)(km, kmp) += CPA_MIX * X(atom, atom)(km, kmp);
		ss4.T(atom, atom)(km, kmp) += CPA_MIX * X(km, kmp);
	      }
	    }
	  }
	}
      }
      
#if VERBOSITY >= 2
      cout << "MESSAGE from cpa3():";
      cout << " step " << setw(3) << count;
      cout << "  layer " << setw(3) << layer;
      cout << "  absolute error: " << setw(13) << setprecision(10) << labserr << endl;
#endif    

      // Add the layer error to the total error
      abserr += labserr;
    }

    if(count == 0){
      olderr = abserr;
    }
    if(olderr < EMACH){
      relerr = 1;
    }
    else{
      relerr =  abserr / olderr;
    }

#if VERBOSITY >= 1
    cout << "MESSAGE from cpa3():";
    cout << " step " << setw(3) << count;
    cout << "  absolute error: " << setw(13) << setprecision(10) << abserr;
    cout << "  relative error: " << setw(13) << setprecision(10) << relerr << endl;
#endif    

    count++;

  } while(count < CPA_MAXITER && abserr > CPA_ABSERROR && relerr > CPA_RELERROR);

#if VERBOSITY >= 1
    cout << "MESSAGE from cpa3(): After self-consistency loop at ";
    cout << " step " << setw(3) << count - 1;
    cout << "  absolute error: " << setw(13) << setprecision(10) << abserr;
    cout << "  relative error: " << setw(13) << setprecision(10) << relerr << endl;
#endif    

  // Error handling
  if(count >= CPA_MAXITER){
    cout << "WARNING from cpa3(): t-matrices not converged.";
    cout << "  absolute error: " << setw(13) << abserr;
    cout << "  relative error: " << setw(13) << relerr << endl;
  }
}

#undef CPA_MIX
#undef CPA_MAXITER
#undef CPA_ABSERROR
#undef CPA_RELERROR
#undef TLIMIT

#undef VERBOSITY
