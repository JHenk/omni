// damping.h
//
// enthaelt Prototypen der in dm.C definierten Funktionen
//

#ifndef _DAMPING_
#define _DAMPING_

#include <iostream>

#include "job.h"
#include "state.h"
#include "crystal.h"

void dampingrs(job &j, state &sdis, state &s, crystal &c, std::ostream &os, int& ref_mag);

#endif
