// getinput.h
//
// reads the input data
//

#ifndef _GETINPUT_
#define _GETINPUT_

#include <iostream>

#include "job.h"

// Function for reading job parameters
void getinput(job &j, crystal &c, crystal &c2, crystal &c3, std::istream &is);

#endif
