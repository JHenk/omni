//
// Adaptive grid method (2dim. case) for Bloch-wave tunneling
//
// 11/09/00 Juergen Henk
//

#ifndef _BINTEGRATESC_
#define _BINTEGRATESC_

void bwtmeanSC(job &j, actual &a, state &l, state &r, state &l2, state &r2,\
               crystal &c, crystal &c2, double &TotalSumTpp_spec, double &TotalSumTpm_spec,\
	       double &TotalSumTmp_spec, double &TotalSumTmm_spec, double &TotalSumTpp_diff,\
	       double &TotalSumTpm_diff, double &TotalSumTmp_diff, double &TotalSumTmm_diff,\
	       int lflagw, std::ofstream &out);

#endif
