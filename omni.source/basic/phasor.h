// phasor.h
//
// enthaelt die Prototypen der in phasor.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _PHASOR_
#define _PHASOR_

#include "state.h"
#include "crystal.h"
#include "beam.h"

void phasor(sslice &sc, cslice &cc, sslice &scp, beamset &b);

#endif
