// leedcf.h
//
// enthaelt Prototypen der in leedcf.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _LEEDCF_
#define _LEEDCF_

#include "job.h"
#include "state.h"
#include "beam.h"
#include "crystal.h"

void leedcf(actual &a, job &j, state &u, beamset &b, crystal &c, std::ostream &os, std::ostream &out=std::cout);


#endif
