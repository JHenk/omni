// diracfp.h
//
// enthaelt Prototypen der Funktionen aus diracfp.C
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _DIRACFP_
#define _DIRACFP_

#include "potential.h"
#include "state.h"

void diracfp(selem &e, potential &p, double x, int flagw);
void diracfpx(selem &e, potential &p, double x, int flagw);

#endif
