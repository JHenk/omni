//
// Affine transformations
//

#ifndef _AFFIN_
#define _AFFIN_

#include "vector2.h"


// Affine transformation - standard triangle -> 2-square
// vin is transformed into vout
void affintrans_stdtri2sq(vector2 &vin, vector2 &vout);

// Affine transformation - 2-square -> standard triangle
// vin is transformed into vout
void affintrans_sq2stdtri(vector2 &vin, vector2 &vout);

// Affine transformation - standard triangle -> initial triangle
// p1, p2, and p3 define the initial triangle
// vin is transformed into vout
void affintrans_stdtri2tri(vector2 &p1, vector2 &p2, vector2 &p3, vector2 &vin, vector2 &vout);

// Jacobian of the affine transformation initial triangle -> standard triangle
double jacobian_affintrans_stdtri2tri(vector2 &p1, vector2 &p2, vector2 &p3);

// Affine transformation - initial triangle -> standard triangle
// p1, p2, and p3 define the initial triangle
// vin is transformed into vout
void affintrans_tri2stdtri(vector2 &p1, vector2 &p2, vector2 &p3, vector2 &vin, vector2 &vout);


// Affine transformation - 2-square -> parallelogram
// p1, p2, and p4 define the parallelogram, see note.
// vin is transformed into vout
void affintrans_sq2par(vector2 &p1, vector2 &p2, vector2 &p4, vector2 &vin, vector2 &vout);

// Jacobian of the affine transformation 2-square -> parallogram
double jacobian_affintrans_sq2par(vector2 &p1, vector2 &p2, vector2 &p4);


// Affine transformation - parallelogram -> 2-square
// p1, p2, and p4 define the parallelogram, see note.
// vin is transformed into vout
void affintrans_par2sq(vector2 &p1, vector2 &p2, vector2 &p4, vector2 &vin, vector2 &vout);

#endif
