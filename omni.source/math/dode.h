//
// ODE solver (double)
//

#ifndef _DODE_
#define _DODE_

#include "dmatrix.h"

// The general driver function
void dode(int odetype, int nvar, int kmax, int &kount, double x1, double x2, double dxsav, double eps, double h1,
	  doublediagmatrix &ystart, doublediagmatrix &xp, doublematrix &yp,
	  void (*odefunc)(double, doublediagmatrix &, doublediagmatrix &));


#endif
