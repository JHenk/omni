//
// Functions for the computation of Gaunt coefficients
//
// JH - 26/05/99


#include <iostream>
#include <cmath>

#include "platform.h"
#include "intmath.h"

using namespace std;

// Calculates (k!) / ((k-i)!)
// k, k-i must be >= 0
double rfctrl(int k, int i){

  int in, it, l;
  
  double r;
  
  r = 1;
  
  if(k >= 0 && k-i >= 0){
    if(i != 0){
      in = k - i + 1;
      it = k;
      for(l = in; l <= it; l++){
	r = r * l;
      }
    }
  }
  else{
    cerr << "ERROR from rfctrl(): " << k << " " << i << endl;
    myexit(1);
  }
  return r;
}


// Calculates factorial of k
// k must be >= 0
double factrl(int k){

  int i;

  double f;

  f = 1;

  if(k < 0){
    cerr << "ERROR from factrl(): " << k << endl;
    myexit(1);
  }
  else{
    if(k > 0){
      for(i = 1; i <= k; i++){
	f *= i;
      }
    }
  }
  return f;
}


// Calculates of ylm* x yllmm* x yl'm' x sqr(4pi)
// condon & shortly convention -- using racah equation quoted by rotenberg

double gaunt(int l1, int l2, int l3, int m1, int m2, int m3){

  double g;

  double xx, sum, arg, sgn;

  int j1,  j2,  j3,  jj;
  int j1h, j2h, j3h, jjh;
  int ii1, ii2, nn1, nn2;
  int k, k1, k2;
  
  g = 0;

  if((-m1-m2+m3) != 0){
    return g;
  }

  if(iabs(m1) > l1){
    return g;
  }
  if(iabs(m2) > l2){
    return g;
  }
  if(iabs(m3) > l3){
    return g;
  }

  jj = l1 + l2 + l3;

  if( (jj / 2 ) * 2 != jj){
    return g;
  }

  j1 = jj -2 * l3;
  j2 = jj -2 * l2;
  j3 = jj -2 * l1;


  if(j1 < 0){
    return g;
  }
  if(j2 < 0){
    return g;
  }
  if(j3 < 0){
    return g;
  }

  xx = (2 * l1 + 1) * (2 * l2 + 1) * ( 2 * l3 + 1);

  j1h = j1 / 2;
  j2h = j2 / 2;
  j3h = j3 / 2;
  jjh = jj / 2;

  g = pow(double(-1), j1h) * sqrt(xx);
  g = g * ((factrl(j2) * factrl(j3)) / factrl(jj+1));
  g = g * (factrl(jjh) / (factrl(j1h) * factrl(j2h) * factrl(j3h)));

  xx = factrl(l3+m3) * factrl(l3-m3);
  if(m2 >= 0){
    xx = xx * rfctrl(l2+m2,2*m2);
  }
  if(m2 < 0){
    xx = xx / rfctrl(l2-m2,-2*m2);
  }
  if(m1 >= 0){
    xx = xx / rfctrl(l1+m1,2*m1);
  }
  if(m1 < 0){
    xx = xx * rfctrl(l1-m1,-2*m1);
  }

  g = g * sqrt(xx);

  ii1 = l3 - l2 - m1;
  ii2 = l3 - l1 + m2;
  //k1  = -min0(0, ii1, ii2);
  k1  = -min(0, min(ii1, ii2));

  nn1 = l1 + m1;
  nn2 = l2 - m2;
  //k2 = min0(j1, nn1, nn2);
  k2 = min(j1, min(nn1, nn2));

  k = k1;

  sum = 0;
  sgn = 1;

  if(k > 0){
    sgn = pow(double(-1), k);
  }

  for(; k <= k2; k++){
    arg = sgn * (rfctrl(nn1,k) / factrl(k));
    arg = arg * (rfctrl(nn2,k) / factrl(ii1+k));
    arg = arg * (rfctrl(j1,k) / factrl(ii2+k));
    sum = sum + arg;
    sgn = -sgn; 
  }
  
  g *= sum;
  
  return g;
}
