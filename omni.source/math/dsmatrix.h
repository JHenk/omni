// smatrix.h
//
// deklariert reelle Super-Matrizen, sowie mathematische Operationen zwischen
// Matrizen und reellen Zahlen.
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//
// Hinweis: eigentlich waere es sinnvoller Matrizen und Vektoren durch
// templates zu definieren. Da templates zu dieser Zeit jedoch noch nicht
// sehr weit verbreitet sind ( zumindestens noch seltener als C++ Compiler),
// findet die Definition hier ohne templates statt. Es sollte jedoch recht
// einfach sein, die Definitionen auf templates umzustellen.
//
// Definition von DEBUG fuegt ausfuehrliche Fehlertests ein.
//

#ifndef _DSMATRIX_
#define _DSMATRIX_

#include "platform.h"

#include "dmatrix.h"

#ifndef NULL
#define NULL    0
#endif

//
// Diese Klassen werden definiert:
//

class doublesupermatrix;

class doubletempsupermatrix;

//
// Definition der Super-Matrixklasse
//

class doublesupermatrix{
 protected:
  int		r, c;		// Anzahl Zeilen (r) und Spalten (c)
  int     	n;		// Gesamtgroesse der Super-Matrix
  int		rs, cs;		// Anzahl Zeilen und Spalten je Block
  doublematrix 	*m;             // Zeiger auf das eigentliche Feld
 public:
  // Die Konstruktoren
  doublesupermatrix();
  doublesupermatrix(int, int, int, int);
  // Der Kopierkonstruktor
  doublesupermatrix(const doublesupermatrix &);
  // Der Umkopierer
  doublesupermatrix(const doubletempsupermatrix &);
  // Der Destruktor
  ~doublesupermatrix();
  // Zugriff auf einzelne Matrixelemente
  doublematrix &operator()(int, int) const;
  // Die Zuweisung
  doublesupermatrix &operator=(double);
  doublesupermatrix &operator=(doublematrix &);
  doublesupermatrix &operator=(const doublesupermatrix &);
  doublesupermatrix &operator=(const doubletempsupermatrix &);
  // Setze die Groesse
  doublesupermatrix &setsize(int, int, int, int);
  // Zuweisungen zwischen Matrizen und Supermatrizen
  friend doublematrix &doublematrix::operator=(const doublesupermatrix &);
  // Unaere Operatoren
  friend doubletempsupermatrix operator+(const doublesupermatrix &);
  friend doubletempsupermatrix operator-(const doublesupermatrix &);
  // Binaere Operatoren
  friend doubletempsupermatrix operator*(const doublesupermatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator+(const doublesupermatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator-(const doublesupermatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator*(const doublesupermatrix &, const doublematrix &);
  friend doubletempsupermatrix operator+(const doublesupermatrix &, const doublematrix &);
  friend doubletempsupermatrix operator-(const doublesupermatrix &, const doublematrix &);
  friend doubletempsupermatrix operator*(const doublematrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator+(const doublematrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator-(const doublematrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator*(const doublesupermatrix &, const doublediagmatrix &);
  friend doubletempsupermatrix operator+(const doublesupermatrix &, const doublediagmatrix &);
  friend doubletempsupermatrix operator-(const doublesupermatrix &, const doublediagmatrix &);
  friend doubletempsupermatrix operator*(const doublediagmatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator+(const doublediagmatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator-(const doublediagmatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator*(const doublesupermatrix &, double);
  friend doubletempsupermatrix operator+(const doublesupermatrix &, double);
  friend doubletempsupermatrix operator-(const doublesupermatrix &, double);
  friend doubletempsupermatrix operator*(double, const doublesupermatrix &);
  friend doubletempsupermatrix operator+(double, const doublesupermatrix &);
  friend doubletempsupermatrix operator-(double, const doublesupermatrix &);
  // Funktionen
  friend doubletempsupermatrix inv(const doublesupermatrix &);
  friend doubletempsupermatrix transp(const doublesupermatrix &);
};

//
// Definition der temporaeren Super-Matrixklasse
//

class doubletempsupermatrix{
 protected:
  int		r, c;		// Anzahl Zeilen (r) und Spalten (c)
  int     	n;              // Gesamtgroesse der Super-Matrix
  int		rs, cs;		// Anzahl Zeilen und Spalten je Block
  doublematrix 	*m;             // Zeiger auf das eigentliche Feld
 public:
  // Der Konstruktor
  doubletempsupermatrix(int, int, int, int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class doublesupermatrix;
  // Unaere Operatoren
  friend doubletempsupermatrix operator+(const doublesupermatrix &);
  friend doubletempsupermatrix operator-(const doublesupermatrix &);
  // Binaere Operatoren
  friend doubletempsupermatrix operator*(const doublesupermatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator+(const doublesupermatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator-(const doublesupermatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator*(const doublesupermatrix &, const doublematrix &);
  friend doubletempsupermatrix operator+(const doublesupermatrix &, const doublematrix &);
  friend doubletempsupermatrix operator-(const doublesupermatrix &, const doublematrix &);
  friend doubletempsupermatrix operator*(const doublematrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator+(const doublematrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator-(const doublematrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator*(const doublesupermatrix &, const doublediagmatrix &);
  friend doubletempsupermatrix operator+(const doublesupermatrix &, const doublediagmatrix &);
  friend doubletempsupermatrix operator-(const doublesupermatrix &, const doublediagmatrix &);
  friend doubletempsupermatrix operator*(const doublediagmatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator+(const doublediagmatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator-(const doublediagmatrix &, const doublesupermatrix &);
  friend doubletempsupermatrix operator*(const doublesupermatrix &, double);
  friend doubletempsupermatrix operator+(const doublesupermatrix &, double);
  friend doubletempsupermatrix operator-(const doublesupermatrix &, double);
  friend doubletempsupermatrix operator*(double, const doublesupermatrix &);
  friend doubletempsupermatrix operator+(double, const doublesupermatrix &);
  friend doubletempsupermatrix operator-(double, const doublesupermatrix &);
  // Funktionen
  friend doubletempsupermatrix inv(const doublesupermatrix &);
  friend doubletempsupermatrix transp(const doublesupermatrix &);
};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine Super-Matrix
//

inline  doublesupermatrix::doublesupermatrix()

{
  // Keine Zeilen und Spalten
  r = 0;
  c = 0;
  // Keine Groesse
  n = 0;
  // Keine Zeilen und Spalten pro Block
  rs = 0;
  cs = 0;
  // Kein Speicher
  m = NULL;
}

//
// Erzeuge eine beliebige Super-Matrix
//

inline  doublesupermatrix::doublesupermatrix(int n1, int n2, int m1, int m2)

{
#ifdef DEBUG
  if(n1 <= 0 || n2 <= 0){
    cerr << "ERROR from doublesupermatrix: Invalid dimensions\n";
    myexit(1);
  }
#endif
  // Anzahl Zeilen und Spalten
  r = n1;
  c = n2;
  // Groesse berechnen
  n = r * c;
  // Anzahl Zeilen und Spalten
  rs = m1;
  cs = m2;
  // Noetigen Speicher anfordern
  m = new doublematrix[n];
}

//
// Der Umkopierer
//

inline  doublesupermatrix::doublesupermatrix(const doubletempsupermatrix &z)

{
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Groesse kopieren
  n = z.n;
  // Abmessungen je Block kopieren
  rs = z.rs;
  cs = z.cs;
  // Einfach Zeiger kopieren
  m = z.m;
}

//
// Der Destruktor
//

inline  doublesupermatrix::~doublesupermatrix()

{
  // Gibt den Speicher frei (Test auf NULL nicht noetig?)
  delete[] m;
}

//
// Zugriff auf einzelne Super-Matrixelemente
//

inline  doublematrix &doublesupermatrix::operator()(int n1, int n2) const

{
#ifdef DEBUG
  if(n1 < 0 || n1 >= r || n2 < 0 || n2 >= c){
    cerr << "ERROR from doublesupermatrix: Subscript out of range\n";
    myexit(1);
  }
#endif
  // Gib Referenz auf Element zurueck
  return m[n1 * c + n2];
}

//
// Die Zuweisung
//

inline  doublesupermatrix &doublesupermatrix::operator=(const doubletempsupermatrix &z)

{
  // Gibt den Speicher frei (Test auf NULL nicht noetig?)
  delete[] m;
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Groesse kopieren
  n = z.n;
  // Abmessungen por Block kopieren
  rs = z.rs;
  cs = z.cs;
  // Einfach Zeiger kopieren
  m = z.m;
  return *this;
}

//
// Zuweisungen zwischen Matrizen und Supermatrizen
//

  inline doublematrix &doublematrix::operator=(const doublesupermatrix &z)

  {
#ifdef DEBUG
    if(z.r != 1 || z.c != 1){
      cerr << "ERROR from doublematrix: Invalid dimensions\n";
      myexit(1);
    }
#endif
    *this = z.m[0];
    return *this;
  }

  //
  // Setze die Groesse einer Super-Matrix
  //

    inline  doublesupermatrix &doublesupermatrix::setsize(int n1, int n2, int m1, int m2)

    {
#ifdef DEBUG
      if(n1 <= 0 || n2 <= 0){
	cerr << "ERROR from doublesupermatrix: Invalid dimensions\n";
	myexit(1);
      }
#endif
      if(n != n1 * n2){
	// Groesse berechnen
	n = n1 * n2;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] m;
	// Noetigen Speicher anfordern
	m = new doublematrix[n];
      }
      // Anzahl Zeilen und Spalten
      r = n1;
      c = n2;
      // Anzahl Zeilen und Spalten pro Block
      rs = m1;
      cs = m2;
      return *this;
    }

//
// Erzeuge eine temporaere Super-Matrix
//

inline  doubletempsupermatrix::doubletempsupermatrix(int n1, int n2, int m1, int m2)

{
  // Anzahl Zeilen und Spalten
  r = n1;
  c = n2;
  // Groesse berechnen
  n = n1 * n2;
  // Anzahl Zeilen und Spalten pro Block
  rs = m1;
  cs = m2;
  // Noetigen Speicher anfordern
  m = new doublematrix[n];
}

#endif
