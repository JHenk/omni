// dmatrix.h
//
// deklariert reele Matrizen, sowie mathematische Operationen zwischen Matrizen
// und reellen Zahlen.
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//
// Hinweis: eigentlich waere es sinnvoller Matrizen und Vektoren durch
// templates zu definieren. Da templates zu dieser Zeit jedoch noch nicht
// sehr weit verbreitet sind ( zumindestens noch seltener als C++ Compiler),
// findet die Definition hier ohne templates statt. Es sollte jedoch recht
// einfach sein, die Definitionen auf templates umzustellen.
//
// Definition von DEBUG fuegt ausfuehrliche Fehlertests ein.
//

#ifndef _DMATRIX_
#define _DMATRIX_

#include <iostream> 
#include <cstdlib>
#include <string>
#include <cstring>

#include "platform.h"

#ifndef NULL
#define NULL    0
#endif

//
// Diese Klassen werden definiert:
//

class doublematrix;
class doublediagmatrix;

class doublesubmatrix;
class doublesubdiagmatrix;

class doubletempmatrix;
class doubletempdiagmatrix;

//
// Folgende Klasse wird hier nicht definiert
//

class doublesupermatrix;

class complex;

class complexmatrix;
class complexdiagmatrix;

class complextempmatrix;
class complextempdiagmatrix;

//
// Definition der Matrixklasse
//

class doublematrix{
protected:
	int     	r, c;           // Anzahl Zeilen (r) und Spalten (c)
	int     	n;              // Gesamtgroesse der Matrix
	double	 	*a;             // Zeiger auf das eigentliche Feld
public:
// Die Konstruktoren
	doublematrix();
	doublematrix(int, int);
// Der Kopierkonstruktor
	doublematrix(const doublematrix &);
// Der Umkopierer
	doublematrix(const doublesubmatrix &);
	doublematrix(const doubletempmatrix &);
// Der Destruktor
	~doublematrix();
// Zugriff auf einzelne Matrixelemente
	double &operator()(int, int) const;
// Die Zuweisung
	doublematrix &operator=(double);
	doublematrix &operator=(const doublematrix &);
	doublematrix &operator=(const doublesubmatrix &);
	doublematrix &operator=(const doubletempmatrix &);
	doublematrix &operator=(const doublediagmatrix &);
	doublematrix &operator=(const doublesupermatrix &);
// Setze die Groesse
	doublematrix &setsize(int, int);
// Test, ob Matrix benutzt ist
	friend int used(const doublematrix &);
// Alle meine Freunde
	friend class doublesubmatrix;
// Unaere Operatoren
	friend doubletempmatrix operator+(const doublematrix &);
	friend doubletempmatrix operator-(const doublematrix &);
// Binaere Operatoren
	friend doubletempmatrix operator*(const doublematrix &, const doublematrix &);
	friend doubletempmatrix operator+(const doublematrix &, const doublematrix &);
	friend doubletempmatrix operator-(const doublematrix &, const doublematrix &);
	friend doubletempmatrix operator*(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator+(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator-(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator*(const doublediagmatrix &, const doublematrix &);
	friend doubletempmatrix operator+(const doublediagmatrix &, const doublematrix &);
	friend doubletempmatrix operator-(const doublediagmatrix &, const doublematrix &);
	friend doubletempmatrix operator*(const doublematrix &, double);
	friend doubletempmatrix operator+(const doublematrix &, double);
	friend doubletempmatrix operator-(const doublematrix &, double);
	friend doubletempmatrix operator*(double, const doublematrix &);
	friend doubletempmatrix operator+(double, const doublematrix &);
	friend doubletempmatrix operator-(double, const doublematrix &);
	friend complextempmatrix operator*(const doublematrix &, const complexmatrix &);
	friend complextempmatrix operator+(const doublematrix &, const complexmatrix &);
	friend complextempmatrix operator-(const doublematrix &, const complexmatrix &);
	friend complextempmatrix operator*(const complexmatrix &, const doublematrix &);
	friend complextempmatrix operator+(const complexmatrix &, const doublematrix &);
	friend complextempmatrix operator-(const complexmatrix &, const doublematrix &);
	friend complextempmatrix operator*(const doublematrix &, const complexdiagmatrix &);
	friend complextempmatrix operator+(const doublematrix &, const complexdiagmatrix &);
	friend complextempmatrix operator-(const doublematrix &, const complexdiagmatrix &);
	friend complextempmatrix operator*(const complexdiagmatrix &, const doublematrix &);
	friend complextempmatrix operator+(const complexdiagmatrix &, const doublematrix &);
	friend complextempmatrix operator-(const complexdiagmatrix &, const doublematrix &);
	friend complextempmatrix operator*(const doublematrix &, const complex &);
	friend complextempmatrix operator+(const doublematrix &, const complex &);
	friend complextempmatrix operator-(const doublematrix &, const complex &);
	friend complextempmatrix operator*(const complex &, const doublematrix &);
	friend complextempmatrix operator+(const complex &, const doublematrix &);
	friend complextempmatrix operator-(const complex &, const doublematrix &);
// Funktionen
	friend doubletempmatrix inv(const doublematrix &);
	friend doubletempmatrix transp(const doublematrix &);
	friend double norm(const doublematrix &);
	friend int diff(const doublematrix &, const doublematrix &);
	friend double trace(const doublematrix &);
// Ein- und Ausgabe
	friend std::ostream &operator<<(std::ostream &, const doublematrix &);
	friend std::istream &operator>>(std::istream &, const doublematrix &);
	};

//
// Definition der diagonalen Matrixklasse
//

class doublediagmatrix{
protected:
	int     	r;		// Anzahl Zeilen (r) und Spalten (r)
	double	 	*a;		// Zeiger auf das eigentliche Feld
public:
// Die Konstruktoren
	doublediagmatrix();
	doublediagmatrix(int);
// Der Kopierkonstruktor
	doublediagmatrix(const doublediagmatrix &);
// Der Umkopierer
	doublediagmatrix(const doublesubdiagmatrix &);
	doublediagmatrix(const doubletempdiagmatrix &);
// Der Destruktor
	~doublediagmatrix();
// Zugriff auf einzelne Matrixelemente
	double &operator()(int) const;
// Die Zuweisung
	doublediagmatrix &operator=(double);
	doublediagmatrix &operator=(const doublediagmatrix &);
	doublediagmatrix &operator=(const doublesubdiagmatrix &);
	doublediagmatrix &operator=(const doubletempdiagmatrix &);
// Setze die Groesse
	doublediagmatrix &setsize(int);
// Test, ob Matrix benutzt ist
	friend int used(const doublediagmatrix &);
// Alle meine Freunde
	friend class doublematrix;
	friend class doublesubdiagmatrix;
// Unaere Operatoren
	friend doubletempdiagmatrix operator+(const doublediagmatrix &);
	friend doubletempdiagmatrix operator-(const doublediagmatrix &);
// Binaere Operatoren
	friend doubletempdiagmatrix operator*(const doublediagmatrix &, const doublediagmatrix &);
	friend doubletempdiagmatrix operator+(const doublediagmatrix &, const doublediagmatrix &);
	friend doubletempdiagmatrix operator-(const doublediagmatrix &, const doublediagmatrix &);
	friend doubletempmatrix operator*(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator+(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator-(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator*(const doublediagmatrix &, const doublematrix &);
	friend doubletempmatrix operator+(const doublediagmatrix &, const doublematrix &);
	friend doubletempmatrix operator-(const doublediagmatrix &, const doublematrix &);
	friend doubletempdiagmatrix operator*(const doublediagmatrix &, double);
	friend doubletempdiagmatrix operator+(const doublediagmatrix &, double);
	friend doubletempdiagmatrix operator-(const doublediagmatrix &, double);
	friend doubletempdiagmatrix operator*(double, const doublediagmatrix &);
	friend doubletempdiagmatrix operator+(double, const doublediagmatrix &);
	friend doubletempdiagmatrix operator-(double, const doublediagmatrix &);
	friend complextempmatrix operator*(const doublediagmatrix &, const complexmatrix &);
	friend complextempmatrix operator+(const doublediagmatrix &, const complexmatrix &);
	friend complextempmatrix operator-(const doublediagmatrix &, const complexmatrix &);
	friend complextempmatrix operator*(const complexmatrix &, const doublediagmatrix &);
	friend complextempmatrix operator+(const complexmatrix &, const doublediagmatrix &);
	friend complextempmatrix operator-(const complexmatrix &, const doublediagmatrix &);
	friend complextempdiagmatrix operator*(const doublediagmatrix &, const complexdiagmatrix &);
	friend complextempdiagmatrix operator+(const doublediagmatrix &, const complexdiagmatrix &);
	friend complextempdiagmatrix operator-(const doublediagmatrix &, const complexdiagmatrix &);
	friend complextempdiagmatrix operator*(const complexdiagmatrix &, const doublediagmatrix &);
	friend complextempdiagmatrix operator+(const complexdiagmatrix &, const doublediagmatrix &);
	friend complextempdiagmatrix operator-(const complexdiagmatrix &, const doublediagmatrix &);
	friend complextempdiagmatrix operator*(const doublediagmatrix &, const complex &);
	friend complextempdiagmatrix operator+(const doublediagmatrix &, const complex &);
	friend complextempdiagmatrix operator-(const doublediagmatrix &, const complex &);
	friend complextempdiagmatrix operator*(const complex &, const doublediagmatrix &);
	friend complextempdiagmatrix operator+(const complex &, const doublediagmatrix &);
	friend complextempdiagmatrix operator-(const complex &, const doublediagmatrix &);
// Funktionen
	friend doubletempdiagmatrix inv(const doublediagmatrix &);
	friend doubletempdiagmatrix transp(const doublediagmatrix &);
	friend doubletempdiagmatrix abs(const doublediagmatrix &);
	friend double norm(const doublediagmatrix &);
	friend int diff(const doublediagmatrix &, const doublediagmatrix &);
	friend double trace(const doublediagmatrix &a);
// Ein- und Ausgabe
	friend std::ostream &operator<<(std::ostream &, const doublediagmatrix &);
	friend std::istream &operator>>(std::istream &, const doublediagmatrix &);
	};

//
// Definition der Sub-Matrixklasse
//

class doublesubmatrix{
protected:
	int		r0, c0;		// Startzeile (r0) und Startspalte (c0)
	int     	r, c;           // Anzahl Zeilen (r) und Spalten (c)
	const doublematrix
			&ref;           // Die Referenzierte Matrix
public:
// Der Konstruktor
	doublesubmatrix(const doublematrix &, int, int, int, int);
// Zugriff auf einzelne Matrixelemente
	double &operator()(int, int) const;
// Die Zuweisung
	doublesubmatrix &operator=(const doublematrix &);
	doublesubmatrix &operator=(const doublesubmatrix &);
// Alle meine Freunde
	friend class doublematrix;
	};

//
// Definition der diagonalen Sub-Matrixklasse
//

class doublesubdiagmatrix{
protected:
	int		r0;		// Startzeile (r0) und Startspalte (r0)
	int     	r;		// Anzahl Zeilen (r) und Spalten (r)
	const doublediagmatrix
			&ref;           // Die Referenzierte Matrix
public:
// Der Konstruktor
	doublesubdiagmatrix(const doublediagmatrix &, int, int);
// Zugriff auf einzelne Matrixelemente
	double &operator()(int) const;
// Die Zuweisung
	doublesubdiagmatrix &operator=(const doublediagmatrix &);
	doublesubdiagmatrix &operator=(const doublesubdiagmatrix &);
// Alle meine Freunde
	friend class doublediagmatrix;
	};

//
// Definition der temporaeren Matrixklasse
//

class doubletempmatrix{
protected:
	int     	r, c;           // Anzahl Spalten (r) und Zeilen (c)
	int     	n;              // Gesamtgroesse der Matrix
	double 	*a;             // Zeiger auf das eigentliche Feld
public:
// Der Konstruktor
	doubletempmatrix(int, int);
// Kein Destruktor
// Alle meine Freunde
	friend class doublematrix;
// Unaere Operatoren
	friend doubletempmatrix operator+(const doublematrix &);
	friend doubletempmatrix operator-(const doublematrix &);
// Binaere Operatoren
	friend doubletempmatrix operator*(const doublematrix &, const doublematrix &);
	friend doubletempmatrix operator+(const doublematrix &, const doublematrix &);
	friend doubletempmatrix operator-(const doublematrix &, const doublematrix &);
	friend doubletempmatrix operator*(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator+(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator-(const doublematrix &, const doublediagmatrix &);
	friend doubletempmatrix operator*(const doublediagmatrix &, const doublematrix &);
	friend doubletempmatrix operator+(const doublediagmatrix &, const doublematrix &);
	friend doubletempmatrix operator-(const doublediagmatrix &, const doublematrix &);
	friend doubletempmatrix operator*(const doublematrix &, double);
	friend doubletempmatrix operator+(const doublematrix &, double);
	friend doubletempmatrix operator-(const doublematrix &, double);
	friend doubletempmatrix operator*(double, const doublematrix &);
	friend doubletempmatrix operator+(double, const doublematrix &);
	friend doubletempmatrix operator-(double, const doublematrix &);
// Funktionen
	friend doubletempmatrix inv(const doublematrix &);
	friend doubletempmatrix transp(const doublematrix &);
	};

//
// Definition der temporaeren diagonalen Matrixklasse
//

class doubletempdiagmatrix{
protected:
	int     	r;              // Anzahl Spalten (r) und Zeilen (r)
	double 	*a;             // Zeiger auf das eigentliche Feld
public:
// Der Konstruktor
	doubletempdiagmatrix(int);
// Kein Destruktor
// Alle meine Freunde
	friend class doublediagmatrix;
// Unaere Operatoren
	friend doubletempdiagmatrix operator+(const doublediagmatrix &);
	friend doubletempdiagmatrix operator-(const doublediagmatrix &);
// Binaere Operatoren
	friend doubletempdiagmatrix operator*(const doublediagmatrix &, const doublediagmatrix &);
	friend doubletempdiagmatrix operator+(const doublediagmatrix &, const doublediagmatrix &);
	friend doubletempdiagmatrix operator-(const doublediagmatrix &, const doublediagmatrix &);
	friend doubletempdiagmatrix operator*(const doublediagmatrix &, double);
	friend doubletempdiagmatrix operator+(const doublediagmatrix &, double);
	friend doubletempdiagmatrix operator-(const doublediagmatrix &, double);
	friend doubletempdiagmatrix operator*(double, const doublediagmatrix &);
	friend doubletempdiagmatrix operator+(double, const doublediagmatrix &);
	friend doubletempdiagmatrix operator-(double, const doublediagmatrix &);
// Funktionen
	friend doubletempdiagmatrix inv(const doublediagmatrix &);
	friend doubletempdiagmatrix transp(const doublediagmatrix &);
	friend doubletempdiagmatrix abs(const doublediagmatrix &);
	};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine Matrix
//

inline  doublematrix::doublematrix()

{
// Keine Zeilen und Spalten
r = 0;
c = 0;
// Keine Groesse
n = 0;
// Kein Speicher
a = NULL;
}

//
// Erzeuge eine beliebige Matrix
//

inline  doublematrix::doublematrix(int n1, int n2)

{
#ifdef DEBUG
if(n1 <= 0 || n2 <= 0){
	cerr << "ERROR from doublematrix: Invalid dimensions\n";
	myexit(1);
	}
#endif
// Anzahl Zeilen und Spalten
r = n1;
c = n2;
// Groesse berechnen
n = r * c;
// Noetigen Speicher anfordern
a = new double[n];
}

//
// Der Kopierkonstruktor
//

inline  doublematrix::doublematrix(const doublematrix &z)

{
// Abmessungen kopieren
r = z.r;
c = z.c;
// Groesse kopieren
n = z.n;
// Speicher anfordern
a = new double[n];
// Und alles kopieren
memcpy(a, z.a, n * sizeof(double));
}

inline  doublematrix::doublematrix(const doubletempmatrix &z)

{
// Abmessungen kopieren
r = z.r;
c = z.c;
// Groesse kopieren
n = z.n;
// Einfach Zeiger kopieren
a = z.a;
}

//
// Der Destruktor
//

inline  doublematrix::~doublematrix()

{
// Gibt den Speicher frei (Test auf NULL nicht noetig?)
delete[] a;
}

//
// Die Zuweisung
//

inline  doublematrix &doublematrix::operator=(const doubletempmatrix &z)

{
// Gibt den Speicher frei (Test auf NULL nicht noetig?)
delete[] a;
// Abmessungen kopieren
r = z.r;
c = z.c;
// Groesse kopieren
n = z.n;
// Einfach Zeiger kopieren
a = z.a;
return *this;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline  double &doublematrix::operator()(int n1, int n2) const

{
#ifdef DEBUG
if(n1 < 0 || n1 >= r || n2 < 0 || n2 >= c){
	cerr << "ERROR from doublematrix: Subscript out of range\n";
	myexit(1);
	}
#endif
// Gib Referenz auf Element zurueck
return a[n1 * c + n2];
}

//
// Setze die Groesse einer Matrix
//

inline  doublematrix &doublematrix::setsize(int n1, int n2)

{
#ifdef DEBUG
if(n1 < 0 || n2 < 0){
	cerr << "ERROR from doublematrix: Invalid dimensions\n";
	myexit(1);
	}
#endif
if(n != n1 * n2){
	// Groesse berechnen
	n = n1 * n2;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	if(n != 0){
		// Noetigen Speicher anfordern
		a = new double[n];
		}
	else{
		// Keinen Speicher anfordern
		a = NULL;
		}
	}
// Anzahl Zeilen und Spalten
r = n1;
c = n2;
return *this;
}

//
// Test, ob die Matrix benutzt worden ist
//

inline int used(const doublematrix &z)

{
return z.n != 0;
}

//
// Erzeuge Platzhalter fuer eine diagonale Matrix
//

inline  doublediagmatrix::doublediagmatrix()

{
// Keine Zeilen und Spalten
r = 0;
// Kein Speicher
a = NULL;
}

//
// Erzeuge eine diagonale Matrix
//

inline  doublediagmatrix::doublediagmatrix(int n)

{
#ifdef DEBUG
if(n <= 0){
	cerr << "ERROR from doublediagmatrix: Invalid dimensions\n";
	myexit(1);
	}
#endif
// Anzahl Zeilen und Spalten
r = n;
// Noetigen Speicher anfordern
a = new double[r];
}

//
// Der Kopierkonstruktor
//

inline  doublediagmatrix::doublediagmatrix(const doublediagmatrix &z)

{
// Abmessungen kopieren
r = z.r;
// Speicher anfordern
a = new double[r];
// Und alles kopieren
memcpy(a, z.a, r * sizeof(double));
}

//
// Die Zuweisung
//

inline  doublediagmatrix::doublediagmatrix(const doubletempdiagmatrix &z)

{
// Abmessungen kopieren
r = z.r;
// Einfach Zeiger kopieren
a = z.a;
}

//
// Der Destruktor
//

inline  doublediagmatrix::~doublediagmatrix()

{
// Gibt den Speicher frei (Test auf NULL nicht noetig?)
delete[] a;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline  double &doublediagmatrix::operator()(int n) const

{
#ifdef DEBUG
if(n < 0 || n >= r){
	cerr << "ERROR from doublediagmatrix: Subscript out of range\n";
	myexit(1);
	}
#endif
// Gib Referenz auf Element zurueck
return a[n];
}

//
// Die Zuweisung
//

inline  doublediagmatrix &doublediagmatrix::operator=(const doubletempdiagmatrix &z)

{
// Gibt den Speicher frei (Test auf NULL nicht noetig?)
delete[] a;
// Abmessungen kopieren
r = z.r;
// Einfach Zeiger kopieren
a = z.a;
return *this;
}

//
// Setze die Groesse einer diagonalen Matrix
//

inline  doublediagmatrix &doublediagmatrix::setsize(int n)

{
#ifdef DEBUG
if(n < 0){
	cerr << "ERROR from doublediagmatrix: Invalid dimensions\n";
	myexit(1);
	}
#endif
if(r != n){
	// Anzahl Zeilen und Spalten
	r = n;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	if(n != 0){
		// Noetigen Speicher anfordern
		a = new double[r];
		}
	else{
		// Keinen Speicher anfordern
		a = NULL;
		}
	}
return *this;
}

//
// Test, ob die Matrix benutzt worden ist
//

inline int used(const doublediagmatrix &z)

{
return z.r != 0;
}

//
// Erzeuge eine Submatrix
//

inline	doublesubmatrix::doublesubmatrix(const doublematrix &z, int m1, int m2, int n1, int n2) : ref(z)

{
#ifdef DEBUG
if(m1 < 0 || m2 < 0 || n1 <= 0 || n2 <= 0){
	cerr << "ERROR from doublesubmatrix: Invalid subdimensions\n";
	myexit(1);
	}
#endif
// Der Startpunkt
r0 = m1;
c0 = m2;
// Die Groesse
r = n1;
c = n2;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline	double &doublesubmatrix::operator()(int n1, int n2) const

{
#ifdef DEBUG
if(n1 < 0 || n1 >= r || n2 < 0 || n2 >= c){
	cerr << "ERROR from doublesubmatrix: Subscript out of range\n";
	myexit(1);
	}
#endif
// Gib Referenz auf Element zurueck
return ref(n1 + r0, n2 + c0);
}

//
// Erzeuge eine diagonale Submatrix
//

inline	doublesubdiagmatrix::doublesubdiagmatrix(const doublediagmatrix &z, int m, int n) : ref(z)

{
#ifdef DEBUG
if(m < 0 || n <= 0){
	cerr << "ERROR from doublesubdiagmatrix: Invalid subdimensions\n";
	myexit(1);
	}
#endif
// Der Startpunkt
r0 = m;
// Die Groesse
r = n;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline	double &doublesubdiagmatrix::operator()(int n) const

{
#ifdef DEBUG
if(n < 0 || n >= r){
	cerr << "ERROR from doublesubdiagmatrix: Subscript out of range\n";
	myexit(1);
	}
#endif
// Gib Referenz auf Element zurueck
return ref(n + r0);
}

//
// Erzeuge eine temporaere Matrix
//

inline  doubletempmatrix::doubletempmatrix(int n1, int n2)

{
// Anzahl Zeilen und Spalten
r = n1;
c = n2;
// Groesse berechnen
n = n1 * n2;
// Noetigen Speicher anfordern
a = new double[n];
}

//
// Erzeuge eine temporaere diagonale Matrix
//

inline  doubletempdiagmatrix::doubletempdiagmatrix(int n)

{
// Anzahl Zeilen und Spalten
r = n;
// Noetigen Speicher anfordern
a = new double[r];
}

#endif
