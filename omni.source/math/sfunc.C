// sfunc.C
//
// Mathematical functions
//
// JH - 10/01/2007

#include <cmath>

#include "platform.h"

#include "complex.h"
#include "intmath.h"

#include "clebgn.h"
#include "constants.h"

// Eine recht grosse Zahl

#define BIG		1.E5
#define BIG2		1.E10

// Eine recht kleine Zahl

#define SML		1.E-5
#define SML2		1.E-10

// Euler's constant
#define EULERGAMMA 0.577215664901532860606512

using namespace std;

// Unterprogramm zur Berechnung der sphaerischen Bessel Funktion
// z ist der Wert fuer den die Funktion berechnet werden soll
// xj enthaelt bei Rueckkehr die Funktionswerte
// l ist das maximale l fuer das die Funktionen berechnet werden

void bessel(complex z, complex *xj, int l){
  int		i, j;
  const int	iacc = 50;
  complex		xj0, xj1, xj2;
  complex		sum;
  complex		t1, t2;
  double		t;


  // Vorbesetzen
  for(i = 0; i <= l; i++){
    xj[i] = 0;
  }
  if(abs(z) < EMACH){
    xj[0] = 1;
    return;
  }
  // Berechne die sphaerische Bessel Funktion
  xj2 = 0;
  xj1 = 1;
  sum = 0;
  for(i = 2 * (l + (int)sqrt(double(iacc * l))) - 1; i >= 0; i--){
    xj0 =(2 * i + 3) * xj1 / z - xj2;
    xj2 = xj1;
    xj1 = xj0;
    if(abs(xj1) > BIG2){
      xj1 *= SML2;
      xj2 *= SML2;
      sum *= SML2 * SML2;
      if(i < l){
	for(j = i + 1; j <= l; j++){
	  xj[j] *= SML2;
	}
      }
    }
    sum += (2 * i + 1) * xj1 * xj1;
    if(i <= l){
      xj[i] = xj1;
    }
  }
  sum = sqrt(sum);
  t1 = sin(z) / z;
  t2 = xj[0] / sum;
  t = abs((t1 - t2) / t2);
  if(t > 1){
    sum = -sum;
  }
  for(i = 0; i <= l; i++){
    xj[i] /= sum;
  }
}

// Unterprogramm zur Berechnung der sphaerischen Bessel und Neumann Funktion
// z ist der Wert fuer den die Funktion berechnet werden soll
// xj und xn enthalten bei Rueckkehr die Funktionswerte
// l ist das maximale l fuer das die Funktionen berechnet werden

void neumann(complex z, complex *xj, complex *xn, int l){
  int		i;
  complex		xj0, xj1, xj2;

  // Berechne zunaechst die Bessel-Funktion
  bessel(z, xj, l);
  // Vorbesetzen
  for(i = 0; i <= l; i++){
    xn[i] = 0;
  }
  if(abs(z) < EMACH){
    return;
  }
  // Berechne nun die sphaerische Neumann Funktion
  xj2 = xj[1];
  xj1 = xj[0];
  for(i = 0; i <= l; i++){
    xj0 = (1 - 2 * i) * xj1 / z - xj2;
    xj2 = xj1;
    xj1 = xj0;
    if(i & 1){
      xn[i] = xj1;
    }
    else{
      xn[i] = -xj1;
    }
  }
}

// Unterprogramm zur Berechnung der sphaerischen Hankel Funktion 1 und 2 Art
// z ist der Wert fuer den die Funktion berechnet werden soll
// h1 und h2 enthalten bei Rueckkehr die Funktionswerte
// l ist das maximale l fuer das die Funktionen berechnet werden

void hankel(complex z, complex *h1, complex *h2, int l){
  int		i;
  complex		xh1, xh2;

  // Berechne sphaerische Bessel und Neumann Funktionen
  neumann(z, h1, h2, l);
  // Und daraus berechne Hankel Funktion 1 und 2 Art
  for(i = 0; i <= l; i++){
    xh1 = h1[i] + cmplx(0, 1) * h2[i];
    xh2 = h1[i] - cmplx(0, 1) * h2[i];
    h1[i] = xh1;
    h2[i] = xh2;
  }
}

// Unterprogramm zur Berechnung der modifizierten sphaerischen Bessel Funktion
// z ist der Wert fuer den die Funktion berechnet werden soll
// xj enthaelt bei Rueckkehr die Funktionswerte
// l ist das maximale l fuer das die Funktionen berechnet werden

void mbessel(complex z, complex *xj, int l){
  int		i, j;
  const int	iacc = 50;
  complex		xj0, xj1, xj2;
  complex		s1, s2;

  // Vorbesetzen
  for(i = 0; i <= l; i++){
    xj[i] = 0;
  }
  if(abs(z) < EMACH){
    xj[0] = 1;
    return;
  }
  // Berechne die modifizierte sphaerische Bessel Funktion
  xj2 = 0;
  xj1 = 1;
  for(i = 2 * (l + (int)sqrt(double(iacc * l))) - 1; i >= 0; i--){
    xj0 =(2 * i + 3) * xj1 / z + xj2;
    xj2 = xj1;
    xj1 = xj0;
    if(abs(xj1) > BIG2){
      xj1 *= SML2;
      xj2 *= SML2;
      if(i < l){
	for(j = i + 1; j <= l; j++){
	  xj[j] *= SML2;
	}
      }
    }
    if(i <= l){
      xj[i] = xj1;
    }
  }
  s1 = sinh(z) / z;
  s2 = s1 / xj[0];
  for(i = 0; i <= l; i++){
    xj[i] *= s2;
  }
}

// Unterprogramm zur Berechnung der modifizierten sphaerischen Bessel und Neumann Funktion
// z ist der Wert fuer den die Funktion berechnet werden soll
// xj und xn enthalten bei Rueckkehr die Funktionswerte
// l ist das maximale l fuer das die Funktionen berechnet werden

void mneumann(complex z, complex *xj, complex *xn, int l){
  int		i;
  complex		xj0, xj1, xj2;

  // Berechne zunaechst die modifizierte Bessel-Funktion
  mbessel(z, xj, l);
  // Vorbesetzen
  for(i = 0; i <= l; i++){
    xn[i] = 0;
  }
  if(abs(z) < EMACH){
    return;
  }
  // Berechne nun die modifizierte sphaerische Neumann Funktion
  xj2 = xj[1];
  xj1 = xj[0];
  for(i = 0; i <= l; i++){
    xj0 = (1 - 2 * i) * xj1 / z + xj2;
    xj2 = xj1;
    xj1 = xj0;
    xn[i] = xj1;
  }
}

// Unterprogramm zur Berechnung der modifizierten sphaerischen Hankel Funktion 1 und 2 Art
// z ist der Wert fuer den die Funktion berechnet werden soll
// h1 und h2 enthalten bei Rueckkehr die Funktionswerte
// l ist das maximale l fuer das die Funktionen berechnet werden

void mhankel(complex z, complex *h1, complex *h2, int l){
  int		i;
  complex		xh1, xh2;
  complex		pf;

  // Berechne modifizierte sphaerische Bessel und Neumann Funktionen
  mneumann(z, h1, h2, l);
  // Und daraus berechne modifizierte Hankel Funktion 1 und 2 Art
  pf = 1;
  for(i = 0; i <= l; i++){
    xh1 = pf * (h1[i] - h2[i]);
    xh2 = pf * (h1[i] + h2[i]);
    h1[i] = xh1;
    h2[i] = xh2;
    pf *= cmplx(0, 1);
  }
}

// Programm zur Berechnung der komplexen Errorfunktion
// z enthaelt den Wert fuer den die komplexe Errorfunktion berechnet wird
// Rueckgabewert ist das Ergebnis der komplexen Errorfunktion

complex cerf(complex z){
  double		eps;
  double		absz;
  int		n, nn;
  complex		zs;
  complex		res, sum;
  complex		t1, t2;
  complex		h1, h2, h3;
  complex		u1, u2, u3;
  int		q, fn, fd;
  int		flag;

  eps = 5 * EMACH;
  absz = abs(z);
  if(absz < EMACH){
    return cmplx(1, 0);
  }
  // bestimme Quadrant
  if(real(z) >= 0){
    if(imag(z) >= 0){
      nn = 0;
    }
    else{
      nn = 3;
      z = conj(z);
    }
  }
  else{
    if(imag(z) >= 0){
      nn = 1;
      z = -conj(z);
    }
    else{
      nn = 2;
      z = -z;
    }
  }
  zs = z * z;
  if(absz > 10){
    // asymptotische Formel
    res = 0.5124242 / (zs - 0.2752551) + 0.05176536 / (zs - 2.724745);
    res *= cmplx(0, 1) * z;
  }
  else{
    // Noch kein Ergebnis
    flag = 0;
    if(imag(z) >= 1 || absz >= 4){
      t2 = BIG;
      q = 1;
      h1 = 1;
      h2 = 2 * z;
      u1 = 0;
      u2 = 2 * sqrt(M_PI);
      do{
	t1 = t2;
	for(n = 0; n < 5; n++){
	  h3 = h2 * z - q * h1;
	  h1 = h2;
	  h2 = 2 * h3;
	  u3 = u2 * z - q * u1;
	  u1 = u2;
	  u2 = 2 * u3;
	  q += 1;
	}
	t2 = u3 / h3;
      }
      while(abs((t2 - t1) / t1) >= eps && q <= 60);
      if(q <= 60){
	// Es gibt ein Ergebnis
	flag = 1;
	res = cmplx(0, 1 / M_PI) * t2;
      }
    }
    // Wenn z zu klein oder die letze Rechnung nicht konvergierte
    if(!flag){
      q = 1;
      fn = -1;
      fd = 1;
      t1 = z;
      sum = z;
      do{
	for(n = 0; n < 5; n++){
	  fn += 2;
	  fd += 2;
	  t1 *= fn * zs / (q * fd);
	  sum += t1;
	  q += 1;
	}
      }
      while(abs(t1) >= eps && q < 100);
      sum *= cmplx(0, 2 / sqrt(M_PI));
      res = exp(-zs) * (1 + sum);
    }
  }
  switch(nn){
  case 0:
    break;
  case 1:
    res = conj(res);
    break;
  case 2:
    res = 2 * exp(-zs) - res;
    break;
  case 3:
    res = conj(2 * exp(-zs) - res);
    break;
  }
  return res;
}

// Programm zur Berechnung des Integrales ueber drei Kugelflaechenfunktionen.
// (l1,m1) (l2,m2) und (l3,m3) enthalten die Indizes der Kugelflaechenfunktionen
// Rueckgabewert ist das Ergebnis der Integration

double blm(int l1, int m1, int l2, int m2, int l3, int m3){
  double		b;
  double		a, aa;
  int		l, m;
  int		s;
  int		i1, i2, i3, i4, i5, i6;
  int		i, it;

  // JH Added parentheses around "l1 + l2 + l3" in migration to g++ 4.3
  if(m1 + m2 + m3 != 0 || ((l1 + l2 + l3) & 1) != 0 || l1 > l2 + l3 || l1 < iabs(l2 - l3)){
    return 0;
  }
  m1 = iabs(m1);
  m2 = iabs(m2);
  m3 = iabs(m3);
  if(m1 > l1 || m2 > l2 || m3 > l3){
    return 0;
  }
  if(m1 < m2){
    m = m1;
    l = l1;
    m1 = m2;
    l1 = l2;
    m2 = m;
    l2 = l;
  }
  if(m1 < m3){
    m = m1;
    l = l1;
    m1 = m3;
    l1 = l3;
    m3 = m;
    l3 = l;
  }
  if(l2 < l3){
    m = m2;
    l = l2;
    m2 = m3;
    l2 = l3;
    m3 = m;
    l3 = l;
  }
  s = (l1 + l2 + l3) / 2;
  b = ((s - l2 - m3 + m1) & 1 ? -1 : 1) * sqrt(((2 * l1 + 1) * (2 * l2 + 1) * (2 * l3 + 1)) / M_PI) / (2 * (2 * s + 1))
    * sqrt((fac[l1 + m1] * fac[l2 + m2] * fac[l3 + m3]) / (fac[l1 - m1] * fac[l2 - m2] * fac[l3 - m3])) / (fac[s - l1] * fac[s - l2])
    * fac[l2 + l3 - m1] / fac[l2 - l3 + m1];
  if(l3 != 0){
    for(l = 1; l <= l3; l++){
      b /= 2 * (2 * (s - l) + 1);
    }
    a = 1;
    aa = 1;
    i1 = l1 + m1;
    i2 = l2 + l3 - m1;
    i3 = 0;
    i4 = l1 - m1;
    i5 = l2 - l3 + m1;
    i6 = l3 - m3;
    it = min(i4, i6);
    for(i = 0; i < it; i++){
      i1 += 1;
      i3 += 1;
      i5 += 1;
      aa *= -i1 * i4 * i6;
      aa /= i2 * i3 * i5;
      a += aa;
      i2 -= 1;
      i4 -= 1;
      i6 -= 1;
    }
    b *= a;
  }
  return b;
}

// Programm zur Berechnung der Kugelflaechenfunktionen
// ylm enthaelt bei Rueckkehr alle Werte fuer 0<=l<=lmax
// ct enthaelt cos(theta)
// st enthaelt sin(theta)
// cf enthaelt exp(i*phi)
// lmx enthaelt lmax

void sphrm(complex *ylm, complex ct, complex st, complex cf, int lmx){
  double		*f1;
  double		*f2;
  double		*f3;
  double		sg;
  complex		sf, sa;
  int		l, m, lm;
  int		lm1, lm2;

  // temporaerer Speicher
  f1 = new double[lmx + 1];
  f2 = new double[lmx + 1];
  f3 = new double[2 * (lmx + 1) * (lmx + 1)];
  // Berechne Vorfaktoren
  lm = 0;
  sg = 1;
  for(l = 0; l <= lmx; l++){
    f1[l] = sg * sqrt((2 * l + 1) * fac[2 * l] / (4 * M_PI)) / fac[l];
    f2[l] = sqrt(double(2 * l));
    for(m = -l; m <= l; m++){
      f3[lm] = sqrt(((l + m + 1) * ( l - m + 1)) / (double)((2 * l + 3) * (2 * l + 1)));
      lm += 1;
    }
    sg *= -1. / 2.;
  }
  // Y00 ist trivial
  ylm[0] = f1[0];
  // Fange nun bei l=1 an
  lm = 1;
  sg = -1;
  sf = cf;
  sa = 1;
  for(l = 1; l <= lmx; l++){
    // Berechne Yl(-l)
    ylm[lm] = sg * f1[l] * sa * st / sf;
    lm += 1;
    // Berechne Yl(-l+1)
    ylm[lm] = sg * f2[l] * f1[l] * sa * ct * cf / sf;
    lm += 1;
    if(l >= 2){
      // Berechne nun Yl(-l+2)..Yl(l-2)
      lm2 = lm - 2 * l;
      lm1 = lm2 - 2 * l + 2;
      for(m = -l + 2; m <= l - 2; m++){
	// Berechne Ylm aus Y(l-1)m und Y(l-2)m
	ylm[lm] = (ct * ylm[lm2] - f3[lm1] * ylm[lm1]) / f3[lm2];
	lm += 1;
	lm1 += 1;
	lm2 += 1;
      }
      // Berechne Yl(l-1)
      ylm[lm] = -f2[l] * f1[l] * sa * ct * sf / cf;
      lm += 1;
    }
    // Berechne Yll
    ylm[lm] = f1[l] * sa * st * sf;
    lm += 1;
    // Faktoren weitersetzen
    sa *= st;
    sf *= cf;
    sg = -sg;
  }
  // temporaeren Speicher wieder freigeben
  delete[] f1;
  delete[] f2;
  delete[] f3;
}


// Gamma function Gamma(1+x) (using a polynomial expansion; see Abramowitz/Steegun, 6.1.36)
double Gamma(double x){

  double g;

  // Check the range of the argument
  g = 0;
  if((x >= 0) && (x <= 1)){
    g = 1 +
      x * (-0.577191652 +
	   x * (0.988205891 +
		x * (-0.897056937 +
		     x * (0.918206857 +
			  x * (-0.756704078 +
			       x * (0.482199394 +
				    x * (-0.193527818 +
					 x * 0.035868343)))))));
  }
  else{
    cout << "Error from Gamma(): Argument range. " << x << endl;
    myexit(1);
  }

  // Return Gamma(1 + x)
  return g;
}

// Derivative of the Gamma function Gamma(1+x) (using a polynomial expansion; see Abramowitz/Steegun, 6.1.36)
double Gammap(double x){

  double g;

  // Check the range of the argument
  g = 0;
  if((x >= 0) && (x <= 1)){
    g = -0.577191652 +
      x * (0.988205891 * 2 +
	   x * (-0.897056937 * 3 +
		x * (0.918206857 * 4 +
		     x * (-0.756704078 * 5 +
			  x * (0.482199394 * 6 +
			       x * (-0.193527818 * 7 +
				    x * 0.035868343 * 8))))));
  }
  else{
    cout << "Error from Gammap(): Argument range. " << x << endl;
    myexit(1);
  }

  // Return Gamma'(1 + x)
  return g;
}

// Second derivative of the Gamma function Gamma(1+x) (using a polynomial expansion; see Abramowitz/Steegun, 6.1.36)
double Gammapp(double x){

  double g;

  // Check the range of the argument
  g = 0;
  if((x >= 0) && (x <= 1)){
    g = 0.988205891 * 2 +
	   x * (-0.897056937 * 3 * 2 +
		x * (0.918206857 * 4 * 3 +
		     x * (-0.756704078 * 5 * 4 +
			  x * (0.482199394 * 6 * 5 +
			       x * (-0.193527818 * 7 * 6 +
				    x * 0.035868343 * 8 * 7)))));
  }
  else{
    cout << "Error from Gammapp(): Argument range. " << x << endl;
    myexit(1);
  }

  // Return Gamma''(1 + x)
  return g;
}


// Digamma function Psi(1+x) (using a polynomial expansion for the Gamma function; see Abramowitz/Steegun, 6.1.36 & 6.3)
double DiGamma(double x){

  return Gammap(x) / Gamma(x);
}

// Derivative of the Digamma function Psi(1+x) (using a polynomial expansion for the Gamma function; see Abramowitz/Steegun, 6.1.36 & 6.3)
double DiGammap(double x){

  return Gammapp(x) / Gamma(x) - DiGamma(x) * DiGamma(x);
}


// Conversion from degree to radians
double deg2rad(double x){

  return x * M_PI / 180;

}

// Conversion from radians to degree
double rad2deg(double x){

  return x * 180 / M_PI;

}





// Langevin function; see Wikipedia
// L(x) = coth(x) - 1 / x
double Langevin(double x){

  double l;

  if(fabs(x) > 0.001){
    l = (exp(x) + exp(-x)) / (exp(x) - exp(-x)) - 1.0 / x;
  }
  else{
    l = x / 3;
    // l = x / 3 - pow(x, 3) / 45 + 2 * pow(x, 5) / 945 - pow(x, 7) / 4725;
  }

  return l;
}



// Binomial coefficient; see Wikipedia
double binom(int n, int k){

   double result;

   int i;

   if(k == 0){
     result = 1;
   }
   else if(2 * k > n){
     result = binom(n, n - k);
   }
   else{
     result = n - k + 1;
     for(i = 2; i <= k; i++){
       result *= n - k + i;
       result /= i;
     }
   }

   return result;
}
