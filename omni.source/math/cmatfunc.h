// cmatfunc.h
//
// enthaelt die Definition der komplexen Matrix-Funktionenklassen
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _CMATFUNC_
#define _CMATFUNC_

#include <iostream>
#include <cstdlib>
#include <cstring>

#include "platform.h"

#include "complex.h"
#include "dmatrix.h"
#include "cmatrix.h"

#ifndef NULL
#define NULL	0
#endif

//
// Diese Klassen werden hier definiert
//

class complexmatrixfunction;

class complextempmatrixfunction;

class complexdiagmatrixfunction;

class complextempdiagmatrixfunction;

// JH - Added the following lines for compatibility with gcc4.1
// Thus, the compiler option -ffriend-injection is not needed
complextempmatrixfunction     integral(const complexmatrixfunction &, const complexmatrix &);
complexmatrix                 integral(const complexmatrixfunction &);
complextempdiagmatrixfunction integral(const complexdiagmatrixfunction &, const complexdiagmatrix &);
complexdiagmatrix             integral(const complexdiagmatrixfunction &);
complextempmatrixfunction     integral(const complexmatrixfunction &, const complexmatrix &);
complextempdiagmatrixfunction integral(const complexdiagmatrixfunction &, const complexdiagmatrix &);

//
// Diese Klassen werden hier definiert
//

class doublematrixfunction;
class doubletempmatrixfunction;

class doublediagmatrixfunction;
class doubletempdiagmatrixfunction;

//
// Definition der komplexen Matrix-Funktionenklasse
//

class complexmatrixfunction{
protected:
  int		n;		// Anzahl Stuetzstellen
  complexmatrix	*v;		// Speicher fuer die Stuetzstellen
public:
  // Die Konstruktoren
  complexmatrixfunction();
  complexmatrixfunction(int);
  complexmatrixfunction(int, int, int);
  // Der Kopierkonstruktor
  complexmatrixfunction(const complexmatrixfunction &);
  // Der Umkopierer
  complexmatrixfunction(const complextempmatrixfunction &);
  // Der Destruktor
  ~complexmatrixfunction();
  // Zugriff auf einzelne Stuetzstellen
  complexmatrix &operator()(int) const;
  complextempfunction f(int, int) const;
  // Die Zuweisung
  complexmatrixfunction &operator=(double);
  complexmatrixfunction &operator=(const complex &);
  complexmatrixfunction &operator=(const complexmatrix &);
  complexmatrixfunction &operator=(const complexmatrixfunction &);
  complexmatrixfunction &operator=(const complextempmatrixfunction &);
  // Setze die Zahl der Stuetzstellen
  complexmatrixfunction &setsize(int);
  complexmatrixfunction &setsize(int, int, int);
  int getsize() const;
  // Test, ob die Funktion benutzt ist
  friend int used(const complexmatrixfunction &);
  // Alle meine Freunde
  // Unaere Operatoren
  friend complextempmatrixfunction operator+(const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &);
  // Binaere Operatoren
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complex &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complex &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complex &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complex &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complex &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complex &);
  friend complextempmatrixfunction operator*(double, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(double, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(double, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, double);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, double);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, double);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublediagmatrix &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublediagmatrix &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublediagmatrix &);
  friend complextempmatrixfunction operator*(const complexdiagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublediagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublediagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublediagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator*(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublefunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublefunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublefunction &, const complexmatrixfunction &);
  // Funktionen
  friend complextempmatrixfunction inv(const complexmatrixfunction &);
  friend complextempmatrixfunction transp(const complexmatrixfunction &);
  friend complextempmatrixfunction conj(const complexmatrixfunction &);
  friend complextempmatrixfunction herm(const complexmatrixfunction &);
  friend complextempmatrixfunction integral(const complexmatrixfunction &, const complexmatrix &);
  friend complexmatrix integral(const complexmatrixfunction &);
  // Ein- und Ausgabe
  friend std::ostream &operator<<(std::ostream &, const complexmatrixfunction &);
  friend std::istream &operator>>(std::istream &, const complexmatrixfunction &);
};

//
// Definition der komplexen diagonalen Matrix-Funktionenklasse
//

class complexdiagmatrixfunction{
protected:
  int		n;		// Anzahl Stuetzstellen
  complexdiagmatrix
  *v;		// Speicher fuer die Stuetzstellen
public:
  // Die Konstruktoren
  complexdiagmatrixfunction();
  complexdiagmatrixfunction(int);
  complexdiagmatrixfunction(int, int);
  // Der Kopierkonstruktor
  complexdiagmatrixfunction(const complexdiagmatrixfunction &);
  // Der Umkopierer
  complexdiagmatrixfunction(const complextempdiagmatrixfunction &);
  // Der Destruktor
  ~complexdiagmatrixfunction();
  // Zugriff auf einzelne Stuetzstellen
  complexdiagmatrix &operator()(int) const;
  complextempfunction f(int) const;
  // Die Zuweisung
  complexdiagmatrixfunction &operator=(double);
  complexdiagmatrixfunction &operator=(const complex &);
  complexdiagmatrixfunction &operator=(const complexdiagmatrix &);
  complexdiagmatrixfunction &operator=(const complexdiagmatrixfunction &);
  complexdiagmatrixfunction &operator=(const complextempdiagmatrixfunction &);
  // Setze die Zahl der Stuetzstellen
  complexdiagmatrixfunction &setsize(int);
  complexdiagmatrixfunction &setsize(int, int);
  int getsize() const;
  // Test, ob die Funktion benutzt ist
  friend int used(const complexdiagmatrixfunction &);
  // Alle meine Freunde
  // Unaere Operatoren
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &);
  // Binaere Operatoren
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complex &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complex &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complex &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator*(double, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(double, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(double, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, double);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, double);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, double);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const doublediagmatrix &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const doublediagmatrix &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const doublediagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublefunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const doublefunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const doublefunction &, const complexdiagmatrixfunction &);
  // Funktionen
  friend complextempdiagmatrixfunction inv(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction transp(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction conj(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction herm(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction integral(const complexdiagmatrixfunction &, const complexdiagmatrix &);
  friend complexdiagmatrix integral(const complexdiagmatrixfunction &);
  // Ein- und Ausgabe
  friend std::ostream &operator<<(std::ostream &, const complexdiagmatrixfunction &);
  friend std::istream &operator>>(std::istream &, const complexdiagmatrixfunction &);
};

//
// Definition der temporaeren komplexen Matrix-Funktionenklasse
//

class complextempmatrixfunction{
protected:
  int		n;		// Anzahl Stuetzstellen
  complexmatrix	*v;		// Speicher fuer die Stuetzstellen
public:
  // Der Konstruktor
  complextempmatrixfunction(int);
  complextempmatrixfunction(int, int, int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class complexmatrixfunction;
  // Unaere Operatoren
  friend complextempmatrixfunction operator+(const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &);
  // Binaere Operatoren
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complex &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complex &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complex &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complex &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complex &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complex &);
  friend complextempmatrixfunction operator*(double, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(double, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(double, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, double);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, double);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, double);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complex &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complex &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complex &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complex &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complex &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complex &);
  friend complextempmatrixfunction operator*(const complexfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const doublefunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexfunction &, const doublematrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const complexfunction &);
  friend complextempmatrixfunction operator*(const doublematrix &, const complexfunction &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const doublefunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const doublediagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator+(const doublediagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator-(const doublediagmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrix &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrix &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrix &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublediagmatrix &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublediagmatrix &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexdiagmatrix &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublediagmatrix &);
  friend complextempmatrixfunction operator*(const complexdiagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublediagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublediagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublediagmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublematrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrix &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrix &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexdiagmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublediagmatrixfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexdiagmatrixfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator*(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const doublefunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublefunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublefunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexfunction &, const doublematrixfunction &);
  // Funktionen
  friend complextempmatrixfunction inv(const complexmatrixfunction &);
  friend complextempmatrixfunction transp(const complexmatrixfunction &);
  friend complextempmatrixfunction conj(const complexmatrixfunction &);
  friend complextempmatrixfunction herm(const complexmatrixfunction &);
  friend complextempmatrixfunction integral(const complexmatrixfunction &, const complexmatrix &);
};

//
// Definition der temporaeren komplexen diagonalen Matrix-Funktionenklasse
//

class complextempdiagmatrixfunction{
protected:
  int		n;		// Anzahl Stuetzstellen
  complexdiagmatrix
  *v;		// Speicher fuer die Stuetzstellen
public:
  // Der Konstruktor
  complextempdiagmatrixfunction(int);
  complextempdiagmatrixfunction(int, int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class complexdiagmatrixfunction;
  // Unaere Operatoren
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &);
  // Binaere Operatoren
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complex &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complex &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complex &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator*(double, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(double, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(double, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, double);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, double);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, double);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complex &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complex &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complex &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complex &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator*(const doublefunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const doublediagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrix &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrix &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrix &, const doublefunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const doublediagmatrix &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const doublediagmatrix &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const doublediagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrix &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrix &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrix &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrix &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const doublefunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const doublefunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const doublefunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexfunction &, const doublediagmatrixfunction &);
  // Funktionen
  friend complextempdiagmatrixfunction inv(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction transp(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction conj(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction herm(const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction integral(const complexdiagmatrixfunction &, const complexdiagmatrix &);
};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine komplexe Matrix-Funktion
//

inline	complexmatrixfunction::complexmatrixfunction()

{
  // Keine Stuetzstellen
  n = 0;
  // Kein Speicher
  v = NULL;
}

//
// Erzeuge eine komplexe Matrix-Funktion
//

inline	complexmatrixfunction::complexmatrixfunction(int nn)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from complexmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new complexmatrix[n];
}

inline	complexmatrixfunction::complexmatrixfunction(int nn, int rr, int cc)

{
#ifdef DEBUG
  if(nn <= 0 || rr <= 0 || cc <= 0){
    cerr << "ERROR from complexmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  //#ifdef GCC3
  //v = new complexmatrix[n](rr, cc);
  //#elif defined(GCC4) || defined(SGI)
  v = new complexmatrix[n];

  int i;
  for(i = 0; i < n; i++){
    v[i] = complexmatrix(rr, cc);
  }
  //#endif
}

//
// Der Kopierkonstruktor
//

inline	complexmatrixfunction::complexmatrixfunction(const complexmatrixfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Speicher anfordern
  v = new complexmatrix[n];
  // Und alles kopieren
  int i = n;
  complexmatrix *pm = v;
  complexmatrix *pz = z.v;
  while( --i >= 0){
    if(used(*pz)){
      *pm = *pz;
    }
    pz += 1;
    pm += 1;
  }
}

//
// Der Umkopierer
//

inline	complexmatrixfunction::complexmatrixfunction(const complextempmatrixfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
}

//
// Der Destruktor
//

inline	complexmatrixfunction::~complexmatrixfunction()

{
  // Gibt den Speicher frei
  delete[] v;
}

//
// Zugriff auf einzelne Stuetzstellen
//

inline	complexmatrix &complexmatrixfunction::operator()(int nn) const

{
#ifdef DEBUG
  if(nn < 0 || nn >= n){
    cerr << "ERROR from complexmatrix: Number out of range\n";
    myexit(1);
  }
#endif
  // Gibt Referenz auf Element zurueck
  return v[nn];
}

inline complextempfunction complexmatrixfunction::f(int rr, int cc) const

{
  complextempfunction f(n);

  complex *p = f.v;
  complexmatrix *pp = v;

  int i = n;
  while(--i >= 0){
    *p++ = (*pp++)(rr, cc);
  }
  return f;
}

//
// Die Zuweisung
//

inline	complexmatrixfunction &complexmatrixfunction::operator=(double z)

{
  complexmatrix *pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	complexmatrixfunction &complexmatrixfunction::operator=(const complex &z)

{
  complexmatrix *pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	complexmatrixfunction &complexmatrixfunction::operator=(const complexmatrix &z)

{
  complexmatrix *pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	complexmatrixfunction &complexmatrixfunction::operator=(const complexmatrixfunction &z)

{
  if(n != z.n){
    // Anzahl kopieren
    n = z.n;
    // Gibt den Speicher frei
    delete[] v;
    // Speicher anfordern
    v = new complexmatrix[n];
  }
  // Und alles kopieren
  int i = n;
  complexmatrix *pm = v;
  complexmatrix *pz = z.v;
  while( --i >= 0){
    if(used(*pz)){
      *pm = *pz;
    }
    pz += 1;
    pm += 1;
  }
  return *this;
}

inline	complexmatrixfunction &complexmatrixfunction::operator=(const complextempmatrixfunction &z)

{
  // Gibt den Speicher frei
  delete[] v;
  // Anzahl kopieren
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
  return *this;
}

//
// Setze Zahl der Stuetzstellen
//

inline	complexmatrixfunction &complexmatrixfunction::setsize(int nn)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from complexmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  if(n != nn){
    // Anzahl Stuetzstellen
    n = nn;
    // Gib den Speicher frei
    delete[] v;
    if(n != 0){
      // Noetigen Speicher anfordern
      v = new complexmatrix[n];
    }
    else{
      // Keinen Speicher anfordern
      v = NULL;
    }
  }
  return *this;
}

inline int complexmatrixfunction::getsize() const
{
  return n;
}

inline	complexmatrixfunction &complexmatrixfunction::setsize(int nn, int rr, int cc)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from complexmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Gib den Speicher frei
  delete[] v;
  if(n != 0){
    // Noetigen Speicher anfordern
    //#ifdef GCC3
    //v = new complexmatrix[n](rr, cc);
    //#elif defined(GCC4) || defined(SGI)
    v = new complexmatrix[n];

    int i;
    for(i = 0; i < n; i++){
      v[i] = complexmatrix(rr, cc);
    }
    //#endif    
  }
  else{
    // Keinen Speicher anfordern
    v = NULL;
  }
  return *this;
}

//
// Test, ob die Funktion benutzt worden ist
//

inline int used(const complexmatrixfunction &z)

{
  return z.n != 0;
}

//
// Erzeuge Platzhalter fuer eine komplexe diagonale Matrix-Funktion
//

inline	complexdiagmatrixfunction::complexdiagmatrixfunction()

{
  // Keine Stuetzstellen
  n = 0;
  // Kein Speicher
  v = NULL;
}

//
// Erzeuge eine komplexe diagonale Matrix-Funktion
//

inline	complexdiagmatrixfunction::complexdiagmatrixfunction(int nn)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from complexdiagmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new complexdiagmatrix[n];
}

inline	complexdiagmatrixfunction::complexdiagmatrixfunction(int nn, int rr)
     
{
#ifdef DEBUG
  if(nn <= 0 || rr <= 0){
    cerr << "ERROR from complexdiagmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  //#ifdef GCC3
  //v = new complexdiagmatrix[n](rr);
  //#elif defined(GCC4) || defined(SGI)
  v = new complexdiagmatrix[n];

  int i;
  for(i = 0; i < n; i++){
    v[i] = complexdiagmatrix(rr);
  }
  //#endif
}

//
// Der Kopierkonstruktor
//

inline	complexdiagmatrixfunction::complexdiagmatrixfunction(const complexdiagmatrixfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Speicher anfordern
  v = new complexdiagmatrix[n];
  // Und alles kopieren
  int i = n;
  complexdiagmatrix *pm = v;
  complexdiagmatrix *pz = z.v;
  while( --i >= 0){
    if(used(*pz)){
      *pm = *pz;
    }
    pz += 1;
    pm += 1;
  }
}

//
// Der Umkopierer
//

inline	complexdiagmatrixfunction::complexdiagmatrixfunction(const complextempdiagmatrixfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
}

//
// Der Destruktor
//

inline	complexdiagmatrixfunction::~complexdiagmatrixfunction()

{
  // Gibt den Speicher frei
  delete[] v;
}

//
// Zugriff auf einzelne Stuetzstellen
//

inline	complexdiagmatrix &complexdiagmatrixfunction::operator()(int nn) const

{
#ifdef DEBUG
  if(nn < 0 || nn >= n){
    cerr << "ERROR from complexdiagmatrix: Number out of range\n";
    myexit(1);
  }
#endif
  // Gibt Referenz auf Element zurueck
  return v[nn];
}

inline complextempfunction complexdiagmatrixfunction::f(int nn) const

{
  complextempfunction f(n);

  complex *p = f.v;
  complexdiagmatrix *pp = v;

  int i = n;
  while(--i >= 0){
    *p++ = (*pp++)(nn);
  }
  return f;
}

//
// Die Zuweisung
//

inline	complexdiagmatrixfunction &complexdiagmatrixfunction::operator=(double z)

{
  complexdiagmatrix *pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	complexdiagmatrixfunction &complexdiagmatrixfunction::operator=(const complex &z)

{
  complexdiagmatrix *pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	complexdiagmatrixfunction &complexdiagmatrixfunction::operator=(const complexdiagmatrix &z)

{
  complexdiagmatrix *pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	complexdiagmatrixfunction &complexdiagmatrixfunction::operator=(const complexdiagmatrixfunction &z)

{
  if(n != z.n){
    // Anzahl kopieren
    n = z.n;
    // Gibt den Speicher frei
    delete[] v;
    // Speicher anfordern
    v = new complexdiagmatrix[n];
  }
  // Und alles kopieren
  int i = n;
  complexdiagmatrix *pm = v;
  complexdiagmatrix *pz = z.v;
  while( --i >= 0){
    if(used(*pz)){
      *pm = *pz;
    }
    pz += 1;
    pm += 1;
  }
  return *this;
}

inline	complexdiagmatrixfunction &complexdiagmatrixfunction::operator=(const complextempdiagmatrixfunction &z)

{
  // Gibt den Speicher frei
  delete[] v;
  // Anzahl kopieren
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
  return *this;
}

//
// Setze Zahl der Stuetzstellen
//

inline	complexdiagmatrixfunction &complexdiagmatrixfunction::setsize(int nn)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from complexdiagmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  if(n != nn){
    // Anzahl Stuetzstellen
    n = nn;
    // Gib den Speicher frei
    delete[] v;
    if(n != 0){
      // Noetigen Speicher anfordern
      v = new complexdiagmatrix[n];
    }
    else{
      // Keinen Speicher anfordern
      v = NULL;
    }
  }
  return *this;
}

inline int complexdiagmatrixfunction::getsize()const
{
  return n;
}

inline	complexdiagmatrixfunction &complexdiagmatrixfunction::setsize(int nn, int rr)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from complexdiagmatrixfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Gib den Speicher frei
  delete[] v;
  if(n != 0){
    // Noetigen Speicher anfordern
    //#ifdef GCC3
    //v = new complexdiagmatrix[n](rr);
    //#elif defined(GCC4) || defined(SGI)
    v = new complexdiagmatrix[n];

    int i;
    for(i = 0; i < n; i++){
      v[i] = complexdiagmatrix(rr);
    }
    //#endif
  }
  else{
    // Keinen Speicher anfordern
    v = NULL;
  }
  return *this;
}

//
// Test, ob die Funktion benutzt worden ist
//

inline int used(const complexdiagmatrixfunction &z)

{
  return z.n != 0;
}

//
// Erzeuge eine temporaere komplexe Matrix-Funktion
//

inline	complextempmatrixfunction::complextempmatrixfunction(int nn)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new complexmatrix[n];
}

inline	complextempmatrixfunction::complextempmatrixfunction(int nn, int rr, int cc)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
#ifdef GGC3
  v = new complexmatrix[n](rr, cc);
#elif defined(GCC4) || defined(SGI)
  v = new complexmatrix[n];
  
  int i;
  for(i = 0; i < n; i++){
    v[i] = complexmatrix(rr, cc);
  }
#endif
}

//
// Erzeuge eine temporaere komplexe diagonale Matrix-Funktion
//

inline	complextempdiagmatrixfunction::complextempdiagmatrixfunction(int nn)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new complexdiagmatrix[n];
}

inline	complextempdiagmatrixfunction::complextempdiagmatrixfunction(int nn, int rr)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  //#ifdef GCC3
  //v = new complexdiagmatrix[n](rr);
  //#elif defined(GCC4) || defined(SGI)
  v = new complexdiagmatrix[n];

  int i;
  for(i = 0; i < n; i++){
    v[i] = complexdiagmatrix(rr);
  }
  //#endif
}

#endif
