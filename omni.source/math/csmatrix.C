// csmatrix.C
//
// Operations on complex super matrices (block matrices)
//

#include <iostream>
#include <cmath>

#include "platform.h"

#include "csmatrix.h"
#include "do_lu.h"

using namespace std;

//
// Der Kopierkonstruktor
//

complexsupermatrix::complexsupermatrix(const complexsupermatrix &z)

{
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Groesse kopieren
  n = z.n;
  // Abmessungen je Block kopieren
  rs = z.rs;
  cs = z.cs;
  // Speicher anfordern
  m = new complexmatrix[n];
  int i = n;
  complexmatrix *pm = m;
  complexmatrix *pz = z.m;
  while( --i >= 0){
    if(used(*pz)){
      *pm = *pz;
    }
    pz += 1;
    pm += 1;
  }
}

//
// Die Zuweisung
//

complexsupermatrix &complexsupermatrix::operator=(double z)

{
  complexmatrix *pm = m;
  int i = n;
  while(--i >= 0){
    // Matrix loeschen
    (*pm).setsize(0, 0);
    pm += 1;
  }
  if(fabs(z) >= 0.0){
    complexmatrix *ppm = m;
    int ii = r;
    while(--ii >= 0){
      (*ppm).setsize(rs, cs);
      *ppm = z;
      ppm += c + 1;
    }
  }
  return *this;
}

complexsupermatrix &complexsupermatrix::operator=(complex &z)

{
  complexmatrix *pm = m;
  int i = n;
  while(--i >= 0){
    // Matrix loeschen
    (*pm).setsize(0, 0);
    pm += 1;
  }
  if(abs(z) >= 0.0){
    complexmatrix *ppm = m;
    int ii = r;
    while(--ii >= 0){
      (*ppm).setsize(rs, cs);
      *ppm = z;
      ppm += c + 1;
    }
  }
  return *this;
}

complexsupermatrix &complexsupermatrix::operator=(complexmatrix &z)

{
  complexmatrix *pm = m;
  int i = n;
  while(--i >= 0){
    // Matrix loeschen
    (*pm).setsize(0, 0);
    pm += 1;
  }
  pm = m;
  i = r;
  while(--i >= 0){
    *pm = z;
    pm += c + 1;
  }
  return *this;
}

complexsupermatrix &complexsupermatrix::operator=(const complexsupermatrix &z)

{
  if(n != z.n){
    // Groesse kopieren
    n = z.n;
    // Gibt den Speicher frei (Test auf NULL nicht noetig?)
    delete[] m;
    // Speicher anfordern
    m = new complexmatrix[n];
  }
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Abmessungen por Block kopieren
  rs = z.rs;
  cs = z.cs;
  // Matrizen kopieren
  int i = n;
  complexmatrix *pm = m;
  complexmatrix *pz = z.m;
  while( --i >= 0){
    if(used(*pz)){
      *pm = *pz;
    }
    pz += 1;
    pm += 1;
  }
  return *this;
}

//
// Die Super-Matrix
//

complextempsupermatrix operator+(const complexsupermatrix &a)

{
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  return s;
}

//
// Die Super-Matrix-Negation
//

complextempsupermatrix operator-(const complexsupermatrix &a)

{
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = -(*pa);
    }
    pa += 1;
    ps += 1;
  }
  return s;
}

//
// Die Super-Matrix-Multiplikation
//

complextempsupermatrix operator*(const complexsupermatrix &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(a.c != b.r){
    cerr << "ERROR from complextempsupermatrix: Matrices do not match\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, b.c, a.rs, a.cs);
  // Und multiplizieren
  complexmatrix *pa = a.m;
  complexmatrix *pb = b.m;
  int j = a.c;
  while(--j >= 0){
    complexmatrix *ps = s.m;
    int i = s.r;
    while(--i >= 0){
      int k = s.c;
      while(--k >= 0){
	if(used(*pa) && used(*pb)){
	  if(used(*ps)){
	    *ps = *ps + *pa * *pb;
	  }
	  else{
	    *ps = *pa * *pb;
	  }
	}
	pb += 1;
	ps += 1;
      }
      pa += a.c;
      pb -= b.c;
    }
    pa -= a.n - 1;
    pb += b.c;
  }
  return s;
}

//
// Die Super-Matrix-Addition
//

complextempsupermatrix operator+(const complexsupermatrix &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(a.r != b.r || a.c != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrices do not match\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und addieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa) && used(*pb)){
      *ps = *pa + *pb;
    }
    else if(used(*pa)){
      *ps = *pa;
    }
    else if(used(*pb)){
      *ps = *pb;
    }
    pa += 1;
    pb += 1;
    ps += 1;
  }
  return s;
}

//
// Die Super-Matrix-Subtraktion
//

complextempsupermatrix operator-(const complexsupermatrix &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(a.r != b.r || a.c != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrices do not match\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und addieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa) && used(*pb)){
      *ps = *pa - *pb;
    }
    else if(used(*pa)){
      *ps = *pa;
    }
    else if(used(*pb)){
      *ps = -(*pb);
    }
    pa += 1;
    pb += 1;
    ps += 1;
  }
  return s;
}

//
// Die Super-Matrix-Komplex-Multiplikation
//

complextempsupermatrix operator*(const complexsupermatrix &a, const complexmatrix &b)

{
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und multiplizieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa * b;
    }
    pa += 1;
    ps += 1;
  }
  return s;
}

complextempsupermatrix operator*(const complexsupermatrix &a, const complexdiagmatrix &b)

{
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und multiplizieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa * b;
    }
    pa += 1;
    ps += 1;
  }
  return s;
}

complextempsupermatrix operator*(const complexsupermatrix &a, const complex &b)

{
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und multiplizieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa * b;
    }
    pa += 1;
    ps += 1;
  }
  return s;
}

complextempsupermatrix operator*(const complexsupermatrix &a, double b)

{
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und multiplizieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa * b;
    }
    pa += 1;
    ps += 1;
  }
  return s;
}

//
// Die Super-Matrix-Komplex-Addition
//

complextempsupermatrix operator+(const complexsupermatrix &a, const complexmatrix &b)

{
#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + b;
    }
    else{
      *ps = b;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator+(const complexsupermatrix &a, const complexdiagmatrix &b)

{
#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + b;
    }
    else{
      *ps = b;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator+(const complexsupermatrix &a, const complex &b)

{
#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + b;
    }
    else{
      (*ps).setsize(s.rs, s.cs);
      *ps = b;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator+(const complexsupermatrix &a, double b)

{
#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + b;
    }
    else{
      (*ps).setsize(s.rs, s.cs);
      *ps = b;
    }
    ps += s.c + 1;
  }
  return s;
}

//
// Die Super-Matrix-Komplex-Subtraktion
//

complextempsupermatrix operator-(const complexsupermatrix &a, const complexmatrix &b)

{
#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps - b;
    }
    else{
      *ps = -b;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator-(const complexsupermatrix &a, const complexdiagmatrix &b)

{
#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps - b;
    }
    else{
      *ps = -b;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator-(const complexsupermatrix &a, const complex &b)

{
#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps - b;
    }
    else{
      (*ps).setsize(s.rs, s.cs);
      *ps = -b;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator-(const complexsupermatrix &a, double b)

{
#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps - b;
    }
    else{
      (*ps).setsize(s.rs, s.cs);
      *ps = -b;
    }
    ps += s.c + 1;
  }
  return s;
}

//
// Die Komplex-Super-Matrix-Multiplikation
//

complextempsupermatrix operator*(const complexmatrix &a, const complexsupermatrix &b)

{
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Und multiplizieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = a * *pb;
    }
    pb += 1;
    ps += 1;
  }
  return s;
}

complextempsupermatrix operator*(const complexdiagmatrix &a, const complexsupermatrix &b)

{
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Und multiplizieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = a * *pb;
    }
    pb += 1;
    ps += 1;
  }
  return s;
}

complextempsupermatrix operator*(const complex &a, const complexsupermatrix &b)

{
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Und multiplizieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = a * *pb;
    }
    pb += 1;
    ps += 1;
  }
  return s;
}

complextempsupermatrix operator*(double a, const complexsupermatrix &b)

{
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Und multiplizieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = a * *pb;
    }
    pb += 1;
    ps += 1;
  }
  return s;
}

//
// Die Komplex-Super-Matrix-Addition
//

complextempsupermatrix operator+(const complexmatrix &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(b.r != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = *pb;
    }
    pb += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + a;
    }
    else{
      *ps = a;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator+(const complexdiagmatrix &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(b.r != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = *pb;
    }
    pb += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + a;
    }
    else{
      *ps = a;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator+(const complex &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(b.r != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = *pb;
    }
    pb += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + a;
    }
    else{
      (*ps).setsize(s.rs, s.cs);
      *ps = a;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator+(double a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(b.r != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = *pb;
    }
    pb += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + a;
    }
    else{
      (*ps).setsize(s.rs, s.cs);
      *ps = a;
    }
    ps += s.c + 1;
  }
  return s;
}

//
// Die Komplex-Super-Matrix-Subtraktion
//

complextempsupermatrix operator-(const complexmatrix &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(b.r != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = -(*pb);
    }
    pb += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + a;
    }
    else{
      *ps = a;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator-(const complexdiagmatrix &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(b.r != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = -(*pb);
    }
    pb += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + a;
    }
    else{
      *ps = a;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator-(const complex &a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(b.r != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = -(*pb);
    }
    pb += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + a;
    }
    else{
      (*ps).setsize(s.rs, s.cs);
      *ps = a;
    }
    ps += s.c + 1;
  }
  return s;
}

complextempsupermatrix operator-(double a, const complexsupermatrix &b)

{
#ifdef DEBUG
  if(b.r != b.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(b.r, b.c, b.rs, b.cs);
  // Erst kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pb = b.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pb)){
      *ps = -(*pb);
    }
    pb += 1;
    ps += 1;
  }
  // Und addieren
  ps = s.m;
  int i = s.r;
  while(--i >= 0){
    if(used(*ps)){
      *ps = *ps + a;
    }
    else{
      (*ps).setsize(s.rs, s.cs);
      *ps = a;
    }
    ps += s.c + 1;
  }
  return s;
}

//
// Die Super-Matrix-Inversion
//

complextempsupermatrix inv(const complexsupermatrix &a)

{
  int	i, j, k;

#ifdef DEBUG
  if(a.r != a.c){
    cerr << "ERROR from complextempsupermatrix: Matrix must be square\n";
    myexit(1);
  }
#endif
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und kopieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    if(used(*pa)){
      *ps = *pa;
    }
    pa += 1;
    ps += 1;
  }
  // Temporaerer Speicher
  n = s.r;
  complexmatrix *ff = new complexmatrix[n];
  complexmatrix *xx = new complexmatrix[n];
  int *ii = new int[n];
  // Zeilennummern vorbesetzen
  for(i = 0; i < n; i++){
    ii[i] = i;
  }
  // Und invertieren
  // Current line
  complexmatrix *pz = s.m;
  for(i = 0; i < n; i++){
    double max = 0;
    // groessten Wert suchen
    if(used(pz[i])){
      max = norm(pz[i]);
    }
    int m = i;
    // Max Line
    complexmatrix *pm = pz;
    // Next Line
    complexmatrix *pn = pz;
    for(j = i + 1; j < n; j++){
      pn += n;
      if(used(pn[i])){
	double cmp = norm(pn[i]);
	if(cmp > max){
	  max = cmp;
	  m = j;
	  pm = pn;
	}
      }
    }
    // Gegebenenfalls vertauschen
    if(m != i){
      for(k = 0; k < n; k++){
	if(used(pz[k])){
	  xx[k] = pz[k];
	}
	else{
	  xx[k].setsize(0,0);
	}
	if(used(pm[k])){
	  pz[k] = pm[k];
	}
	else{
	  pz[k].setsize(0,0);
	}
	if(used(xx[k])){
	  pm[k] = xx[k];
	}
	else{
	  pm[k].setsize(0,0);
	}
      }
      // Vertauschung merken
      int jj = ii[i];
      ii[i] = ii[m];
      ii[m] = jj;
    }
#ifdef USELU
    ff[i] = invlu(pz[i]);
#else
    ff[i] = inv(pz[i]);
#endif
    pz[i] = 1;
    // Durch alle Zeilen
    ps = s.m;
    j = n;
    while(--j >= 0){
      // Ausser der aktuellen Zeile
      if(ps != pz && used(ps[i])){
	// Faktor festlegen
	complexmatrix cf = -ps[i] * ff[i];
	ps[i].setsize(0, 0);
	// Zeilen subtrahieren
	int kk = n;
	while(--kk >= 0){
	  if(used(*ps)){
	    *ps = *ps + cf * *pz;
	  }
	  else{
	    *ps = cf * *pz;
	  }
	  ps += 1;
	  pz += 1;
	}
	pz -= n;
      }
      else{
	ps += n;
      }
    }
    // Naechste Zeile;
    pz += n;
  }
  // Und zuruecktauschen
  ps = s.m;
  for(i = 0; i < n; i++){
    for(j = 0; j < n; j++){
      if(used(ps[j])){
	xx[j] = ps[j];
      }
      else{
	xx[j].setsize(0, 0);
      }
    }
    for(j = 0; j < n; j++){
      if(used(xx[j])){
	ps[ii[j]] = ff[i] * xx[j];
      }
      else{
	ps[ii[j]].setsize(0, 0);
      }
    }
    ps += n;
  }
  // Temporaeren Speicher wieder freigeben
  delete[] ii;
  delete[] xx;
  delete[] ff;
  return s;
}

//
// Die transponierte Super-Matrix
//

complextempsupermatrix transp(const complexsupermatrix &a)

{
  complextempsupermatrix s(a.c, a.r, a.rs, a.cs);
  // Und transponieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int i = s.r;
  while(--i >= 0){
    int j = s.c;
    while(--j >= 0){
      if(used(*pa)){
	*ps = transp(*pa);
      }
      ps += 1;
      pa += a.c;
    }
    pa -= a.n - 1;
  }
  return s;
}

//
// Die komplex konjugierte Super-Matrix
//

complextempsupermatrix conj(const complexsupermatrix &a)

{
  complextempsupermatrix s(a.r, a.c, a.rs, a.cs);
  // Und konjugieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int n = s.n;
  while(--n >= 0){
    *ps = conj(*pa);
    pa += 1;
    ps += 1;
  }
  return s;
}

//
// Die hermitesch konjugierte Super-Matrix
//

complextempsupermatrix herm(const complexsupermatrix &a)

{
  complextempsupermatrix s(a.c, a.r, a.rs, a.cs);
  // Und hermitesch konjugieren
  complexmatrix *ps = s.m;
  complexmatrix *pa = a.m;
  int i = s.r;
  while(--i >= 0){
    int j = s.c;
    while(--j >= 0){
      if(used(*pa)){
	*ps = herm(*pa);
      }
      ps += 1;
      pa += a.c;
    }
    pa -= a.n - 1;
  }
  return s;
}


//
// Trace
//
complex trace(const complexsupermatrix &a){

  int     i, is;

  complex d;

  // Checking dimensions
  if(a.r != a.c){
    myerror("ERROR from complexsupermatrix::trace(): Non-square matrix.", 1);
  }
  if(a.rs != a.cs){
    myerror("ERROR from complexsupermatrix::trace(): Non-square submatrix.", 1);
  }

  // And add the diagonals
  d = 0;
  for(i = 0; i < a.r; i++){
    for(is = 0; is < a.rs; is++){
      d += a(i, i)(is, is);
    }
  }

  return d;
}

//
// Norm
//
double norm(const complexsupermatrix &a){

  return sqrt(real(trace(a * herm(a))));
}

ostream &operator<<(ostream &os, const complexsupermatrix &a)
{
  int ncols, nraws;

  int w = os.width(0);
  int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
  long f = os.flags(std::_Ios_Fmtflags(0));
#else
  long f = os.flags(0);
#endif


  complexmatrix *pa = a.m;
  int i = a.r;
  while(--i >= 0){
    int j = a.c;
    while(--j >= 0){
      os.width(w);
      os.precision(p);
#if defined(GCC3) || defined(GCC4)
      os.flags(std::_Ios_Fmtflags(f));
#else
      os.flags(f);
#endif
      pa->getsize(nraws, ncols);
      os << "submatrix " << nraws << " " << ncols << endl;
      os << *pa++;
      if(j != 0){
	os << " ";
      }
    }
    os << "\n";
  }
  return os;
}
