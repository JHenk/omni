// vector2.h
//
// deklariert relle zweidimensionale Vektoren, sowie mathematische Operationen
// zwischen Vektoren und reellen Zahlen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _VECTOR2_
#define _VECTOR2_

#include <iostream>
using namespace std;

#include <cmath>

#include "platform.h"

class vector2{
 public:
  double		x;		// die x-Komponente des Vektors
  double		y;		// die y-Komponente des Vektors
  // Die Konstruktoren
  vector2();
  vector2(double, double);
  // Der Destruktor
  ~vector2();
  // Die Zuweisung
  vector2 &operator=(double);
  vector2 &operator=(const vector2 &);
  // Unaere Operatoren
  inline friend vector2 operator+(const vector2 &);
  inline friend vector2 operator-(const vector2 &);
  // Binaere Operatoren
  inline friend double operator*(const vector2 &, const vector2 &);
  inline friend vector2 operator+(const vector2 &, const vector2 &);
  inline friend vector2 operator-(const vector2 &, const vector2 &);
  inline friend vector2 operator*(const vector2 &, double);
  inline friend vector2 operator/(const vector2 &, double);
  inline friend vector2 operator*(double, const vector2 &);
  // Funktionen
  inline friend double norm(const vector2 &);
  inline friend vector2 perp(const vector2 &);
  inline friend double area(const vector2 &, const vector2 &);
  // Ein- und Ausgabe
  inline friend std::ostream &operator<<(std::ostream &, const vector2 &);
  inline friend std::istream &operator>>(std::istream &, vector2 &);
};

// Die Konstruktoren

inline vector2::vector2()

{
}

inline vector2::vector2(double xx, double yy)

{
  x = xx;
  y = yy;
}

// Der Destruktor

inline vector2::~vector2()

{
}

// Die Zuweisung

inline vector2 &vector2::operator=(double zero)

{
#ifdef DEBUG
  if(zero != 0){
    cerr << "ERROR from vector2: Cannot assign a number to a vector.\n";
    myexit(1);
  }
#endif
  x = zero;
  y = zero;
  return *this;
}

  inline vector2 &vector2::operator=(const vector2 &a)

  {
    x = a.x;
    y = a.y;
    return *this;
  }

  // Unaere Operatoren

    inline vector2 operator+(const vector2 &a)

    {
      return a;
    }

inline vector2 operator-(const vector2 &a)

{
  return vector2(-a.x, -a.y);
}

// Binaere Operatoren

inline double operator*(const vector2 &a, const vector2 &b)

{
  return a.x * b.x + a.y * b.y;
}

inline vector2 operator+(const vector2 &a, const vector2 &b)

{
  return vector2(a.x + b.x, a.y + b.y);
}

inline vector2 operator-(const vector2 &a, const vector2 &b)

{
  return vector2(a.x - b.x, a.y - b.y);
}

inline vector2 operator*(const vector2 &a, double b)

{
  return vector2(a.x * b, a.y * b);
}

inline vector2 operator/(const vector2 &a, double b)

{
  return vector2(a.x / b, a.y / b);
}

inline vector2 operator*(double a, const vector2 &b)

{
  return vector2(a * b.x, a * b.y);
}

// Funktionen

// berechne die Norm eines Vektors

inline double norm(const vector2 &a)

{
  return sqrt(a.x * a.x + a.y * a.y);
}

// berechne einen um 90 Grad gedrehten Vektor

inline vector2 perp(const vector2 &a)

{
  return vector2(-a.y, a.x);
}

// berechne die von zwei Vektoren aufgespannte Parallelogrammflaeche

inline double area(const vector2 &a, const vector2 &b)

{
  return a.x * b.y - a.y * b.x;
}

// Ein- und Ausgabe

inline std::ostream &operator<<(std::ostream &os, const vector2 &a)

{
  int w = os.width(0);
  int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
  long f = os.flags(std::_Ios_Fmtflags(0));
#else
  long f = os.flags(0);
#endif
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << a.x;
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << a.y;
  return os;
}

inline std::istream &operator>>(std::istream &is, vector2 &a)

{
  is >> a.x;
  is >> a.y;
  return is;
}

#endif
