// matrix.C
//
// Operations on double matrices
//

#include <iostream>
#include <cmath>

#include "platform.h"
#include "dmatrix.h"

using namespace std;

//
// Der Umkopierer
//

doublematrix::doublematrix(const doublesubmatrix &z)

{
#ifdef DEBUG
if(z.r0 + z.r > z.ref.r || z.c0 + z.c > z.ref.c){
	cerr << "ERROR from doublematrix: Invalid submatrix\n";
	myexit(1);
	}
#endif
// Abmessungen kopieren
r = z.r;
c = z.c;
// Groesse berechnen
n = r * c;
// Speicher anfordern
a = new double[n];
double *pa = a;
double *pz = z.ref.a + z.r0 * z.ref.c + z.c0;
int i = r;
while(--i >= 0){
	memcpy(pa, pz, c * sizeof(double));
	pa += c;
	pz += z.ref.c;
	}
}

//
// Die Zuweisung
//

doublematrix &doublematrix::operator=(double z)

{
#ifdef DEBUG
if(z != 0 && r != c){
	cerr << "ERROR from doublematrix: Matrix must be square";
	myexit(1);
	}
#endif
double *pa = a;
int i = n;
while(--i >= 0){
	*pa++ = 0;
	}
 if(fabs(z) > 0.0){
	double *ppa = a;
	int ii = r;
	while(--ii >= 0){
		*ppa = z;
		ppa += c + 1;
		}
	}
return *this;
}

doublematrix &doublematrix::operator=(const doublematrix &z)

{
if(n != z.n){
	// Groesse kopieren
	n = z.n;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	// Speicher anfordern
	a = new double[n];
	}
// Abmessungen kopieren
r = z.r;
c = z.c;
// Und alles kopieren
memcpy(a, z.a, n * sizeof(double));
return *this;
}

doublematrix &doublematrix::operator=(const doublesubmatrix &z)

{
#ifdef DEBUG
if(z.r0 + z.r > z.ref.r || z.c0 + z.c > z.ref.c){
	cerr << "ERROR from doublematrix: Invalid submatrix\n";
	myexit(1);
	}
#endif
if(n != z.r * z.c){
	// Groesse berechnen
	n = z.r * z.c;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	// Speicher anfordern
	a = new double[n];
	}
// Abmessungen kopieren
r = z.r;
c = z.c;
double *pa = a;
double *pz = z.ref.a + z.r0 * z.ref.c + z.c0;
int i = r;
while(--i >= 0){
	memcpy(pa, pz, c * sizeof(double));
	pa += c;
	pz += z.ref.c;
	}
return *this;
}

doublematrix &doublematrix::operator=(const doublediagmatrix &z)

{
if(n != z.r * z.r){
	// Groesse berechnen
	n = z.r * z.r;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	// Speicher anfordern
	a = new double[n];
	}
// Abmessungen kopieren
r = z.r;
c = z.r;
double *pa = a;
int i = n;
while(--i >= 0){
	*pa++ = 0;
	}
pa = a;
double *pb = z.a;
i = r;
while(--i >= 0){
	*pa = *pb;
	pa += c + 1;
	pb += 1;
	}
return *this;
}

//
// Der Umkopierer
//

doublediagmatrix::doublediagmatrix(const doublesubdiagmatrix &z)

{
#ifdef DEBUG
if(z.r0 + z.r > z.ref.r){
	cerr << "ERROR from doublediagmatrix: Invalid submatrix\n";
	myexit(1);
	}
#endif
// Abmessungen kopieren
r = z.r;
// Speicher anfordern
a = new double[r];
memcpy(a, z.ref.a + z.r0, r * sizeof(double));
}

//
// Die Zuweisung
//

doublediagmatrix &doublediagmatrix::operator=(double z)

{
double *pa = a;
int i = r;
while(--i >= 0){
	*pa++ = z;
	}
return *this;
}

doublediagmatrix &doublediagmatrix::operator=(const doublediagmatrix &z)

{
if(r != z.r){
	// Abmessungen kopieren
	r = z.r;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	// Speicher anfordern
	a = new double[r];
	}
// Und alles kopieren
memcpy(a, z.a, r * sizeof(double));
return *this;
}

doublediagmatrix &doublediagmatrix::operator=(const doublesubdiagmatrix &z)

{
#ifdef DEBUG
if(z.r0 + z.r > z.ref.r){
	cerr << "ERROR from doublediagmatrix: Invalid submatrix\n";
	myexit(1);
	}
#endif
if(r != z.r){
	// Abmessungen kopieren
	r = z.r;
	// Gibt den Speicher frei (Test auf NULL nicht noetig?)
	delete[] a;
	// Speicher anfordern
	a = new double[r];
	}
memcpy(a, z.ref.a + z.r0, r * sizeof(double));
return *this;
}

//
// Die Zuweisung
//

doublesubmatrix &doublesubmatrix::operator=(const doublematrix &z)

{
#ifdef DEBUG
if(r0 + r > ref.r || c0 + c > ref.c){
	cerr << "ERROR from doublesubmatrix: Invalid submatrix\n";
	myexit(1);
	}
if(r != z.r || c != z.c){
	cerr << "ERROR from doublesubmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
double *pa = ref.a + r0 * ref.c + c0;
double *pz = z.a;
int i = r;
while(--i >= 0){
	memcpy(pa, pz, c * sizeof(double));
	pa += ref.c;
	pz += z.c;
	}
return *this;
}

doublesubmatrix &doublesubmatrix::operator=(const doublesubmatrix &z)

{
#ifdef DEBUG
if(r0 + r > ref.r || c0 + c > ref.c || z.r0 + z.r > z.ref.r || z.c0 + z.c > z.ref.c){
	cerr << "ERROR from doublesubmatrix: Invalid submatrix\n";
	myexit(1);
	}
if(r != z.r || c != z.c){
	cerr << "ERROR from doublesubmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
double *pa = ref.a + r0 * ref.c + c0;
double *pz = z.ref.a + z.r0 * z.ref.c + z.c0;
int i = r;
while(--i >= 0){
	memcpy(pa, pz, c * sizeof(double));
	pa += ref.c;
	pz += z.ref.c;
	}
return *this;
}

//
// Die Zuweisung
//

doublesubdiagmatrix &doublesubdiagmatrix::operator=(const doublediagmatrix &z)

{
#ifdef DEBUG
if(r0 + r > ref.r){
	cerr << "ERROR from doublesubdiagmatrix: Invalid submatrix\n";
	myexit(1);
	}
if(r != z.r){
	cerr << "ERROR from doublesubdiagmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
memcpy(ref.a + r0, z.a, r * sizeof(double));
return *this;
}

doublesubdiagmatrix &doublesubdiagmatrix::operator=(const doublesubdiagmatrix &z)

{
#ifdef DEBUG
if(r0 + r > ref.r || z.r0 + z.r > z.ref.r){
	cerr << "ERROR from doublesubdiagmatrix: Invalid submatrix\n";
	myexit(1);
	}
if(r != z.r){
	cerr << "ERROR from doublesubdiagmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
memcpy(ref.a + r0, z.ref.a + z.r0, r * sizeof(double));
return *this;
}

//
// Die Matrix
//

doubletempmatrix operator+(const doublematrix &a)

{
doubletempmatrix s(a.r, a.c);
// Und kopieren
double *ps = s.a;
double *pa = a.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pa++;
	}
return s;
}

//
// Die Matrix-Negation
//

doubletempmatrix operator-(const doublematrix &a)

{
doubletempmatrix s(a.r, a.c);
// Und negieren
double *ps = s.a;
double *pa = a.a;
int n = s.n;
while(--n >= 0){
	*ps++ = -(*pa++);
	}
return s;
}

//
// Die diagonale Matrix
//

doubletempdiagmatrix operator+(const doublediagmatrix &a)

{
doubletempdiagmatrix s(a.r);
// Und kopieren
double *ps = s.a;
double *pa = a.a;
int n = s.r;
while(--n >= 0){
	*ps++ = *pa++;
	}
return s;
}

//
// Die diagonale Matrix-Negation
//

doubletempdiagmatrix operator-(const doublediagmatrix &a)

{
doubletempdiagmatrix s(a.r);
// Und negieren
double *ps = s.a;
double *pa = a.a;
int n = s.r;
while(--n >= 0){
	*ps++ = -(*pa++);
	}
return s;
}

//
// Die Matrix-Multiplikation
//

doubletempmatrix operator*(const doublematrix &a, const doublematrix &b)

{
#ifdef DEBUG
if(a.c != b.r){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(a.r, b.c);
// Produktmatrix loeschen
double *ps = s.a;
int n = s.n;
while(--n >= 0){
	*ps++ = 0;
	}
// Und multiplizieren
double *pa = a.a;
double *pb = b.a;
int j = a.c;
while(--j >= 0){
	ps = s.a;
	int i = s.r;
	while(--i >= 0){
		double z = *pa;
		int k = s.c;
		while(--k >= 0){
			*ps++ += z * *pb++;
			}
		pa += a.c;
		pb -= b.c;
		}
	pa -= a.n - 1;
	pb += b.c;
	}
return s;
}

doubletempmatrix operator*(const doublematrix &a, const doublediagmatrix &b)

{
#ifdef DEBUG
if(a.c != b.r){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(a.r, a.c);
// Und multiplizieren
double *ps = s.a;
double *pa = a.a;
double *pb = b.a;
int i = s.r;
while(--i >= 0){
	int j = s.c;
	while(--j >= 0){
		*ps = *pa * *pb;
		ps += 1;
		pa += 1;
		pb += 1;
		}
	pb -= b.r;
	}
return s;
}

doubletempmatrix operator*(const doublediagmatrix &a, const doublematrix &b)

{
#ifdef DEBUG
if(a.r != b.r){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(b.r, b.c);
// Und multiplizieren
double *ps = s.a;
double *pa = a.a;
double *pb = b.a;
int i = s.r;
while(--i >= 0){
	int j = s.c;
	while(--j >= 0){
		*ps = *pa * *pb;
		ps += 1;
		pb += 1;
		}
	pa += 1;
	}
return s;
}

doubletempdiagmatrix operator*(const doublediagmatrix &a, const doublediagmatrix &b)

{
#ifdef DEBUG
if(a.r != b.r){
	cerr << "ERROR from doubletempdiagmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempdiagmatrix s(a.r);
// Und multiplizieren
double *ps = s.a;
double *pa = a.a;
double *pb = b.a;
int i = s.r;
while(--i >= 0){
	*ps++ = *pa++ * *pb++;
	}
return s;
}

//
// Die Matrix-Addition
//

doubletempmatrix operator+(const doublematrix &a, const doublematrix &b)

{
#ifdef DEBUG
if(a.r != b.r || a.c != b.c){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(a.r, a.c);
// Und addieren
double *ps = s.a;
double *pa = a.a;
double *pb = b.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pa++ + *pb++;
	}
return s;
}

doubletempmatrix operator+(const doublematrix &a, const doublediagmatrix &b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
	myexit(1);
	}
if(a.r != b.r){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(a.r, a.c);
// Erst kopieren
double *ps = s.a;
double *pa = a.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pa++;
	}
// Und addieren
ps = s.a;
double *pb = b.a;
int i = s.r;
while(--i >= 0){
	*ps += *pb;
	ps += s.c + 1;
	pb += 1;
	}
return s;
}

doubletempmatrix operator+(const doublediagmatrix &a, const doublematrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
	myexit(1);
	}
if(a.r != b.r){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(b.r, b.c);
// Erst kopieren
double *ps = s.a;
double *pb = b.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pb++;
	}
// Und addieren
ps = s.a;
double *pa = a.a;
int i = s.r;
while(--i >= 0){
	*ps += *pa;
	ps += s.c + 1;
	pa += 1;
	}
return s;
}

doubletempdiagmatrix operator+(const doublediagmatrix &a, const doublediagmatrix &b)

{
#ifdef DEBUG
if(a.r != b.r){
	cerr << "ERROR from doubletempdiagmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempdiagmatrix s(a.r);
// Und addieren
double *ps = s.a;
double *pa = a.a;
double *pb = b.a;
int i = s.r;
while(--i >= 0){
	*ps++ = *pa++ + *pb++;
	}
return s;
}

//
// Die Matrix-Subtraktion
//

doubletempmatrix operator-(const doublematrix &a, const doublematrix &b)

{
#ifdef DEBUG
if(a.r != b.r || a.c != b.c){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(a.r, a.c);
// Und subtrahieren
double *ps = s.a;
double *pa = a.a;
double *pb = b.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pa++ - *pb++;
	}
return s;
}

doubletempmatrix operator-(const doublematrix &a, const doublediagmatrix &b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
	myexit(1);
	}
if(a.r != b.r){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(a.r, a.c);
// Erst kopieren
double *ps = s.a;
double *pa = a.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pa++;
	}
// Und subtrahieren
ps = s.a;
double *pb = b.a;
int i = s.r;
while(--i >= 0){
	*ps -= *pb;
	ps += s.c + 1;
	pb += 1;
	}
return s;
}

doubletempmatrix operator-(const doublediagmatrix &a, const doublematrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
	myexit(1);
	}
if(a.r != b.r){
	cerr << "ERROR from doubletempmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempmatrix s(b.r, b.c);
// Erst negieren
double *ps = s.a;
double *pb = b.a;
int n = s.n;
while(--n >= 0){
	*ps++ = -(*pb++);
	}
// Und addieren
ps = s.a;
double *pa = a.a;
int i = s.r;
while(--i >= 0){
	*ps += *pa;
	ps += s.c + 1;
	pa += 1;
	}
return s;
}

doubletempdiagmatrix operator-(const doublediagmatrix &a, const doublediagmatrix &b)

{
#ifdef DEBUG
if(a.r != b.r){
	cerr << "ERROR from doubletempdiagmatrix: Matrices do not match\n";
	myexit(1);
	}
#endif
doubletempdiagmatrix s(a.r);
// Und subtrahieren
double *ps = s.a;
double *pa = a.a;
double *pb = b.a;
int i = s.r;
while(--i >= 0){
	*ps++ = *pa++ - *pb++;
	}
return s;
}

//
// Die Matrix-Reell-Multiplikation
//

doubletempmatrix operator*(const doublematrix &a, double b)

{
doubletempmatrix s(a.r, a.c);
// Und multiplizieren
double *ps = s.a;
double *pa = a.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pa++ * b;
	}
return s;
}

doubletempdiagmatrix operator*(const doublediagmatrix &a, double b)

{
doubletempdiagmatrix s(a.r);
// Und multiplizieren
double *ps = s.a;
double *pa = a.a;
int n = s.r;
while(--n >= 0){
	*ps++ = *pa++ * b;
	}
return s;
}

//
// Die Matrix-Reell-Addition
//

doubletempmatrix operator+(const doublematrix &a, double b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempmatrix s(a.r, a.c);
// Erst kopieren
double *ps = s.a;
double *pa = a.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pa++;
	}
// Und addieren
ps = s.a;
int i = s.r;
while(--i >= 0){
	*ps += b;
	ps += s.c + 1;
	}
return s;
}

doubletempdiagmatrix operator+(const doublediagmatrix &a, double b)

{
doubletempdiagmatrix s(a.r);
// Und addieren
double *ps = s.a;
double *pa = a.a;
int n = s.r;
while(--n >= 0){
	*ps++ = *pa++ + b;
	}
return s;
}

//
// Die Matrix-Reell-Subtraktion
//

doubletempmatrix operator-(const doublematrix &a, double b)

{
#ifdef DEBUG
if(a.r != a.c){
	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempmatrix s(a.r, a.c);
// Erst kopieren
double *ps = s.a;
double *pa = a.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pa++;
	}
// Und subtrahieren
ps = s.a;
int i = s.r;
while(--i >= 0){
	*ps -= b;
	ps += s.c + 1;
	}
return s;
}

doubletempdiagmatrix operator-(const doublediagmatrix &a, double b)

{
doubletempdiagmatrix s(a.r);
// Und subtrahieren
double *ps = s.a;
double *pa = a.a;
int n = s.r;
while(--n >= 0){
	*ps++ = *pa++ - b;
	}
return s;
}

//
// Die Reell-Matrix-Multiplikation
//

doubletempmatrix operator*(double a, const doublematrix &b)

{
doubletempmatrix s(b.r, b.c);
// Und multiplizieren
double *ps = s.a;
double *pb = b.a;
int n = s.n;
while(--n >= 0){
	*ps++ = a * *pb++;
	}
return s;
}

doubletempdiagmatrix operator*(double a, const doublediagmatrix &b)

{
doubletempdiagmatrix s(b.r);
// Und multiplizieren
double *ps = s.a;
double *pb = b.a;
int n = s.r;
while(--n >= 0){
	*ps++ = a * *pb++;
	}
return s;
}

//
// Die Reell-Matrix-Addition
//

doubletempmatrix operator+(double a, const doublematrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempmatrix s(b.r, b.c);
// Erst kopieren
double *ps = s.a;
double *pb = b.a;
int n = s.n;
while(--n >= 0){
	*ps++ = *pb++;
	}
// Und addieren
ps = s.a;
int i = s.r;
while(--i >= 0){
	*ps += a;
	ps += s.c + 1;
	}
return s;
}

doubletempdiagmatrix operator+(double a, const doublediagmatrix &b)

{
doubletempdiagmatrix s(b.r);
// Und addieren
double *ps = s.a;
double *pb = b.a;
int n = s.r;
while(--n >= 0){
	*ps++ = a + *pb++;
	}
return s;
}

//
// Die Reell-Matrix-Subtraktion
//

doubletempmatrix operator-(double a, const doublematrix &b)

{
#ifdef DEBUG
if(b.r != b.c){
	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
	myexit(1);
	}
#endif
doubletempmatrix s(b.r, b.c);
// Erst negieren
double *ps = s.a;
double *pb = b.a;
int n = s.n;
while(--n >= 0){
	*ps++ = -(*pb++);
	}
// Und addieren
ps = s.a;
int i = s.r;
while(--i >= 0){
	*ps += a;
	ps += s.c + 1;
	}
return s;
}

doubletempdiagmatrix operator-(double a, const doublediagmatrix &b)

{
doubletempdiagmatrix s(b.r);
// Und subtrahieren
double *ps = s.a;
double *pb = b.a;
int n = s.r;
while(--n >= 0){
	*ps++ = a - *pb++;
	}
return s;
}

//
// Die Matrix-Inversion
//

//  doubletempmatrix inv(const doublematrix &a)

//  {
//  int	i, j;

//  #ifdef DEBUG
//  if(a.r != a.c){
//  	cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
//  	myexit(1);
//  	}
//  #endif
//  doubletempmatrix s(a.r, a.c);
//  // Und kopieren
//  memcpy(s.a, a.a, s.n * sizeof(double));
//  // Temporaerer Speicher
//  int n = s.r;
//  double *ff = new double[n];
//  double *xx = new double[n];
//  int *ii = new int[n];
//  // Zeilennummern vorbesetzen
//  for(i = 0; i < n; i++){
//  	ii[i] = i;
//  	}
//  // Und invertieren
//  // Current line
//  double *pz = s.a;
//  for(i = 0; i < n; i++){
//  	// groessten Wert suchen
//  	double max = fabs(pz[i]);
//  	int m = i;
//  	// Max Line
//  	double *pm = pz;
//  	// Next Line
//  	double *pn = pz;
//  	for(j = i + 1; j < n; j++){
//  		pn += n;
//  		double cmp = fabs(pn[i]);
//  		if(cmp > max){
//  			max = cmp;
//  			m = j;
//  			pm = pn;
//  			}
//  		}
//  	// Gegebenenfalls vertauschen
//  	if(m != i){
//  		memcpy(xx, pz, n * sizeof(double));
//  		memcpy(pz, pm, n * sizeof(double));
//  		memcpy(pm, xx, n * sizeof(double));
//  		// Vertauschung merken
//  		int j = ii[i];
//  		ii[i] = ii[m];
//  		ii[m] = j;
//  		}
//  	ff[i] = 1 / pz[i];
//  	pz[i] = 1;
//  	// Durch alle Zeilen
//  	double *ps = s.a;
//  	j = n;
//  	while(--j >= 0){
//  		// Ausser der aktuellen Zeile
//  		if(ps != pz){
//  			// Faktor festlegen
//  			double cf = ps[i] * ff[i];
//  			ps[i] = 0;
//  			// Zeilen subtrahieren
//  			int k = n;
//  			while(--k >= 0){
//  				*ps -= cf * *pz;
//  				ps += 1;
//  				pz += 1;
//  				}
//  			pz -= n;
//  			}
//  		else{
//  			ps += n;
//  			}
//  		}
//  	// Naechste Zeile;
//  	pz += n;
//  	}
//  // Und zuruecktauschen
//  double *ps = s.a;
//  for(i = 0; i < n; i++){
//  	memcpy(xx, ps, n * sizeof(double));
//  	double cf = ff[i];
//  	for(j = 0; j < n; j++){
//  		ps[ii[j]] = cf * xx[j];
//  		}
//  	ps += n;
//  	}
//  // Temporaeren Speicher wieder freigeben
//  delete[] ii;
//  delete[] xx;
//  delete[] ff;
//  return s;
//  }

doubletempmatrix inv(const doublematrix &a)

{
int      i, j, k;

#ifdef DEBUG
if(a.r != a.c){
 cerr << "ERROR from doubletempmatrix: Matrix must be square\n";
 myexit(1);
 }
#endif
doubletempmatrix s(a.r, a.c);
// Und kopieren
memcpy(s.a, a.a, s.n * sizeof(double));
// Temporaerer Speicher
int n = s.r;
double *ff = new double[n];
double *xx = new double[n];
int *ii = new int[n];
// Zeilennummern vorbesetzen
for(i = 0; i < n; i++){
 ii[i] = i;
 }
// Und invertieren
// Current line
double *pz = s.a;
for(i = 0; i < n; i++){
 // groessten Wert suchen
 double max = 0;
 int m = 0;
 // Max Line
 double *pm = NULL;
 // Next Line
 double *pn = pz;
 for(j = i; j < n; j++){
         double *po = pn + i;
         double cmp = fabs(*po++);
         double sum = cmp;
         for(k = i + 1; k < n; k++){
                 sum += fabs(*po++);
                 }
         if(cmp > sum * max){
                 max = cmp / sum;
                 m = j;
                 pm = pn;
                 }
         pn += n;
         }
 // Matrix singulaer?
 if(pm == NULL){
         cout << "ERROR from doublematrix::inv(): Matrix is singular\n";
         myexit(1);
         }
 // Gegebenenfalls vertauschen
 if(m != i){
         memcpy(xx, pz, n * sizeof(double));
         memcpy(pz, pm, n * sizeof(double));
         memcpy(pm, xx, n * sizeof(double));
         // Vertauschung merken
         int jj = ii[i];
         ii[i] = ii[m];
         ii[m] = jj;
         }
 ff[i] = 1 / pz[i];
 pz[i] = 1;
 // Durch alle Zeilen
 double *ps = s.a;
 j = n;
 while(--j >= 0){
         // Ausser der aktuellen Zeile
         if(ps != pz){
                 // Faktor festlegen
                 double cf = ps[i] * ff[i];
                 ps[i] = 0;
                 // Zeilen subtrahieren
                 int kk = n;
                 while(--kk >= 0){
                         *ps -= cf * *pz;
                         ps += 1;
                         pz += 1;
                         }
                 pz -= n;
                 }
         else{
                 ps += n;
                 }
         }
 // Naechste Zeile;
 pz += n;
 }
// Und zuruecktauschen
double *ps = s.a;
for(i = 0; i < n; i++){
 memcpy(xx, ps, n * sizeof(double));
 double cf = ff[i];
 for(j = 0; j < n; j++){
         ps[ii[j]] = cf * xx[j];
         }
 ps += n;
 }
// Temporaeren Speicher wieder freigeben
delete[] ii;
delete[] xx;
delete[] ff;
return s;
}


doubletempdiagmatrix inv(const doublediagmatrix &a)

{
doubletempdiagmatrix s(a.r);
// Und invertieren
double *ps = s.a;
double *pa = a.a;
int n = s.r;
while(--n >= 0){
	*ps++ = 1 / *pa++;
	}
return s;
}

//
// Die transponierte Matrix
//

doubletempmatrix transp(const doublematrix &a)

{
doubletempmatrix s(a.c, a.r);
// Und transponieren
double *ps = s.a;
double *pa = a.a;
int i = s.r;
while(--i >= 0){
	int j = s.c;
	while(--j >= 0){
		*ps = *pa;
		ps += 1;
		pa += a.c;
		}
	pa -= a.n - 1;
	}
return s;
}

doubletempdiagmatrix transp(const doublediagmatrix &a)

{
doubletempdiagmatrix s(a.r);
// Und transponieren
double *ps = s.a;
double *pa = a.a;
int n = s.r;
while(--n >= 0){
	*ps++ = *pa++;
	}
return s;
}

doubletempdiagmatrix abs(const doublediagmatrix &a)

{
doubletempdiagmatrix s(a.r);
// Und transponieren
double *ps = s.a;
double *pa = a.a;
int n = s.r;
while(--n >= 0){
	*ps++ = fabs(*pa++);
	}
return s;
}

//
// Die Norm einer Matrix
//

double norm(const doublematrix &a)

{
double norm = 0;
double *pa = a.a;
int n = a.n;
while(--n >= 0){
	double x = fabs(*pa);
	pa += 1;
	if( x > norm ){
		norm = x;
		}
	}
return norm;
}

double norm(const doublediagmatrix &a)

{
double norm = 0;
double *pa = a.a;
int n = a.r;
while(--n >= 0){
	double x = fabs(*pa);
	pa += 1;
	if( x > norm ){
		norm = x;
		}
	}
return norm;
}


// Check whether two matrices are identical
int diff(const doublematrix &a, const doublematrix &b)
{

  int i, j;

  int d = 0;

  for(i = 0; i < a.r; i++){
    for(j = 0; j < a.c; j++){
      if(fabs(a(i,j) - b(i,j)) > 1.e-6){
	d = 1;
      }
    }
  }

  return d;
}

int diff(const doublediagmatrix &a, const doublediagmatrix &b)
{
  int i;

  int d = 0;

  for(i = 0; i < a.r; i++){
    if(fabs(a(i) - b(i)) > 1.e-6){
      d = 1;
    }
  }
  
  return d;
}

// Trace of a
double trace(const doublematrix &a)
{

  int i;

  double d;

  if(a.r != a.c){
    myerror("ERROR from doublematrix::trace(): Non-square matrix.", 1);
  }
  
  d = 0.0;
  for(i = 0; i < a.r; i++){
    d = d + a(i, i);
  }

  return d;
}

double trace(const doublediagmatrix &a)
{

  int i;

  double d;

  d = 0.0;
  for(i = 0; i < a.r; i++){
    d = d + a(i);
  }

  return d;
}


//
// Ein- und Ausgabe
//

ostream &operator<<(ostream &os, const doublematrix &a)

{
int w = os.width(0);
int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
 long f = os.flags(std::_Ios_Fmtflags(0));
#else
 long f = os.flags(0);
#endif
double *pa = a.a;
int i = a.r;
while(--i >= 0){
	int j = a.c;
	while(--j >= 0){
		os.width(w);
		os.precision(p);
#if defined(GCC3) || defined(GCC4)
		os.flags(std::_Ios_Fmtflags(f));
#else
		os.flags(f);
#endif
		os << *pa++;
		if(j != 0){
			os << " ";
			}
		}
	os << "\n";
	}
return os;
}

ostream &operator<<(ostream &os, const doublediagmatrix &a)

{
int w = os.width(0);
int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
 long f = os.flags(std::_Ios_Fmtflags(0));
#else
 long f = os.flags(0);
#endif
 double *pa = a.a;
int i = a.r;
while(--i >= 0){
	os.width(w);
	os.precision(p);
#if defined(GCC3) || defined(GCC4)
	os.flags(std::_Ios_Fmtflags(f));
#else
	os.flags(f);
#endif
	os << *pa++;
	if(i != 0){
		os << " ";
		}
	}
os << "\n";
return os;
}

istream &operator>>(istream &is, const doublematrix &a)

{
double *pa = a.a;
int i = a.r;
while(--i >= 0){
	int j = a.c;
	while(--j >= 0){
		is >> *pa++;
		}
	}
return is;
}

istream &operator>>(istream &is, const doublediagmatrix &a)

{
double *pa = a.a;
int i = a.r;
while(--i >= 0){
	is >> *pa++;
	}
return is;
}
