// Balance the matrix A (see Numerical Recipes, 11.5)

#ifndef _BALANCE_
#define _BALANCE_

#include "cmatrix.h"

void balance(complexmatrix &a, int n);


#endif
