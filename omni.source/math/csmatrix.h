// csmatrix.h
//
// deklariert komplexe Super-Matrizen, sowie mathematische Operationen zwischen
// Matrizen, reellen und komplexen Zahlen.
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//
// Hinweis: eigentlich waere es sinnvoller Matrizen und Vektoren durch
// templates zu definieren. Da templates zu dieser Zeit jedoch noch nicht
// sehr weit verbreitet sind ( zumindestens noch seltener als C++ Compiler),
// findet die Definition hier ohne templates statt. Es sollte jedoch recht
// einfach sein, die Definitionen auf templates umzustellen.
//
// Definition von DEBUG fuegt ausfuehrliche Fehlertests ein.
//

#ifndef _CSMATRIX_
#define _CSMATRIX_

#include "platform.h"

#include "cmatrix.h"

#ifndef NULL
#define NULL    0
#endif

//
// Diese Klassen werden definiert:
//

class complexsupermatrix;
// JH - Added the following lines for compatibility with gcc4.1
// Thus, the compiler option -ffriend-injection is not needed
complex  trace(const complexsupermatrix &);
double   norm(const complexsupermatrix &);

class complextempsupermatrix;

//
// Definition der Super-Matrixklasse
//

class complexsupermatrix{
protected:
  int		r, c;		// Anzahl Zeilen (r) und Spalten (c)
  int     	n;		// Gesamtgroesse der Super-Matrix
  int		rs, cs;		// Anzahl Zeilen und Spalten je Block
  complexmatrix	*m;             // Zeiger auf das eigentliche Feld
public:
  // Die Konstruktoren
  complexsupermatrix();
  complexsupermatrix(int, int, int, int);
  // Der Kopierkonstruktor
  complexsupermatrix(const complexsupermatrix &);
  // Der Umkopierer
  complexsupermatrix(const complextempsupermatrix &);
  // Der Destruktor
  ~complexsupermatrix();
  // Zugriff auf einzelne Matrixelemente
  complexmatrix &operator()(int, int) const;
  // Die Zuweisung
  complexsupermatrix &operator=(double);
  complexsupermatrix &operator=(complex &);
  complexsupermatrix &operator=(complexmatrix &);
  complexsupermatrix &operator=(const complexsupermatrix &);
  complexsupermatrix &operator=(const complextempsupermatrix &);
  // Setze die Groesse
  complexsupermatrix &setsize(int, int, int, int);
  void getsize(int &, int &, int &, int &) const;
  void getsize(int &, int &) const;
  void getrawdata(double *&) const;
  void setrawdata(const double *, const int, const int, const int, const int);
  // Zuweisungen zwischen Matrizen und Supermatrizen
  friend complexmatrix &complexmatrix::operator=(const complexsupermatrix &);
  // Unaere Operatoren
  friend complextempsupermatrix operator+(const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complexsupermatrix &);
  // Binaere Operatoren
  friend complextempsupermatrix operator*(const complexsupermatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator+(const complexsupermatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complexsupermatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator*(const complexsupermatrix &, const complexmatrix &);
  friend complextempsupermatrix operator+(const complexsupermatrix &, const complexmatrix &);
  friend complextempsupermatrix operator-(const complexsupermatrix &, const complexmatrix &);
  friend complextempsupermatrix operator*(const complexmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator+(const complexmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complexmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator*(const complexsupermatrix &, const complexdiagmatrix &);
  friend complextempsupermatrix operator+(const complexsupermatrix &, const complexdiagmatrix &);
  friend complextempsupermatrix operator-(const complexsupermatrix &, const complexdiagmatrix &);
  friend complextempsupermatrix operator*(const complexdiagmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator+(const complexdiagmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complexdiagmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator*(const complexsupermatrix &, const complex &);
  friend complextempsupermatrix operator+(const complexsupermatrix &, const complex &);
  friend complextempsupermatrix operator-(const complexsupermatrix &, const complex &);
  friend complextempsupermatrix operator*(const complex &, const complexsupermatrix &);
  friend complextempsupermatrix operator+(const complex &, const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complex &, const complexsupermatrix &);
  friend complextempsupermatrix operator*(const complexsupermatrix &, double);
  friend complextempsupermatrix operator+(const complexsupermatrix &, double);
  friend complextempsupermatrix operator-(const complexsupermatrix &, double);
  friend complextempsupermatrix operator*(double, const complexsupermatrix &);
  friend complextempsupermatrix operator+(double, const complexsupermatrix &);
  friend complextempsupermatrix operator-(double, const complexsupermatrix &);
  // Funktionen
  friend complextempsupermatrix inv(const complexsupermatrix &);
  friend complextempsupermatrix transp(const complexsupermatrix &);
  friend complextempsupermatrix conj(const complexsupermatrix &);
  friend complextempsupermatrix herm(const complexsupermatrix &);
  friend complex                trace(const complexsupermatrix &);
  friend double                 norm(const complexsupermatrix &);
  friend std::ostream &operator<<(std::ostream &, const complexsupermatrix &);
};

//
// Definition der temporaeren Super-Matrixklasse
//

class complextempsupermatrix{
protected:
  int		r, c;		// Anzahl Zeilen (r) und Spalten (c)
  int     	n;              // Gesamtgroesse der Super-Matrix
  int		rs, cs;		// Anzahl Zeilen und Spalten je Block
  complexmatrix 	*m;             // Zeiger auf das eigentliche Feld
public:
  // Der Konstruktor
  complextempsupermatrix(int, int, int, int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class complexsupermatrix;
  // Unaere Operatoren
  friend complextempsupermatrix operator+(const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complexsupermatrix &);
  // Binaere Operatoren
  friend complextempsupermatrix operator*(const complexsupermatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator+(const complexsupermatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complexsupermatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator*(const complexsupermatrix &, const complexmatrix &);
  friend complextempsupermatrix operator+(const complexsupermatrix &, const complexmatrix &);
  friend complextempsupermatrix operator-(const complexsupermatrix &, const complexmatrix &);
  friend complextempsupermatrix operator*(const complexmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator+(const complexmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complexmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator*(const complexsupermatrix &, const complexdiagmatrix &);
  friend complextempsupermatrix operator+(const complexsupermatrix &, const complexdiagmatrix &);
  friend complextempsupermatrix operator-(const complexsupermatrix &, const complexdiagmatrix &);
  friend complextempsupermatrix operator*(const complexdiagmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator+(const complexdiagmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complexdiagmatrix &, const complexsupermatrix &);
  friend complextempsupermatrix operator*(const complexsupermatrix &, const complex &);
  friend complextempsupermatrix operator+(const complexsupermatrix &, const complex &);
  friend complextempsupermatrix operator-(const complexsupermatrix &, const complex &);
  friend complextempsupermatrix operator*(const complex &, const complexsupermatrix &);
  friend complextempsupermatrix operator+(const complex &, const complexsupermatrix &);
  friend complextempsupermatrix operator-(const complex &, const complexsupermatrix &);
  friend complextempsupermatrix operator*(const complexsupermatrix &, double);
  friend complextempsupermatrix operator+(const complexsupermatrix &, double);
  friend complextempsupermatrix operator-(const complexsupermatrix &, double);
  friend complextempsupermatrix operator*(double, const complexsupermatrix &);
  friend complextempsupermatrix operator+(double, const complexsupermatrix &);
  friend complextempsupermatrix operator-(double, const complexsupermatrix &);
  // Funktionen
  friend complextempsupermatrix inv(const complexsupermatrix &);
  friend complextempsupermatrix transp(const complexsupermatrix &);
  friend complextempsupermatrix conj(const complexsupermatrix &);
  friend complextempsupermatrix herm(const complexsupermatrix &);
};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine Super-Matrix
//

inline  complexsupermatrix::complexsupermatrix()
{
  // Keine Zeilen und Spalten
  r = 0;
  c = 0;
  // Keine Groesse
  n = 0;
  // Keine Zeilen und Spalten pro Block
  rs = 0;
  cs = 0;
  // Kein Speicher
  m = NULL;
}

//
// Erzeuge eine beliebige Super-Matrix
//

inline  complexsupermatrix::complexsupermatrix(int n1, int n2, int m1, int m2)
{
#ifdef DEBUG
  if(n1 <= 0 || n2 <= 0){
    cerr << "ERROR from complexsupermatrix: Invalid dimensions\n";
    myexit(1);
  }
#endif
  // Anzahl Zeilen und Spalten
  r = n1;
  c = n2;
  // Groesse berechnen
  n = r * c;
  // Anzahl Zeilen und Spalten
  rs = m1;
  cs = m2;
  // Noetigen Speicher anfordern
  m = new complexmatrix[n];
}

//
// Der Umkopierer
//

inline  complexsupermatrix::complexsupermatrix(const complextempsupermatrix &z)
{
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Groesse kopieren
  n = z.n;
  // Abmessungen je Block kopieren
  rs = z.rs;
  cs = z.cs;
  // Einfach Zeiger kopieren
  m = z.m;
}

//
// Der Destruktor
//

inline  complexsupermatrix::~complexsupermatrix()

{
  // Gibt den Speicher frei (Test auf NULL nicht noetig?)
  delete[] m;
}

//
// Zugriff auf einzelne Super-Matrixelemente
//

inline  complexmatrix &complexsupermatrix::operator()(int n1, int n2) const
{
#ifdef DEBUG
  if(n1 < 0 || n1 >= r || n2 < 0 || n2 >= c){
    cerr << "ERROR from complexmatrix: Subscript out of range\n";
    myexit(1);
  }
#endif
  // Gib Referenz auf Element zurueck
  return m[n1 * c + n2];
}

//
// Die Zuweisung
//

inline  complexsupermatrix &complexsupermatrix::operator=(const complextempsupermatrix &z)
{
  // Gibt den Speicher frei (Test auf NULL nicht noetig?)
  delete[] m;
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Groesse kopieren
  n = z.n;
  // Abmessungen por Block kopieren
  rs = z.rs;
  cs = z.cs;
  // Einfach Zeiger kopieren
  m = z.m;
  return *this;
}

//
// Zuweisungen zwischen Matrizen und Supermatrizen
//

inline complexmatrix &complexmatrix::operator=(const complexsupermatrix &z)
{
#ifdef DEBUG
  if(z.r != 1 || z.c != 1){
    cerr << "ERROR from complexmatrix: Invalid dimensions\n";
    myexit(1);
  }
#endif
  *this = z.m[0];
  return *this;
}

//
// Setze die Groesse einer Super-Matrix
//

inline  complexsupermatrix &complexsupermatrix::setsize(int n1, int n2, int m1, int m2)
{
#ifdef DEBUG
  if(n1 <= 0 || n2 <= 0){
    cerr << "ERROR from complexsupermatrix: Invalid dimensions\n";
    myexit(1);
  }
#endif
  if(n != n1 * n2){
    // Groesse berechnen
    n = n1 * n2;
    // Gibt den Speicher frei (Test auf NULL nicht noetig?)
    delete[] m;
    // Noetigen Speicher anfordern
    m = new complexmatrix[n];
  }
  // Anzahl Zeilen und Spalten
  r = n1;
  c = n2;
  // Anzahl Zeilen und Spalten pro Block
  rs = m1;
  cs = m2;
  return *this;
}

inline void complexsupermatrix::getsize(int &rr, int &cc, int &rss, int &css) const
{
  rr = r;
  cc = c;
  rss = rs;
  css = cs;
}

inline void complexsupermatrix::getsize(int &rr, int &cc) const
{
  rr = r;
  cc = c;
}

inline void complexsupermatrix::getrawdata(double *&data) const
{
  //just a long array of doubles, the 2 is because the data is complex
  if(data == NULL)
    data = new double[r*c*rs*cs*2];
  
  double *p = data;
  
  for(int i=0; i<r*c; i++){
    m[i].getrawdata(p);
    p += rs*cs*2;
  }
}

inline void complexsupermatrix::setrawdata(const double *data, const int rr, const int cc, const int rss, const int css)
{
  r = rr;
  c = cc;
  rs = rss;
  cs = css;
  n = r*c;
  
  if(m != NULL)
    delete[] m;
  m = new complexmatrix[n];
  
  const double *p = data;

  for(int i=0; i<n; i++ ){
    m[i].setrawdata(p, rs, rs);
    p += rs*cs*2;
  }
}

//
// Erzeuge eine temporaere Super-Matrix
//

inline  complextempsupermatrix::complextempsupermatrix(int n1, int n2, int m1, int m2)
{
  // Anzahl Zeilen und Spalten
  r = n1;
  c = n2;
  // Groesse berechnen
  n = n1 * n2;
  // Anzahl Zeilen und Spalten pro Block
  rs = m1;
  cs = m2;
  // Noetigen Speicher anfordern
  m = new complexmatrix[n];
}

  
#endif
  
