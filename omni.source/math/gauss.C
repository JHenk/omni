#include <iomanip>
#include <iostream>
#include <fstream>

#include "platform.h"
#include "complex.h"
#include "sfunc.h"

using namespace std;

#define EPS 3.0e-11

// Compute the abscissas and weights used in a n-point Gauss-Legendre
// quadrature
// Taken from Numerical Recipes
//
// OBS: The Gaussian mesh has indices from 1 ... n, *not* from 0 ... n-1.
void gauleg(double x1, double x2, double *x, double *w, int n){

  static int flag = 0;

  int    m,  j, i;
  double z1, z, xm, xl, pp, p3,  p2, p1;
  
  m  = (n+1) / 2;
  xm = 0.5 * (x2+x1);
  xl = 0.5 * (x2-x1);
  
  for (i = 1; i <= m;i++){
    z = cos(M_PI * (i-0.25) / (n+0.5));
    do {
      p1 = 1.0;
      p2 = 0.0;
      for(j = 1; j <= n; j++){
	p3 = p2;
	p2 = p1;
	p1 = ((2.0*j-1.0) * z * p2 - (j-1.0) * p3) / j;
      }
      pp = n * (z*p1-p2) / (z*z-1.0);
      z1 = z;
      z  = z1 - p1/pp;
    } while (fabs(z-z1) > EPS);
    
    x[i]     = xm - xl * z;
    x[n+1-i] = xm + xl * z;
    w[i]     = 2.0 * xl / ((1.0-z*z) * pp * pp);
    w[n+1-i] = w[i];
  }

  // Output, only on first call
  if(flag == 0){
    cout << "MESSAGE from gauleg(): Gauss-Legendre mesh points and weights:" << endl;
    for(i = 1; i <= n; i++){
      cout << setw(3)  << i;
      cout << " " << setw(20) << setprecision(15) << x[i];
      cout << " " << setw(20) << setprecision(15) << w[i] << endl;
    }
    flag = 1;
  }
}


// Perform a N-point Gauss-Legendre quadrature (see Abramowitz/Steegun Table 25.4.)
// N       number of abscissa
// weight  weighting factors
// values  ordinates

complex gauss(int N, complex *weight, complex *values){

  int i;
  complex sum;

  sum = 0.0;
  for(i = 1; i <= N; i++){
    sum += weight[i] * values[i];
  }

  return sum;
}


// Read abscissa and weights for N-point Gauss-Legendre quadrature
// N          number of abscissa
// abscissa  the abscissa in [-1, ..., +1]
// weight     weighting factors

void read_gauss(int N, double *abscissa, double *weight){

  int i, idum;

  const char     *name;

  ifstream inp;

  switch(N){
  case 2:
    name = "gauss.2.dat";
    break;
  case 3:
    name = "gauss.3.dat";
    break;
  case 4:
    name = "gauss.4.dat";
    break;
  case 8:
    name = "gauss.8.dat";
    break;
  case 16:
    name = "gauss.16.dat";
    break;
  case 32:
    name = "gauss.32.dat";
    break;
  default:
    name = "";
    cerr << "ERROR from read_gauss(): N wrong!" << endl;
    myexit(1);
    break;
  }

  inp.open(name);
  for(i = 1; i <= N; i++){
    inp >> idum >> abscissa[i] >> weight[i];
  }
  inp.close();
}

// Transform abscissa and weights for N-point Gauss-Legendre quadrature
// (cf. Abramowitz/Steegun 25.4.30)
//
// N          number of abscissa
// abscissa  the abscissa in [-1, ..., +1], overwritten

void tform_gauss(int N, double *abscissa, double *weight, double left, double right){

  int i;

  for(i = 1; i <= N; i++){
    abscissa[i] = 0.5 * (right - left) * abscissa[i] + 0.5 * (left + right);
    weight[i]    = 0.5 * (right - left) * weight[i];
  }

}

// Transform abscissa and weights for N-point Gauss-Legendre quadrature
// for logarithmic mesh (see note)
//
// N          number of abscissa
// abscissa  the abscissa in [-1, ..., +1], overwritten

void tform2_gauss(int N, double *abscissa, double *weight, double left, double right, double beta){

  int i;

  double alpha, gamma;

  if(fabs(beta) < 1.e-10){
    cerr << "ERROR from tform2_gauss(): beta too small." << endl;
    myexit(1);
  }

  alpha = (right - left) / (exp(beta) - exp(-beta));
  gamma = (left * exp(beta) - right * exp(-beta)) / (exp(beta) - exp(-beta));

  for(i = 1; i <= N; i++){
    weight[i]   = alpha * beta * weight[i] * exp(beta * abscissa[i]);
    abscissa[i] = alpha * exp(beta * abscissa[i]) + gamma;
  }


}



// Laguerre polynomial
//
// x - abscissa
// n - order of the polynomial
double Laguerre(double x, int n){
 
  double lag, fxi;

  double *fac;
  
  int i;
  
  if(n < 0){
    cerr << "ERROR from Laguerre(): order is negative. " << n << endl;
    myexit(1);
  }
  
  // Array of factorials
  fac = new double[n + 1];
  // Initial value
  fac[0] = 1;
  for(i = 1; i <= n; i++){
    fac[i] = fac[i - 1] * i;
  }

  // Initialization
  // i = 0;
  lag = binom(n, 0);
  fxi = 1.0;
  
  // Loop over all i > 1;
  for(i = 1; i <= n; i++){
//    lag +=  fac[n] / (fac[n - i] * fac[i] * fac[i]) * pow(-x, i);
    fxi *= -x / i;
    lag += binom(n, i) * fxi;
//    lag += binom(n, i) / fac[i] * pow(-x, i);
  }
  
  delete[] fac;
  
  return lag;
  
}


// Laguerre polynomial, using recursion
//
// x - abscissa
// n - order of the polynomial
double Laguerre_recursive(double x, int n){
 
  double lag;

  lag = 0;

  switch(n){
    case 0:
      lag = 1.0;
      break;
    case 1:
      lag = 1.0 - x;
      break;
    case  2:
      lag = (x * x - 4 * x + 2) / 2;
      break;
    case  3:
      lag = (-x * x * x + 9 * x * x - 18 * x + 6) / 6;
      break;
    case  4:
      lag = (x * x * x * x - 16 * x * x * x + 72 * x * x - 96 * x + 24) / 24;
      break;
    case  5:
      lag = (-x * x * x * x * x + 25 * x * x * x * x - 200 * x * x * x + 600 * x * x - 600 * x + 120) / 120;
      break;
    case  6:
      lag = (x * x * x * x * x * x - 36 * x * x * x * x * x + 450 * x * x * x * x - 2400 * x * x * x + 5400 * x * x - 4320 * x + 720) / 720;
      break;
    default:
      lag = ((2 * n - 1 - x) * Laguerre_recursive(x, n-1) - (n - 1) * Laguerre_recursive(x, n-2)) / n;
      break;  
  }
  
  return lag;
  
}

// First derivative of a Laguerre polynomial
// Abramowitz-Stegun 22.8.
//
// x - abscissa
// n - order of the polynomial
double Laguerre_deriv(double x, int n){
 
  double lag;

  lag = 0.0;
  
  if(n == 0){
    lag = 0; 
  }
  else if(n == 1){
    lag = -1;
  }
  else{
    if(fabs(x) < EMACH){
      cerr << "ERROR from Laguerre_deriv(): x equals zero." << endl;
      myexit(1);
    }
    else{
      lag = n * (Laguerre(x, n) - Laguerre(x, n-1)) / x;
    }
  }
  
  return lag;
  
}

// Return the ratio L_n(x) / L_n(x)' (logarithmic derivative) of Laguerre polynomials
// Uses the Shao-Chen-Frank algorithm
//
// x - abscissa
// n - order of the Laguerre polynomial
double ShaoChenFrank(double x, int n){
  
  int i;
  
  double lnfrac;
  
  // Initialization
  lnfrac = 0.0;
  
  // Continued fraction a la Shao-Chen-Frank
  // Equation (1.5) in R. J. Mathar
  for(i = n - 1; i >= 0; i--){
    lnfrac = (n - i) * (n - i) / (2.0 * n - 2.0 * i - 1.0 - x - lnfrac);
  }
  lnfrac = x / (n - lnfrac);
  
  return lnfrac;
}


// Run three steps of Newton iteration
//
// x - estimated root
// n - order of the Laguerre polynomial
double lagroot(double x, int n){
  
  int i;
  
  double xref, ffprime;
  
  xref = x;
  
  // Newton iteration, equation (1.4) in R. J. Mathar
  for(i = 0; i < 3; i++){
    ffprime = ShaoChenFrank(xref, n);
    xref    = xref - ffprime * (1.0 + 0.5 * (1.0 - 1.0 / xref) * ffprime);
  }
  
  return xref;
}

// Compute the abscissas and weights used in a n-point Gauss-Laguerre
// quadrature
// Adapted from from R. J. Mathar
void gaulag(double *x, double *w, int n){

  static int flag = 0;
  
  int i, j;
  
  double left, right, kn, sum, product, fac;
  double xi, x1, x2, f, fmid, rtb, dx, xmid, xstep;
  
  // Roots of the Laguerre polynomial of order n
  x[0] = 0.0;

  // Finding roots by bisectioning
  // - Left and right boundary; see Abramowitz-Stegun 22.16.8.
  kn = double(n + 0.5);

  left  = 0;
  right = 2 * kn + sqrt(4 * kn * kn + 0.25);

  // Counter for zeroes
  i = 0;
  
  // -- Step width for bracketing
  xstep = (right - left) / (100.0 * n);
  
  // Loop over coarse interval; bracketing
  // See Numerical Recipes
  for(xi = left; xi <= right; xi = xi + xstep){
    x1 = xi;
    x2 = x1 + xstep;
    
    f    = Laguerre(x1, n);
    fmid = Laguerre(x2, n);
    
    if(f * fmid < 0.0){
      if(f < 0.0){
	rtb = x1;
	dx  = x2 - x1;
      }
      else{
	rtb = x2;
	dx  = x1 - x2;
      }
      
      j = 0;
      while((j < 40) && (fabs(dx) > 0.00001)){
	dx   = dx * 0.5;
	xmid = rtb + dx;
	fmid = Laguerre(xmid, n);
	if(fmid < 0.0){
	  rtb = xmid;
	}
      }
      
      i++;
      // Checking the number of zeroes
      if(i > n){
	cerr << "ERROR from gaulag(): number of zeroes larger than polynomial order. " << i << " " << n << endl;
	myexit(1);
      }
      x[i] = rtb;
    }
  }

   // Checking the number of zeroes
   if(i != n){
     cerr << "ERROR from gaulag(): number of zeroes does not match polynomial order. " << i << " " << n << endl;
     myexit(1);
    }

   // Improve the found zeroes by Newton method
   for(i = 1; i <= n; i++){
     xi = x[i];
     x[i] = lagroot(xi, n);
   }

  // Compute the weights
  w[0] = 0.0;
  for(i = 1; i <= n; i++){
    xi   = x[i];
    f    = Laguerre_deriv(xi, n);
    w[i] = 1.0 / xi / (f * f);
  }
   
   
  // Output and checks, only on first call
  if(flag == 0){
    cout << "MESSAGE from gaulag(): Gauss-Laguerre mesh points and weights:" << endl;
    for(i = 1; i <= n; i++){
      cout << setw(3)  << i;
      cout << " " << setw(20) << setprecision(15) << x[i];
      cout << " " << setw(20) << setprecision(15) << w[i] << endl;
    }
    
    // 1. check
    sum = 0.0;
    for(i = 1; i <= n; i++){
      sum += x[i];
    }
    if(fabs(sum - n * n) > EMACH){
      cout << "MESSAGE from gaulag(): sum of zeroes = " << sum << " should equal " << n * n << endl;
    }
    
    // 2. check
    product = 0.0;
    fac     = 0.0;
    for(i = 1; i <= n; i++){
      product += log(x[i]);
      fac     += log(i);
    }
    if(fabs(product - fac) > EMACH){
      cout << "MESSAGE from gaulag(): log(product of zeroes) = " <<  product << " should equal " << fac << endl;
    }
    
    flag = 1;
  }
}
