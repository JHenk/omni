// cmatrix.h
//
// defines complex matrices and their operation with real and complex numbers
//
// DEBUG allows for error checking
//

#ifndef _CMATRIX_
#define _CMATRIX_

#include <iostream> 
#include <cstdlib>
#include <cstring>

#include "platform.h"

#include "complex.h"

#ifndef NULL
#define NULL    0
#endif

// JH To get rid of warning issued by g++ version 9
#pragma GCC diagnostic ignored "-Wclass-memaccess"

//
// Diese Klassen werden definiert:
//

class complexmatrix;
// JH - Added the following lines for compatibility with gcc4.1
// Thus, the compiler option -ffriend-injection is not needed
complex trace(const complexmatrix &);
double  norm(const complexmatrix &);

class complexdiagmatrix;
// JH - Added the following lines for compatibility with gcc4.1
// Thus, the compiler option -ffriend-injection is not needed
complex trace(const complexdiagmatrix &a);
double  norm(const complexdiagmatrix &);

class complexsubmatrix;

class complexsubdiagmatrix;

class complextempmatrix;
complextempmatrix inv(const complexmatrix &);
complextempmatrix transp(const complexmatrix &);

class complextempdiagmatrix;

//
// Folgende Klasse wird hier nicht definiert
//

class complexsupermatrix;

class doublematrix;
class doublediagmatrix;

//
// Definition der Matrixklasse
//

class complexmatrix{
protected:
  int     	r, c;           // Anzahl Zeilen (r) und Spalten (c)
  int     	n;              // Gesamtgroesse der Matrix
  complex 	*a;             // Zeiger auf das eigentliche Feld
public:
  // Die Konstruktoren
  complexmatrix();
  complexmatrix(int, int);
  // Der Kopierkonstruktor
  complexmatrix(const complexmatrix &);
  // Der Umkopierer
  complexmatrix(const complexsubmatrix &);
  complexmatrix(const complextempmatrix &);
  // Der Destruktor
  ~complexmatrix();
  // Zugriff auf einzelne Matrixelemente
  complex &operator()(int, int) const;
  // Die Zuweisung
  complexmatrix &operator=(double);
  complexmatrix &operator=(const complex &);
  complexmatrix &operator=(const complexmatrix &);
  complexmatrix &operator=(const complexsubmatrix &);
  complexmatrix &operator=(const complextempmatrix &);
  complexmatrix &operator=(const complexdiagmatrix &);
  complexmatrix &operator=(const complexsupermatrix &);
  // Setze die Groesse
  complexmatrix &setsize(int, int);
  void getsize(int &, int &) const;
  void getrawdata(double *&) const;
  void setrawdata(const double *, const int , const int);
  // Test, ob Matrix benutzt ist
  friend int used(const complexmatrix &);
  // Alle meine Freunde
  friend class complexsubmatrix;
  // Unaere Operatoren
  friend complextempmatrix operator+(const complexmatrix &);
  friend complextempmatrix operator-(const complexmatrix &);
  // Binaere Operatoren
  friend complextempmatrix operator*(const complexmatrix &, const complexmatrix &);
  friend complextempmatrix operator+(const complexmatrix &, const complexmatrix &);
  friend complextempmatrix operator-(const complexmatrix &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator+(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator-(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator*(const complexdiagmatrix &, const complexmatrix &);
  friend complextempmatrix operator+(const complexdiagmatrix &, const complexmatrix &);
  friend complextempmatrix operator-(const complexdiagmatrix &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const complex &);
  friend complextempmatrix operator+(const complexmatrix &, const complex &);
  friend complextempmatrix operator-(const complexmatrix &, const complex &);
  friend complextempmatrix operator*(const complex &, const complexmatrix &);
  friend complextempmatrix operator+(const complex &, const complexmatrix &);
  friend complextempmatrix operator-(const complex &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, double);
  friend complextempmatrix operator+(const complexmatrix &, double);
  friend complextempmatrix operator-(const complexmatrix &, double);
  friend complextempmatrix operator*(double, const complexmatrix &);
  friend complextempmatrix operator+(double, const complexmatrix &);
  friend complextempmatrix operator-(double, const complexmatrix &);
  friend complextempmatrix operator*(const doublematrix &, const complexmatrix &);
  friend complextempmatrix operator+(const doublematrix &, const complexmatrix &);
  friend complextempmatrix operator-(const doublematrix &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const doublematrix &);
  friend complextempmatrix operator+(const complexmatrix &, const doublematrix &);
  friend complextempmatrix operator-(const complexmatrix &, const doublematrix &);
  friend complextempmatrix operator*(const doublediagmatrix &, const complexmatrix &);
  friend complextempmatrix operator+(const doublediagmatrix &, const complexmatrix &);
  friend complextempmatrix operator-(const doublediagmatrix &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const doublediagmatrix &);
  friend complextempmatrix operator+(const complexmatrix &, const doublediagmatrix &);
  friend complextempmatrix operator-(const complexmatrix &, const doublediagmatrix &);
  // Funktionen
  friend complextempmatrix inv(const complexmatrix &);
  friend complextempmatrix transp(const complexmatrix &);
  friend complextempmatrix conj(const complexmatrix &);
  friend complextempmatrix imag(const complexmatrix &);
  friend complextempmatrix herm(const complexmatrix &);
  friend double norm(const complexmatrix &);
  friend void eigen(const complexmatrix &a, complexdiagmatrix &e, complexmatrix &v);
  friend complextempdiagmatrix solvelu(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix solve2lu(const complexmatrix &, const complexmatrix &);
  friend complextempmatrix invlu(const complexmatrix &);
  friend complex detlu(const complexmatrix &);
  friend complex logdetlu(const complexmatrix &);
  friend int diff(const complexmatrix &, const complexmatrix &);
  friend complex trace(const complexmatrix &);
  friend complexdiagmatrix diag(const complexmatrix &);
  // Ein- und Ausgabe
  friend std::ostream &operator<<(std::ostream &, const complexmatrix &);
  friend std::istream &operator>>(std::istream &, const complexmatrix &);
  friend void printcmatrix(const complexmatrix &a);
};

//
// Definition der diagonalen Matrixklasse
//

class complexdiagmatrix{
protected:
  int     	r;		// Anzahl Zeilen (r) und Spalten (r)
  complex 	*a;		// Zeiger auf das eigentliche Feld
public:
  // Die Konstruktoren
  complexdiagmatrix();
  complexdiagmatrix(int);
  // Der Kopierkonstruktor
  complexdiagmatrix(const complexdiagmatrix &);
  // Der Umkopierer
  complexdiagmatrix(const complexsubdiagmatrix &);
  complexdiagmatrix(const complextempdiagmatrix &);
  // Der Destruktor
  ~complexdiagmatrix();
  // Zugriff auf einzelne Matrixelemente
  complex &operator()(int) const;
  // Die Zuweisung
  complexdiagmatrix &operator=(double);
  complexdiagmatrix &operator=(const complex &);
  complexdiagmatrix &operator=(const complexdiagmatrix &);
  complexdiagmatrix &operator=(const complexsubdiagmatrix &);
  complexdiagmatrix &operator=(const complextempdiagmatrix &);
  // Setze die Groesse
  complexdiagmatrix &setsize(int);
  void getsize(int &, int &) const;
  // Test, ob Matrix benutzt ist
  friend int used(const complexdiagmatrix &);
  // Alle meine Freunde
  friend class complexmatrix;
  friend class complexsubdiagmatrix;
  // Unaere Operatoren
  friend complextempdiagmatrix operator+(const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &);
  // Binaere Operatoren
  friend complextempdiagmatrix operator*(const complexdiagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator+(const complexdiagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator+(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator-(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator*(const complexdiagmatrix &, const complexmatrix &);
  friend complextempmatrix operator+(const complexdiagmatrix &, const complexmatrix &);
  friend complextempmatrix operator-(const complexdiagmatrix &, const complexmatrix &);
  friend complextempdiagmatrix operator*(const complexdiagmatrix &, const complex &);
  friend complextempdiagmatrix operator+(const complexdiagmatrix &, const complex &);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &, const complex &);
  friend complextempdiagmatrix operator*(const complex &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator+(const complex &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(const complex &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator*(const complexdiagmatrix &, double);
  friend complextempdiagmatrix operator+(const complexdiagmatrix &, double);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &, double);
  friend complextempdiagmatrix operator*(double, const complexdiagmatrix &);
  friend complextempdiagmatrix operator+(double, const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(double, const complexdiagmatrix &);
  friend complextempmatrix operator*(const doublematrix &, const complexdiagmatrix &);
  friend complextempmatrix operator+(const doublematrix &, const complexdiagmatrix &);
  friend complextempmatrix operator-(const doublematrix &, const complexdiagmatrix &);
  friend complextempmatrix operator*(const complexdiagmatrix &, const doublematrix &);
  friend complextempmatrix operator+(const complexdiagmatrix &, const doublematrix &);
  friend complextempmatrix operator-(const complexdiagmatrix &, const doublematrix &);
  friend complextempdiagmatrix operator*(const doublediagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator+(const doublediagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(const doublediagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator*(const complexdiagmatrix &, const doublediagmatrix &);
  friend complextempdiagmatrix operator+(const complexdiagmatrix &, const doublediagmatrix &);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &, const doublediagmatrix &);
  // Funktionen
  friend complextempdiagmatrix exp(const complexdiagmatrix &);
  friend complextempdiagmatrix log(const complexdiagmatrix &);
  friend complextempdiagmatrix real(const complexdiagmatrix &);
  friend complextempdiagmatrix imag(const complexdiagmatrix &);
  friend complextempdiagmatrix inv(const complexdiagmatrix &);
  friend complextempdiagmatrix transp(const complexdiagmatrix &);
  friend complextempdiagmatrix abs(const complexdiagmatrix &);
  friend complextempdiagmatrix conj(const complexdiagmatrix &);
  friend complextempdiagmatrix herm(const complexdiagmatrix &);
  friend double norm(const complexdiagmatrix &);
  friend void eigen(const complexmatrix &a, complexdiagmatrix &e, complexmatrix &v);
  friend int diff(const complexdiagmatrix &, const complexdiagmatrix &);
  friend complex trace(const complexdiagmatrix &a);
  // Ein- und Ausgabe
  friend std::ostream &operator<<(std::ostream &, const complexdiagmatrix &);
  friend std::istream &operator>>(std::istream &, const complexdiagmatrix &);
  friend void printcmatrix(const complexdiagmatrix &a);
};

//
// Definition der Sub-Matrixklasse
//

class complexsubmatrix{
protected:
  int		r0, c0;		// Startzeile (r0) und Startspalte (c0)
  int     	r, c;           // Anzahl Zeilen (r) und Spalten (c)
  const complexmatrix
  &ref;           // Die Referenzierte Matrix
public:
  // Der Konstruktor
  complexsubmatrix(const complexmatrix &, int, int, int, int);
  // Zugriff auf einzelne Matrixelemente
  complex &operator()(int, int) const;
  // Die Zuweisung
  complexsubmatrix &operator=(const complexmatrix &);
  complexsubmatrix &operator=(const complexsubmatrix &);
  // Alle meine Freunde
  friend class complexmatrix;
};

//
// Definition der diagonalen Sub-Matrixklasse
//

class complexsubdiagmatrix{
protected:
  int		r0;		// Startzeile (r0) und Startspalte (r0)
  int     	r;		// Anzahl Zeilen (r) und Spalten (r)
  const complexdiagmatrix
  &ref;           // Die Referenzierte Matrix
public:
  // Der Konstruktor
  complexsubdiagmatrix(const complexdiagmatrix &, int, int);
  // Zugriff auf einzelne Matrixelemente
  complex &operator()(int) const;
  // Die Zuweisung
  complexsubdiagmatrix &operator=(const complexdiagmatrix &);
  complexsubdiagmatrix &operator=(const complexsubdiagmatrix &);
  // Alle meine Freunde
  friend class complexdiagmatrix;
};

//
// Definition der temporaeren Matrixklasse
//

class complextempmatrix{
protected:
  int     	r, c;           // Anzahl Spalten (r) und Zeilen (c)
  int     	n;              // Gesamtgroesse der Matrix
  complex 	*a;             // Zeiger auf das eigentliche Feld
public:
  // Der Konstruktor
  complextempmatrix(int, int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class complexmatrix;
  // Unaere Operatoren
  friend complextempmatrix operator+(const complexmatrix &);
  friend complextempmatrix operator-(const complexmatrix &);
  // Binaere Operatoren
  friend complextempmatrix operator*(const complexmatrix &, const complexmatrix &);
  friend complextempmatrix operator+(const complexmatrix &, const complexmatrix &);
  friend complextempmatrix operator-(const complexmatrix &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator+(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator-(const complexmatrix &, const complexdiagmatrix &);
  friend complextempmatrix operator*(const complexdiagmatrix &, const complexmatrix &);
  friend complextempmatrix operator+(const complexdiagmatrix &, const complexmatrix &);
  friend complextempmatrix operator-(const complexdiagmatrix &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const complex &);
  friend complextempmatrix operator+(const complexmatrix &, const complex &);
  friend complextempmatrix operator-(const complexmatrix &, const complex &);
  friend complextempmatrix operator*(const complex &, const complexmatrix &);
  friend complextempmatrix operator+(const complex &, const complexmatrix &);
  friend complextempmatrix operator-(const complex &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, double);
  friend complextempmatrix operator+(const complexmatrix &, double);
  friend complextempmatrix operator-(const complexmatrix &, double);
  friend complextempmatrix operator*(double, const complexmatrix &);
  friend complextempmatrix operator+(double, const complexmatrix &);
  friend complextempmatrix operator-(double, const complexmatrix &);
  friend complextempmatrix operator*(const doublematrix &, const complexmatrix &);
  friend complextempmatrix operator+(const doublematrix &, const complexmatrix &);
  friend complextempmatrix operator-(const doublematrix &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const doublematrix &);
  friend complextempmatrix operator+(const complexmatrix &, const doublematrix &);
  friend complextempmatrix operator-(const complexmatrix &, const doublematrix &);
  friend complextempmatrix operator*(const doublematrix &, const complexdiagmatrix &);
  friend complextempmatrix operator+(const doublematrix &, const complexdiagmatrix &);
  friend complextempmatrix operator-(const doublematrix &, const complexdiagmatrix &);
  friend complextempmatrix operator*(const complexdiagmatrix &, const doublematrix &);
  friend complextempmatrix operator+(const complexdiagmatrix &, const doublematrix &);
  friend complextempmatrix operator-(const complexdiagmatrix &, const doublematrix &);
  friend complextempmatrix operator*(const doublematrix &, const complex &);
  friend complextempmatrix operator+(const doublematrix &, const complex &);
  friend complextempmatrix operator-(const doublematrix &, const complex &);
  friend complextempmatrix operator*(const complex &, const doublematrix &);
  friend complextempmatrix operator+(const complex &, const doublematrix &);
  friend complextempmatrix operator-(const complex &, const doublematrix &);
  friend complextempmatrix operator*(const doublediagmatrix &, const complexmatrix &);
  friend complextempmatrix operator+(const doublediagmatrix &, const complexmatrix &);
  friend complextempmatrix operator-(const doublediagmatrix &, const complexmatrix &);
  friend complextempmatrix operator*(const complexmatrix &, const doublediagmatrix &);
  friend complextempmatrix operator+(const complexmatrix &, const doublediagmatrix &);
  friend complextempmatrix operator-(const complexmatrix &, const doublediagmatrix &);
  // Funktionen
  friend complextempmatrix inv(const complexmatrix &);
  friend complextempmatrix solve2lu(const complexmatrix &, const complexmatrix &);
  friend complextempmatrix invlu(const complexmatrix &);
  friend complextempmatrix transp(const complexmatrix &);
  friend complextempmatrix conj(const complexmatrix &);
  friend complextempmatrix imag(const complexmatrix &);
  friend complextempmatrix herm(const complexmatrix &);
};

//
// Definition der temporaeren diagonalen Matrixklasse
//

class complextempdiagmatrix{
protected:
  int     	r;              // Anzahl Spalten (r) und Zeilen (r)
  complex 	*a;             // Zeiger auf das eigentliche Feld
public:
  // Der Konstruktor
  complextempdiagmatrix(int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class complexdiagmatrix;
  // Unaere Operatoren
  friend complextempdiagmatrix operator+(const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &);
  // Binaere Operatoren
  friend complextempdiagmatrix operator*(const complexdiagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator+(const complexdiagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator*(const complexdiagmatrix &, const complex &);
  friend complextempdiagmatrix operator+(const complexdiagmatrix &, const complex &);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &, const complex &);
  friend complextempdiagmatrix operator*(const complex &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator+(const complex &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(const complex &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator*(const complexdiagmatrix &, double);
  friend complextempdiagmatrix operator+(const complexdiagmatrix &, double);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &, double);
  friend complextempdiagmatrix operator*(double, const complexdiagmatrix &);
  friend complextempdiagmatrix operator+(double, const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(double, const complexdiagmatrix &);
  friend complextempdiagmatrix operator*(const doublediagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator+(const doublediagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator-(const doublediagmatrix &, const complexdiagmatrix &);
  friend complextempdiagmatrix operator*(const complexdiagmatrix &, const doublediagmatrix &);
  friend complextempdiagmatrix operator+(const complexdiagmatrix &, const doublediagmatrix &);
  friend complextempdiagmatrix operator-(const complexdiagmatrix &, const doublediagmatrix &);
  friend complextempdiagmatrix operator*(const doublediagmatrix &, const complex &);
  friend complextempdiagmatrix operator+(const doublediagmatrix &, const complex &);
  friend complextempdiagmatrix operator-(const doublediagmatrix &, const complex &);
  friend complextempdiagmatrix operator*(const complex &, const doublediagmatrix &);
  friend complextempdiagmatrix operator+(const complex &, const doublediagmatrix &);
  friend complextempdiagmatrix operator-(const complex &, const doublediagmatrix &);
  // Funktionen
  friend complextempdiagmatrix exp(const complexdiagmatrix &);
  friend complextempdiagmatrix log(const complexdiagmatrix &);
  friend complextempdiagmatrix real(const complexdiagmatrix &);
  friend complextempdiagmatrix imag(const complexdiagmatrix &);
  friend complextempdiagmatrix inv(const complexdiagmatrix &);
  friend complextempdiagmatrix transp(const complexdiagmatrix &);
  friend complextempdiagmatrix abs(const complexdiagmatrix &);
  friend complextempdiagmatrix conj(const complexdiagmatrix &);
  friend complextempdiagmatrix herm(const complexdiagmatrix &);
  friend complextempdiagmatrix solvelu(const complexmatrix &, const complexdiagmatrix &);
};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine Matrix
//

inline  complexmatrix::complexmatrix()

{
  // Keine Zeilen und Spalten
  r = 0;
  c = 0;
  // Keine Groesse
  n = 0;
  // Kein Speicher
  a = NULL;
}

//
// Erzeuge eine beliebige Matrix
//

inline  complexmatrix::complexmatrix(int n1, int n2)

{
#ifdef DEBUG
  if(n1 <= 0 || n2 <= 0){
    cerr << "ERROR from complexmatrix: Invalid dimensions\n";
    myexit(1);
  }
#endif
  // Anzahl Zeilen und Spalten
  r = n1;
  c = n2;
  // Groesse berechnen
  n = r * c;
  // Noetigen Speicher anfordern
  a = new complex[n];
}

//
// Der Kopierkonstruktor
//

inline  complexmatrix::complexmatrix(const complexmatrix &z)

{
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Groesse kopieren
  n = z.n;
  // Speicher anfordern
  a = new complex[n];
  // Und alles kopieren
  memcpy(a, z.a, n * sizeof(complex));
}

inline  complexmatrix::complexmatrix(const complextempmatrix &z)

{
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Groesse kopieren
  n = z.n;
  // Einfach Zeiger kopieren
  a = z.a;
}

//
// Der Destruktor
//

inline  complexmatrix::~complexmatrix()

{
  // Gibt den Speicher frei (Test auf NULL nicht noetig?)
  delete[] a;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline  complex &complexmatrix::operator()(int n1, int n2) const

{
#ifdef DEBUG
  if(n1 < 0 || n1 >= r || n2 < 0 || n2 >= c){
    cerr << "ERROR from complex: Subscript out of range\n";
    myexit(1);
  }
#endif
  // Gib Referenz auf Element zurueck
  return a[n1 * c + n2];
}

//
// Die Zuweisung
//

inline  complexmatrix &complexmatrix::operator=(const complextempmatrix &z)

{
  // Gibt den Speicher frei (Test auf NULL nicht noetig?)
  delete[] a;
  // Abmessungen kopieren
  r = z.r;
  c = z.c;
  // Groesse kopieren
  n = z.n;
  // Einfach Zeiger kopieren
  a = z.a;
  return *this;
}

//
// Setze die Groesse einer Matrix
//

inline  complexmatrix &complexmatrix::setsize(int n1, int n2)
{
#ifdef DEBUG
  if(n1 < 0 || n2 < 0){
    cerr << "ERROR from complexmatrix: Invalid dimensions\n";
    myexit(1);
  }
#endif
  if(n != n1 * n2){
    // Groesse berechnen
    n = n1 * n2;
    // Gibt den Speicher frei (Test auf NULL nicht noetig?)
    delete[] a;
    if(n != 0){
      // Noetigen Speicher anfordern
      a = new complex[n];
    }
    else{
      // Keinen Speicher anfordern
      a = NULL;
    }
  }
  // Anzahl Zeilen und Spalten
  r = n1;
  c = n2;
  return *this;
}

inline void complexmatrix::getsize(int &rr, int &cc) const
{
  rr = r;
  cc = c;
}

inline void complexmatrix::getrawdata(double *&data) const
{
  //just a long array of doubles, the 2 is because the data is complex
  if(data == NULL)
    data = new double[r*c*2];
  for(int i=0;i< r*c; i++){
    data[2*i] = a[i].real();
    data[2*i+1] = a[i].imag();
  }
}

inline void complexmatrix::setrawdata(const double *data, const int rr, const int cc)
{
  r = rr;
  c = cc;
  n = r*c;

  a = new complex[n];
  for(int i=0;i< r*c; i++){
    a[i].re = data[2*i];
    a[i].im = data[2*i+1];
  }
}


//
// Test, ob die Matrix benutzt worden ist
//

inline int used(const complexmatrix &z)

{
  return z.n != 0;
}

//
// Erzeuge Platzhalter fuer eine diagonale Matrix
//

inline  complexdiagmatrix::complexdiagmatrix()

{
  // Keine Zeilen und Spalten
  r = 0;
  // Kein Speicher
  a = NULL;
}

//
// Erzeuge eine diagonale Matrix
//

inline  complexdiagmatrix::complexdiagmatrix(int n)

{
#ifdef DEBUG
  if(n <= 0){
    cerr << "ERROR from complexdiagmatrix: Invalid dimensions\n";
    myexit(1);
  }
#endif
  // Anzahl Zeilen und Spalten
  r = n;
  // Noetigen Speicher anfordern
  a = new complex[r];
}

//
// Der Kopierkonstruktor
//

inline  complexdiagmatrix::complexdiagmatrix(const complexdiagmatrix &z)

{
  // Abmessungen kopieren
  r = z.r;
  // Speicher anfordern
  a = new complex[r];
  // Und alles kopieren
  memcpy(a, z.a, r * sizeof(complex));
}

inline  complexdiagmatrix::complexdiagmatrix(const complextempdiagmatrix &z)

{
  // Abmessungen kopieren
  r = z.r;
  // Einfach Zeiger kopieren
  a = z.a;
}

//
// Der Destruktor
//

inline  complexdiagmatrix::~complexdiagmatrix()

{
  // Gibt den Speicher frei (Test auf NULL nicht noetig?)
  delete[] a;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline  complex &complexdiagmatrix::operator()(int n) const

{
#ifdef DEBUG
  if(n < 0 || n >= r){
    cerr << "ERROR from complex: Subscript out of range\n";
    myexit(1);
  }
#endif
  // Gib Referenz auf Element zurueck
  return a[n];
}

//
// Die Zuweisung
//

inline  complexdiagmatrix &complexdiagmatrix::operator=(const complextempdiagmatrix &z)

{
  // Gibt den Speicher frei (Test auf NULL nicht noetig?)
  delete[] a;
  // Abmessungen kopieren
  r = z.r;
  // Einfach Zeiger kopieren
  a = z.a;
  return *this;
}

//
// Setze die Groesse einer diagonalen Matrix
//

inline  complexdiagmatrix &complexdiagmatrix::setsize(int n)

{
#ifdef DEBUG
  if(n < 0){
    cerr << "ERROR from complexdiagmatrix: Invalid dimensions\n";
    myexit(1);
  }
#endif
  if(r != n){
    // Anzahl Zeilen und Spalten
    r = n;
    // Gibt den Speicher frei (Test auf NULL nicht noetig?)
    delete[] a;
    if(n != 0){
      // Noetigen Speicher anfordern
      a = new complex[r];
    }
    else{
      // Keinen Speicher anfordern
      a = NULL;
    }
  }
  return *this;
}

inline void complexdiagmatrix::getsize(int &rr, int &cc) const{
  rr=r;
  cc=r;
}


//
// Test, ob die Matrix benutzt worden ist
//

inline int used(const complexdiagmatrix &z)

{
  return z.r != 0;
}

//
// Erzeuge eine Submatrix
//

inline	complexsubmatrix::complexsubmatrix(const complexmatrix &z, int m1, int m2, int n1, int n2) : ref(z)

{
#ifdef DEBUG
  if(m1 < 0 || m2 < 0 || n1 <= 0 || n2 <= 0){
    cerr << "ERROR from complexsubmatrix: Invalid subdimensions\n";
    myexit(1);
  }
#endif
  // Der Startpunkt
  r0 = m1;
  c0 = m2;
  // Die Groesse
  r = n1;
  c = n2;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline	complex &complexsubmatrix::operator()(int n1, int n2) const

{
#ifdef DEBUG
  if(n1 < 0 || n1 >= r || n2 < 0 || n2 >= c){
    cerr << "ERROR from complex: Subscript out of range\n";
    myexit(1);
  }
#endif
  // Gib Referenz auf Element zurueck
  return ref(n1 + r0, n2 + c0);
}

//
// Erzeuge eine diagonale Submatrix
//

inline	complexsubdiagmatrix::complexsubdiagmatrix(const complexdiagmatrix &z, int m, int n) : ref(z)

{
#ifdef DEBUG
  if(m < 0 || n <= 0){
    cerr << "ERROR from complexsubdiagmatrix: Invalid subdimensions\n";
    myexit(1);
  }
#endif
  // Der Startpunkt
  r0 = m;
  // Die Groesse
  r = n;
}

//
// Zugriff auf einzelne Matrixelemente
//

inline	complex &complexsubdiagmatrix::operator()(int n) const

{
#ifdef DEBUG
  if(n < 0 || n >= r){
    cerr << "ERROR from complex: Subscript out of range\n";
    myexit(1);
  }
#endif
  // Gib Referenz auf Element zurueck
  return ref(n + r0);
}

//
// Erzeuge eine temporaere Matrix
//

inline  complextempmatrix::complextempmatrix(int n1, int n2)

{
  // Anzahl Zeilen und Spalten
  r = n1;
  c = n2;
  // Groesse berechnen
  n = n1 * n2;
  // Noetigen Speicher anfordern
  a = new complex[n];
}

//
// Erzeuge eine temporaere diagonale Matrix
//

inline  complextempdiagmatrix::complextempdiagmatrix(int n)

{
  // Anzahl Zeilen und Spalten
  r = n;
  // Noetigen Speicher anfordern
  a = new complex[r];
}

#endif
