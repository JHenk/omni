//
// Driver functions for pseudo random numbers
//

#include <iostream>
#include <iomanip>

#include <cstdlib>
#include <ctime>

using namespace std;

// Initialize the random number generator
void init_omni_random(int seed){

  cout << "MESSAGE from init_omni_random(): RAND_MAX = " << RAND_MAX << endl;

  if(seed == 1){
    // The generator is reinitialized to its initial value and produces
    // the same values as before any call to rand or srand
    srand(1);
    cout << "MESSAGE from init_omni_random(): seed     = " << seed << " (fixed)" << endl;
  }
  else{
    // Initialize the generator by the current time
    srand ( time(NULL) );
    cout << "MESSAGE from init_omni_random(): seed     = " << seed << " (time-dependent)" << endl;
  }


}

// Produce a random number between 0 and 1
// RAND_MAX is defined in stdlib.h. Its default value may vary between 
// implementations but it is granted to be at least 32767.
double omni_random(){

  // rand() returns an integer between 0 and RAND_MAX
  return double(rand()) / double(RAND_MAX);

}
