// density.h
//
// deklariert komplexe Spindichtematrizen, sowie mathematische Operationen
// zwischen Spindichtematrizen, komplexen und reellen Zahlen
//

#ifndef _DENSITY_
#define _DENSITY_

#include <iostream>
#include <cmath>

#include "complex.h"

#define	SX	density(cmplx(0, 0), cmplx(1, 0), cmplx(1, 0), cmplx(0, 0))
#define	SY	density(cmplx(0, 0), cmplx(0, -1), cmplx(0, 1), cmplx(0, 0))
#define SZ	density(cmplx(1, 0), cmplx(0, 0), cmplx(0, 0), cmplx(-1, 0))

class density{
 public:
  complex		uu;		// die up up-Komponente der Matrix
  complex		ud;		// die up dn-Komponente der Matrix
  complex		du;		// die dn up-Komponente der Matrix
  complex		dd;		// die dn dn-Komponente der Matrix
  // Die Konstruktoren
  density();
  density(const complex &, const complex &, const complex &, const complex &);
  density(const complexmatrix &);
  // Der Destruktor
  ~density();
  // Die Zuweisung
  density &operator=(double);
  density &operator=(const complex &);
  density &operator=(const density &);
  // Unaere Operatoren
  inline friend density operator+(const density &);
  inline friend density operator-(const density &);
  // Binaere Operatoren
  inline friend density operator*(const density &, const density &);
  inline friend density operator+(const density &, const density &);
  inline friend density operator-(const density &, const density &);
  inline friend density operator*(const density &, const complex &);
  inline friend density operator/(const density &, const complex &);
  inline friend density operator+(const density &, const complex &);
  inline friend density operator-(const density &, const complex &);
  inline friend density operator*(const density &, double);
  inline friend density operator/(const density &, double);
  inline friend density operator+(const density &, double);
  inline friend density operator-(const density &, double);
  inline friend density operator*(const complex &, const density &);
  inline friend density operator+(const complex &, const density &);
  inline friend density operator-(const complex &, const density &);
  inline friend density operator*(double, const density &);
  inline friend density operator+(double, const density &);
  inline friend density operator-(double, const density &);
  // Funktionen
  inline friend complex trace(const density &);
  inline friend density herm(const density &);
  inline friend density real(const density &);
  inline friend density imag(const density &);
  // Ein- und Ausgabe
  inline friend std::ostream &operator<<(std::ostream &, const density &);
  inline friend std::istream &operator>>(std::istream &, density &);
};

// Die Konstruktoren

inline density::density()

{
}

inline density::density(const complex &xx, const complex &xy, const complex &yx, const complex &yy)

{
  uu = xx;
  ud = xy;
  du = yx;
  dd = yy;
}

inline density::density(const complexmatrix &A)

{
  uu = A(0, 0);
  ud = A(0, 1);
  du = A(1, 0);
  dd = A(1, 1);
}

// Der Destruktor

inline density::~density()

{
}

// Die Zuweisung

inline density &density::operator=(double z)

{
  uu = z;
  ud = 0;
  du = 0;
  dd = z;
  return *this;
}

  inline density &density::operator=(const complex &z)

  {
    uu = z;
    ud = 0;
    du = 0;
    dd = z;
    return *this;
  }

    inline density &density::operator=(const density &a)

    {
      uu = a.uu;
      ud = a.ud;
      du = a.du;
      dd = a.dd;
      return *this;
    }

    // Unaere Operatoren

      inline density operator+(const density &a)

      {
	return density(a.uu, a.ud, a.du, a.dd);
      }

inline density operator-(const density &a)

{
  return density(-a.uu, -a.ud, -a.du, -a.dd);
}

// Binaere Operatoren

inline density operator*(const density &a, const density &b)

{
  return density(a.uu * b.uu + a.ud * b.du, a.uu * b.ud + a.ud * b.dd,
		 a.du * b.uu + a.dd * b.du, a.du * b.ud + a.dd * b.dd);
}

inline density operator+(const density &a, const density &b)

{
  return density(a.uu + b.uu, a.ud + b.ud, a.du + b.du, a.dd + b.dd);
}

inline density operator-(const density &a, const density &b)

{
  return density(a.uu - b.uu, a.ud - b.ud, a.du - b.du, a.dd - b.dd);
}

inline density operator*(const density &a, const complex &b)

{
  return density(a.uu * b, a.ud * b, a.du * b, a.dd * b);
}

inline density operator/(const density &a, const complex &b)

{
  return density(a.uu / b, a.ud / b, a.du / b, a.dd / b);
}

inline density operator+(const density &a, const complex &b)

{
  return density(a.uu + b, a.ud, a.du, a.dd + b);
}

inline density operator-(const density &a, const complex &b)

{
  return density(a.uu - b, a.ud, a.du, a.dd - b);
}

inline density operator*(const density &a, double b)

{
  return density(a.uu * b, a.ud * b, a.du * b, a.dd * b);
}

inline density operator/(const density &a, double b)

{
  return density(a.uu / b, a.ud / b, a.du / b, a.dd / b);
}

inline density operator+(const density &a, double b)

{
  return density(a.uu + b, a.ud, a.du, a.dd + b);
}

inline density operator-(const density &a, double b)

{
  return density(a.uu - b, a.ud, a.du, a.dd - b);
}

inline density operator*(const complex &a, const density &b)

{
  return density(a * b.uu, a * b.ud, a * b.du, a * b.dd);
}

inline density operator+(const complex &a, const density &b)

{
  return density(a + b.uu, b.ud, b.du, a + b.dd);
}

inline density operator-(const complex &a, const density &b)

{
  return density(a - b.uu, -b.ud, -b.du, a - b.dd);
}

inline density operator*(double a, const density &b)

{
  return density(a * b.uu, a * b.ud, a * b.du, a * b.dd);
}

inline density operator+(double a, const density &b)

{
  return density(a + b.uu, b.ud, b.du, a + b.dd);
}

inline density operator-(double a, const density &b)

{
  return density(a - b.uu, -b.ud, -b.du, a - b.dd);
}

// Funktionen

// berechne die Spur einer Spindichtematrix
inline complex trace(const density &a)

{
  return a.uu + a.dd;
}

inline density herm(const density &a)

{
  return density(conj(a.uu), conj(a.du), conj(a.ud), conj(a.dd));
}

inline density real(const density &a)

{
  return (a + herm(a)) / 2;
}

inline density imag(const density &a)

{
  return (a - herm(a)) / cmplx(0, 2);
}

// Ein- und Ausgabe

inline std::ostream &operator<<(std::ostream &os, const density &a)

{
  int w = os.width(0);
  int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
  long f = os.flags(std::_Ios_Fmtflags(0));
#else
  long f = os.flags(0);
#endif
  os << "(";
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << real(a.uu);
  os << ",";
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << imag(a.uu);
  os << ") ";
  os << "(";
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << real(a.ud);
  os << ",";
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << imag(a.ud);
  os << ") " << std::endl;
  os << "(";
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << real(a.du);
  os << ",";
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << imag(a.du);
  os << ") ";
  os << "(";
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << real(a.dd);
  os << ",";
  os.width(w);
  os.precision(p);
#if defined(GCC3) || defined(GCC4)
  os.flags(std::_Ios_Fmtflags(f));
#else
  os.flags(f);
#endif
  os << imag(a.dd);
  os << ") " << std::endl;
  return os;
}

inline std::istream &operator>>(std::istream &is, density &a)

{
  is >> a.uu;
  is >> a.ud;
  is >> a.du;
  is >> a.dd;
  return is;
}

#endif
